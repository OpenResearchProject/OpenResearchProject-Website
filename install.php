<?php
session_start();
error_reporting(E_ALL); // Error reporting is fine here since the admin will be running it (hopefully)
ini_set('display_errors',1);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>OpenResearch Installer</title>
<style type="text/css">
body {
	padding: 0px;
	font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
	margin: 0px;
	height: 100%;
	background-color: #E0E0E0;
}
h1 {
	text-align:center;
	width: 100%;
	background-color: #039BE5;
	height: 64px;
	-webkit-box-shadow: 0px 1px 3px 1px #BBBBBB;
	box-shadow: 0px 1px 3px 1px #BBBBBB;
	color: #FFFFFF;
	background-image: url(../Alpha/pages/img/ORPLogo.png);
	background-repeat: no-repeat;
	padding: 0px;
	background-position: 8px 0%;
	font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
	line-height: 64px;
	margin-top: 0px;
	margin-left: 0px;
	margin-right: 0px;
	margin-bottom: 12px;
}
form {
	width: 488px;
	margin-left: auto;
	margin-right: auto;
	display: block;
	margin-bottom: 24px;
	-webkit-box-shadow: 0px 1px 3px 1px #BBBBBB;
	box-shadow: 0px 1px 3px 1px #BBBBBB;
	border-radius: 2px;
	padding: 12px;
	background-color: #FFFFFF;
	text-align: justify;
	margin-top: 24px;
}
label {
	width: 100%;
	margin-bottom: 12px;
	display: block;
	padding: 0px;
	margin-top: 0px;
	float: left;
	clear: both;
}
br {
	display: none;
}
hr {
	width: 70%;
	position: relative;
	float: left;
	clear: both;
	margin-left: 15%;
	margin-right: 15%;
}
p {
	text-align:center;
	margin-left: 12px;
	margin-right: 12px;
}
form p {
	text-align:left;
	float: left;
	clear: both;
}
form>input {
	clear: both;
	display: block;
	margin-top: 12px;
	margin-left: auto;
	margin-right: auto;
}
</style>
</head>
<body>

<h1 style="letter-spacing:1px;">Install OpenResearch</h1>

<?php
///////////////////////
//
//  install.php
//  Not Included
//  Installs OpenResearch
//  for the first time.
//  DELETE AFTERWARDS!
//
//////////////////////

if((isset($_POST['phase'])) && (is_numeric($_POST['phase']))){
	$phase = (int)$_POST['phase'];
}else{
	if((isset($_GET['phase'])) && (is_numeric($_GET['phase']))){
		$phase = (int)$_GET['phase'];
	}else{
		$phase = 0;
	}
}

if(isset($_POST['lang'])){

	$langNum = (int)$_POST['lang'];

	switch($langNum){
		case 1: $lang = "en_us.php"; break;
		default: $lang = "en_us.php"; break;
	}

	$includeCheck = 1;
	if(!include('./lang/' . $lang)){ die('Unable to find language file.'); }
}

$message="";

if($phase==0){
	//select language
?>
<form action="./install.php" method="POST" autocomplete="off">
<input type="hidden" name="phase" value="1">
<select name="lang">
<option value="1">American English</option>
</select><br><br><br><br>
<input type="submit" value="Continue">
</form>
<?php
}

if($phase==1){
	if((isset($_POST['dbUN'])) && (isset($_POST['dbName'])) && (isset($_POST['dbPW'])) && (isset($_POST['dbURL'])) && (isset($_POST['dbType'])) && (isset($_POST['adminUserName'])) && (isset($_POST['adminPW'])) && (isset($_POST['adminPW2'])) && (isset($_POST['adminEmail'])) && (isset($_POST['timezone'])) && (isset($_POST['userFullName'])) && (isset($_POST['hashCost']))){
	//if the form was submitted
		$dbUN = $_POST['dbUN'];
		$dbPW = $_POST['dbPW'];
		$dbURL = $_POST['dbURL'];
		$dbName = $_POST['dbName'];
		$dbType = (int)$_POST['dbType'];
		$userName = $_POST['adminUserName'];
		$password = $_POST['adminPW'];
		$passwordVerify = $_POST['adminPW2'];
		$email = $_POST['adminEmail'];
		$timezone = $_POST['timezone'];
		$hashCost = (int)$_POST['hashCost'];
		$userFullName = $_POST['userFullName'];

		if((strlen($userName)<3) || (strlen($userName)>25)){ $message.='Your username MUST be between 3 and 25 characters long!<br>'; }
                if((strlen($password)<8) || (strlen($password)>70)){ $message.='Your password MUST be between 8 and 70 characters long!<br>'; }
                if(strcmp($password, $passwordVerify) !== 0){ $message.='Your password didn\'t match! It IS case sensitive!<br>'; }
		if(strlen($email)<3){ $message.='You must enter your email address!<br>'; }
		if(strlen($timezone)<4){ $message.='You must enter a timezone!<br>'; }
		if(strlen($userFullName)<1){ $message.='You must enter your name!<br>'; }

		if(strlen($message)==0){
			// IF there are no errors

			if($dbType === 1){ // IF MySQLi was selected
				$link = mysqli_connect($dbURL,$dbUN,$dbPW,$dbName);
				if(mysqli_connect_errno($link)){ $message.='Couldn\'t connect to database.'; }

				if(strlen($message)==0){
					//last error check

					// Setup Query Array for our loop below
					$query = [];

					// DROP ALL IF EXISTS:
					$query[] = 'DROP TABLE IF EXISTS PeerReviews';
					$query[] = 'DROP TABLE IF EXISTS PaperCategoryAssoc';
					$query[] = 'DROP TABLE IF EXISTS UserCategoriesAssoc';
                                        $query[] = 'DROP TABLE IF EXISTS Categories';
                                        $query[] = 'DROP TABLE IF EXISTS PaperLinks';
                                        $query[] = 'DROP TABLE IF EXISTS PaperComments';
                                        $query[] = 'DROP TABLE IF EXISTS PaperReferences';
                                        $query[] = 'DROP TABLE IF EXISTS PaperFootnotes';
					$query[] = 'DROP TABLE IF EXISTS UsersPapersAssoc';
					$query[] = 'DROP TABLE IF EXISTS UsersPapersEndorseAssoc';
                                        $query[] = 'DROP TABLE IF EXISTS PaperTags';
                                        $query[] = 'DROP TABLE IF EXISTS Papers';
                                        $query[] = 'DROP TABLE IF EXISTS UsersGroupsAssoc';
                                        $query[] = 'DROP TABLE IF EXISTS GroupComments';
                                        $query[] = 'DROP TABLE IF EXISTS Groups';
					$query[] = 'DROP TABLE IF EXISTS Logins';
					$query[] = 'DROP TABLE IF EXISTS IPs';
                                        $query[] = 'DROP TABLE IF EXISTS Users';
                                        $query[] = 'DROP TABLE IF EXISTS Journal';
                                        $query[] = 'DROP TABLE IF EXISTS Publisher';
                                        $query[] = 'DROP TABLE IF EXISTS Settings';
                                        $query[] = 'DROP TABLE IF EXISTS Taxonomy';

                                        // CREATE TABLES:

$query[] = 'CREATE TABLE Users(userId bigint(20) NOT NULL AUTO_INCREMENT,userName longtext,password longtext, emailAddress varchar(255), avatar longtext, authorID bigint(20), joinDate DATETIME, degree varchar(255), title varchar(255), university varchar(255), timezone varchar(255), language varchar(255), lastLogin DATETIME, activationCode longtext, uniqueId longtext, admin int(1) DEFAULT 0, paperCount int(12), endorserStatus int(1) NOT NULL DEFAULT 1,userFullName varchar(255),skipCount int DEFAULT 0,skipTime bigint(20), PRIMARY KEY(userId))';
$query[] = 'CREATE TABLE IPs(ipId bigint(20) NOT NULL AUTO_INCREMENT, ipAddress text NOT NULL, userId bigint(20) NOT NULL, PRIMARY KEY(ipId), FOREIGN KEY(userId) REFERENCES Users(userId))';
$query[] = 'CREATE TABLE Logins(loginId bigint(20) NOT NULL AUTO_INCREMENT, ipAddress text NOT NULL, userId bigint(20), lastLoginAttempt bigint(20), loginAttempts int, PRIMARY KEY(loginId))';
$query[] = 'CREATE TABLE Groups(groupId bigint(20) NOT NULL AUTO_INCREMENT,groupName varchar(255),groupLeader bigint(20) NOT NULL, PRIMARY KEY(groupId), FOREIGN KEY(groupLeader) REFERENCES Users(userId))';
$query[] = 'CREATE TABLE GroupComments(groupCommentId bigint(20) NOT NULL AUTO_INCREMENT, groupId bigint(20) NOT NULL,userId bigint(20) NOT NULL,content text, PRIMARY KEY(groupCommentId), FOREIGN KEY(groupId) REFERENCES Groups(groupId), FOREIGN KEY(userId) REFERENCES Users(userId))';
$query[] = 'CREATE TABLE UsersGroupsAssoc(userGroupAssocId bigint(20) NOT NULL AUTO_INCREMENT, userId bigint(20) NOT NULL, groupId bigint(20) NOT NULL, PRIMARY KEY(userGroupAssocId), FOREIGN KEY(userId) REFERENCES Users(userId), FOREIGN KEY(groupId) REFERENCES Groups(groupId))';

if((double)substr(mysqli_get_server_info($link), 0, strpos(mysqli_get_server_info($link), '.', 2)) >= 5.6){
	// IF we are using a version of mysql that supports FULLTEXT
	$query[] = 'CREATE TABLE Papers(paperId bigint(20) NOT NULL AUTO_INCREMENT, paperName varchar(255), paperURL varchar(255), paperPublishDate date, publisherId bigint(20), abstract text, sponsor varchar(255), digObjIdUrl varchar(255), digObjId varchar(255), copyRightOwner varchar(255), copyRightDate date, paperText mediumtext, isEndorsed int(1) NOT NULL DEFAULT 1, views bigint(20), fulltext (paperName,abstract), PRIMARY KEY(paperId))engine=InnoDB';
}else{
	$query[] = 'CREATE TABLE Papers(paperId bigint(20) NOT NULL AUTO_INCREMENT, paperName varchar(255), paperURL varchar(255), paperPublishDate date, publisherId bigint(20), abstract text, sponsor varchar(255), digObjIdUrl varchar(255), digObjId varchar(255), copyRightOwner varchar(255), copyRightDate date, paperText mediumtext, isEndorsed int(1) NOT NULL DEFAULT "1", views bigint(20), PRIMARY KEY(paperId))';
}
$query[] = 'CREATE TABLE PaperTags(paperTagId bigint(20) NOT NULL AUTO_INCREMENT,paperTagName varchar(255),paperId bigint(20) NOT NULL, PRIMARY KEY(paperTagId), FOREIGN KEY(paperId) REFERENCES Papers(paperId))';

if((double)substr(mysqli_get_server_info($link), 0, strpos(mysqli_get_server_info($link), '.', 2)) >= 5.6){
        // IF we are using a version of mysql that supports FULLTEXT
	$query[] = 'CREATE TABLE Publisher(publisherId bigint(20) NOT NULL AUTO_INCREMENT, publisherName varchar(255), publisherUrl varchar(255), fulltext (publisherName), PRIMARY KEY(publisherId))engine=InnoDB';
}else{
	$query[] = 'CREATE TABLE Publisher(publisherId bigint(20) NOT NULL AUTO_INCREMENT, publisherName varchar(255), publisherUrl varchar(255), PRIMARY KEY(publisherId))';
}


$query[] = 'CREATE TABLE Journal(journalId bigint(20) NOT NULL AUTO_INCREMENT, journalName varchar(255) NOT NULL, publisherId bigint(20), PRIMARY KEY(journalId), FOREIGN KEY(publisherId) REFERENCES Publisher(publisherId))';

if((double)substr(mysqli_get_server_info($link), 0, strpos(mysqli_get_server_info($link), '.', 2)) >= 5.6){
        // IF we are using a version of mysql that supports FULLTEXT
	$query[] = 'CREATE TABLE PaperFootnotes(paperFootnotesId bigint(20) NOT NULL AUTO_INCREMENT, paperId bigint(20) NOT NULL, pageNumber bigint(20), content text, fulltext(content), PRIMARY KEY(paperFootnotesId), FOREIGN KEY(paperId) REFERENCES Papers(paperId))engine=InnoDB';
}else{
	$query[] = 'CREATE TABLE PaperFootnotes(paperFootnotesId bigint(20) NOT NULL AUTO_INCREMENT, paperId bigint(20) NOT NULL, pageNumber bigint(20), content text, PRIMARY KEY(paperFootnotesId), FOREIGN KEY(paperId) REFERENCES Papers(paperId))engine=InnoDB';
}

$query[] = 'CREATE TABLE PaperReferences(paperReferenceId bigint(20) NOT NULL AUTO_INCREMENT,paperId bigint(20) NOT NULL, start bigint(20) NOT NULL, stop bigint(20) NOT NULL, PRIMARY KEY(paperReferenceId), FOREIGN KEY(paperId) REFERENCES Papers(paperId))';
$query[] = 'CREATE TABLE PaperComments(paperCommentId bigint(20) NOT NULL AUTO_INCREMENT,paperId bigint(20) NOT NULL,userId bigint(20) NOT NULL,content longtext, start bigint(20), stop bigint(20), parentId bigint(20), PRIMARY KEY(paperCommentId), FOREIGN KEY(userId) REFERENCES Users(userId), FOREIGN KEY(paperId) REFERENCES Papers(paperId))';
$query[] = 'CREATE TABLE PaperLinks(paperLinkId bigint(20) NOT NULL AUTO_INCREMENT,paperId bigint(20) NOT NULL,paperLinkURL mediumtext,paperLinkName mediumtext, PRIMARY KEY(paperLinkId), FOREIGN KEY(paperId) REFERENCES Papers(paperId))';
$query[] = 'CREATE TABLE Categories(categoryId bigint(20) NOT NULL AUTO_INCREMENT,categoryName varchar(255),categoryIdRef bigint(20) NOT NULL, PRIMARY KEY(categoryId))';
$query[] = 'CREATE TABLE UserCategoriesAssoc(userCategoryAssocId bigint(20) NOT NULL AUTO_INCREMENT, userId bigint(20) NOT NULL, categoryId bigint(20) NOT NULL, PRIMARY KEY(userCategoryAssocId), FOREIGN KEY(userId) REFERENCES Users(userId), FOREIGN KEY(categoryId) REFERENCES Categories(categoryId))';
$query[] = 'CREATE TABLE PaperCategoryAssoc(paperCategoryAssocId bigint(20) NOT NULL AUTO_INCREMENT,paperId bigint(20) NOT NULL,categoryId bigint(20) NOT NULL, PRIMARY KEY(paperCategoryAssocId), FOREIGN KEY(paperId) REFERENCES Papers(paperId), FOREIGN KEY(categoryId) REFERENCES Categories(categoryId))';
$query[] = 'CREATE TABLE PeerReviews(peerReviewId bigint(20) NOT NULL AUTO_INCREMENT,paperId bigint(20) NOT NULL,userId bigint(20) NOT NULL, comment longtext, startTime bigint(20) NOT NULL, isComplete int(1) NOT NULL DEFAULT 0, isEndorsed int(1), PRIMARY KEY(peerReviewId), FOREIGN KEY(userId) REFERENCES Users(userId), FOREIGN KEY(paperId) REFERENCES Papers(paperId))';
$query[] = 'CREATE TABLE Settings(settingId int NOT NULL AUTO_INCREMENT, name varchar(255), numValue bigint(20), textValue longtext, PRIMARY KEY(settingId))';
$query[] = 'CREATE TABLE Taxonomy(taxId bigint(20) NOT NULL AUTO_INCREMENT,taxName varchar(255) NOT NULL, taxDepth bigint(20) NOT NULL, parentId bigint(20) NOT NULL, PRIMARY KEY(taxId))';
$query[] = 'CREATE TABLE UsersPapersAssoc(userPaperAssocId bigint(20) NOT NULL AUTO_INCREMENT, userId bigint(20) NOT NULL, paperId bigint(20) NOT NULL, COIDisclosure text, PRIMARY KEY(userPaperAssocId), FOREIGN KEY(userId) REFERENCES Users(userId), FOREIGN KEY(paperId) REFERENCES Papers(paperId))';
$query[] = 'CREATE TABLE UsersPapersEndorseAssoc(userPaperEndorseId bigint(20) NOT NULL AUTO_INCREMENT, userId bigint(20) NOT NULL, paperId bigint(20) NOT NULL, COIDisclosure text, PRIMARY KEY(userPaperEndorseId), FOREIGN KEY(userId) REFERENCES Users(userId), FOREIGN KEY(paperId) REFERENCES Papers(paperId))';

					foreach($query as $mysqlQuery){
	                                        if(!$result = mysqli_query($link, $mysqlQuery)){
	                                                die('<p>Error!</p><p>' . mysqli_error($link) . '</p>');
	                                        }
	                                        unset($query); unset($row); unset($result);
					}


					//CREATE Admin User Account:
	                                $password = password_hash($password, PASSWORD_DEFAULT, ['cost' => $hashCost ]);
					date_default_timezone_set('UTC'); // UTC is our default time for database inserts. Convert to default timezone on print/echo.
	                                $joinDate = date("Y-m-d H:i:s");
					$uid = hash('sha256', mt_rand(0,100000) . rand(1,1000) . $joinDate . $userName);

					$query = 'INSERT INTO Users(userName,password,joinDate,lastLogin,emailAddress,userFullName,uniqueID,admin) VALUES (?,?,?,?,?,?,?,?)';
				        $stmt = mysqli_stmt_init($link);
				        if(mysqli_stmt_prepare($stmt, $query)){
						$admin=1;
				                mysqli_stmt_bind_param($stmt, "sssssssi", $userName, $password, $joinDate, $joinDate, $email, $userFullName, $uid, $admin);
				                mysqli_stmt_execute($stmt);
						$userNumber = mysqli_insert_id($link);
				                mysqli_stmt_close($stmt);
				                unset($query);
				        }else{ //if stmt_prepare fails:
				                die("Error!<br>" . mysqli_error($link));
				    	}


					$query = 'INSERT INTO Settings(name,numValue,textValue) VALUES (?,?,?)';
                                        $stmt = mysqli_stmt_init($link);
                                        if(mysqli_stmt_prepare($stmt, $query)){

						$name = 'paperReviews';
                                                $numValue = 10;
                                                $textValue = '';

                                                mysqli_stmt_bind_param($stmt, "sis", $name, $numValue, $textValue);
                                                mysqli_stmt_execute($stmt);

						$name = 'paperApprovals';
                                                $numValue = 5;
						$textValue = '';

						mysqli_stmt_bind_param($stmt, "sis", $name, $numValue, $textValue);
                                                mysqli_stmt_execute($stmt);

						$name = 'loginAttempts';
                                                $numValue = 5;
                                                $textValue = '';

                                                mysqli_stmt_bind_param($stmt, "sis", $name, $numValue, $textValue);
                                                mysqli_stmt_execute($stmt);

						$name = 'supportEmail';
                                                $numValue = 0;
                                                $textValue = '';

                                                mysqli_stmt_bind_param($stmt, "sis", $name, $numValue, $textValue);
                                                mysqli_stmt_execute($stmt);

						$name = 'registration';
                                                $numValue = 1;
                                                $textValue = '';

                                                mysqli_stmt_bind_param($stmt, "sis", $name, $numValue, $textValue);
                                                mysqli_stmt_execute($stmt);

                                                $name = 'language';
                                                $numValue = $langNum;
                                                $textValue = $lang;

                                                mysqli_stmt_bind_param($stmt, "sis", $name, $numValue, $textValue);
                                                mysqli_stmt_execute($stmt);

                                                $name = 'timezone';
                                                $numValue = 0;
                                                $textValue = $timezone;

                                                mysqli_stmt_bind_param($stmt, "sis", $name, $numValue, $textValue);
                                                mysqli_stmt_execute($stmt);

                                                $name = 'loginLockTime';
                                                $numValue = 60;
                                                $textValue = '';

                                                mysqli_stmt_bind_param($stmt, "sis", $name, $numValue, $textValue);
                                                mysqli_stmt_execute($stmt);

						$name = 'reviewSkipLockTime';
                                                $numValue = 24;
                                                $textValue = '';

                                                mysqli_stmt_bind_param($stmt, "sis", $name, $numValue, $textValue);
                                                mysqli_stmt_execute($stmt);

						$name = 'reviewSkipCount';
                                                $numValue = 5;
                                                $textValue = '';

                                                mysqli_stmt_bind_param($stmt, "sis", $name, $numValue, $textValue);
                                                mysqli_stmt_execute($stmt);

						$name = 'reviewTime';
                                                $numValue = 14;
                                                $textValue = '';

                                                mysqli_stmt_bind_param($stmt, "sis", $name, $numValue, $textValue);
                                                mysqli_stmt_execute($stmt);

						$name = 'enableCSPRNG';
                                                $numValue = 0;
                                                $textValue = '';

                                                mysqli_stmt_bind_param($stmt, "sis", $name, $numValue, $textValue);
                                                mysqli_stmt_execute($stmt);


                                                mysqli_stmt_close($stmt);
                                                unset($query);
                                        }else{ //if stmt_prepare fails:
                                                die("Error!<br>" . mysqli_error($link));
                                        }


					mysqli_close($link);

					$_SESSION['user_number']=$userNumber;
			                $_SESSION['user_name']=$userName;

					$phase=2;


				} // END last error check

			}else{ // ELSE IF MySQLi was selected
				// More database types to be added here later
				echo '<h1>Database type not found.</h1>';
			} // END ELSE IF MySQLi was selected

		} // END IF there are no errors

	} // END IF the form was submited
} //end of Phase 1

if($phase==1){ //check again seperately for second run; do NOT combine with above
?>
<p>Welcome to the OpenResearch installer. Running this script will install OpenResearch on your server. Before you start, setup a table and user with access to that table in your database.</p>
<p><b>Warning! This will overwrite and delete any previous installations of OpenResearch!</b></p>
<br><br>

<?php if((isset($message)) && (strlen($message)>0)){ ?>
<br>
<div style="border: solid 2px red; color: red; font-weight: bold;">
<?php echo $message; ?>
</div><br><br>
<?php } ?>

<form action="./install.php" method="POST" autocomplete="off">
<input type="hidden" name="phase" value="1">
<input type="hidden" name="lang" value="<?php echo $langNum; ?>">
<label>Database URL: <input type="text" value="localhost" name="dbURL"></label><br>
<label>Database User's Name: <input type="text" name="dbUN"></label><br>
<label>Database User's Password: <input type="password" name="dbPW"></label><br>
<label>Database Name: <input type="text" name="dbName"></label><br>
<label>Database Type: <select name="dbType"><option value="1">MySQLi</option></select></label><br><br>
<hr />
<p>Setup your admin account on OpenResearch too:</p>
<label>Full Name: <input type="text" name="userFullName"></label><br>
<label>Username: <input type="text" name="adminUserName"></label><br>
<label>Email Address: <input type="text" name="adminEmail"></label><br>
<label>Password: <input type="password" name="adminPW"></label><br>
<label>Password (Again): <input type="password" name="adminPW2"></label><br><br>

<label>Timezone: <select name="timezone">
<?php
/*
Thanks very much to Toland Hon from StackOverflow for the timezone code below! (I've only slightly modified it.)
https://stackoverflow.com/questions/1727077/generating-a-drop-down-list-of-timezones-with-php
*/

$timeList = DateTimeZone::listIdentifiers(DateTimeZone::ALL);

$timezone_offsets = array();

foreach( $timeList as $timezone )
{
	$tz = new DateTimeZone($timezone);
        $timezone_offsets[$timezone] = $tz->getOffset(new DateTime);
}

// sort timezone by offset
asort($timezone_offsets);

$timezone_list = array();

foreach( $timezone_offsets as $timezone => $offset )
{
        $offset_prefix = $offset < 0 ? '-' : '+';
        $offset_formatted = gmdate( 'H:i', abs($offset) );

        $pretty_offset = "UTC${offset_prefix}${offset_formatted}";

        $timezone_list[$timezone][0] = "(${pretty_offset}) $timezone";
	$timezone_list[$timezone][1] = $timezone;
}

foreach($timezone_list as $timezone){
	echo '<option value="' . $timezone[1] . '">' . $timezone[0] . '</option>' . "\n";
}
?>
</select></label>

<hr /><br>
<p>Security Settings:</p>
<?php
// Hash Cost Calculator from:
// https://secure.php.net/manual/en/function.password-hash.php
$timeTarget = 0.05; // 50 milliseconds

$cost = 8;
do {
    $cost++;
    $start = microtime(true);
    password_hash("test", PASSWORD_BCRYPT, ["cost" => $cost]);
    $end = microtime(true);
} while (($end - $start) < $timeTarget);

?>

<p><label>Hash Cost: <input type="number" value="<?php echo $cost; ?>" name="hashCost"></label><br><?php echo 'Recommended Hash Cost: ' . $cost; ?></p>
<br><br>
<input type="submit" value="Install">
</form>
<?php
} //end of phase 1

/////////////////////////////// PHASE 2 ////////////////////////

if($phase==2){

$config = '<?php' . "\n" .
'if(isset($includeCheck)){ // IF included from another php source' . "\n\n" .
'///////////////////////////////////////////////////////////////////////' . "\n" .
'//' . "\n" .
'//  config.php' . "\n" .
'//  Sets important variables for all of OpenResearch. Change these whenever' . "\n" .
'//    you need to.' . "\n" .
'//' . "\n" .
'///////////////////////////////////////////////////////////////////////' . "\n" .
'///////////////////////////////////////////////////////////////////////////////////' . "\n" .
'//' . "\n" .
'//  Important Settings:' . "\n" .
'//' . "\n" .
'//  Change the variable below when you move "dbConfig.php" so that OpenResearch can find it!' . "\n" .
'//  Absolute Location of dbConfig.php. It usually is in the root of the installation.' . "\n" .
'//' . "\n" .
'//  Example:    $configPath = \'/home/user/private_html/\';' . "\n" .
'                $configPath = \'\';' . "\n" .
'//' . "\n" .
'//  Location to your user\'s uploads. By default it is $root . \'/uploads/\' where $root is' . "\n" .
'//  a link to your root directory. If you want to change where things are uploaded, change' . "\n" .
'//  this. For example: $uploads = \'/var/private_html/uploads/\';' . "\n" .
'//  Make sure www-data has read, write, and execute permissions on your uploads folder!' . "\n" .
'		$uploads = $root . \'/uploads/\';' . "\n" .
'//' . "\n" .
'//  The database type. 1 = MySQLi; More options  will be available as soon as I can' . "\n" .
'//  get to it!' . "\n" .
'		$dbType = ' . $dbType . '; //database type' . "\n" .
'//' . "\n" .
'//  Set force_https to true if you want to force it to use a SSL' . "\n" .
'//  certficate to encrypt data. If you have https setup for your host,' . "\n" .
'//  setting this to \'true\' will increase your security.' . "\n" .
'//' . "\n" .
'//  Example:   $forceHttps = false;' . "\n" .
'                $forceHttps = false;' . "\n" .
'//' . "\n" .
'//  Set language file to use for SIGN:' . "\n" .
'                $lang = \'' . $lang . '\';' . "\n" .
'//' . "\n" .
'//  The code below removes possible attack vectors, but may also cause older browsers' . "\n" .
'//  to not work. It is recommended that you use the latest browser and set this to 1,' . "\n" .
'//  but if you can\'t for some reason, just change the 1 to a 0.' . "\n" .
'//' . "\n" .
'//  Example: ini_set(\'session.cookie_httponly\', 0);' . "\n" .
'                ini_set(\'session.cookie_httponly\', 1);' . "\n" .
'//' . "\n" .
'//  The code below forces SIGN to show error messages. This is generally a bad idea' . "\n" .
'//  unless you are developing new features. It is recommended to have this set to' . "\n" .
'//  false.' . "\n" .
'//' . "\n" .
'//  Example: $showErrors = false;' . "\n" .
'                $showErrors = true;' . "\n" .
'//' . "\n" .
'//  php.ini setting to force sessions to use cookies for id\'s. The only alternative is' . "\n" .
'//  GET data which is terrible for security. It is recommended to set this to 1.' . "\n" .
'//' . "\n" .
'//  Example: ini_set(\'session.use_only_cookies\', 0);' . "\n" .
'                ini_set(\'session.use_only_cookies\', 1);' . "\n" .
'//' . "\n" .
'///////////////////////////  Do NOT edit below this line!!  ///////////////////////' . "\n\n\n" .
'}else{ // ELSE IF included from another php source
        if(headers_sent()){
                if(isset($root)){
                        die(include($root . \'/404.html\'));
                }else{
                        die(include(\'404.html\'));
                }
        } // if headers were somehow already sent, just die to a custom 404 page

        header(\'HTTP/1.1 404 Not Found\'); // send headers if not sent already

        if(isset($root)){
                die(include($root . \'/404.html\'));
        }else{
                // die to custom 404 page
                die(include(\'404.html\'));
        }
} // END ELSE IF included from another php source
?>';


$dbConfig ='<?php
///////////////////////////////////////////////////////////////////////' . "\n" .
'//' . "\n" .
'//  dbConfig.php' . "\n" .
'//  Sets very important variables for database connections and other' . "\n" .
'//    security critical items.' . "\n" .
'//' . "\n" .
'///////////////////////////////////////////////////////////////////////' . "\n" .
'///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////' . "\n" .
'//' . "\n" .
'//   FOR SECURITY THIS PAGE SHOULD BE MOVED OUT OF THE WWW FACING DIRECTORY AND config.php SHOULD BE EDITED TO ITS NEW LOCATION!!!' . "\n" .
'//' . "\n" .
'///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////' . "\n" .
'
if(isset($includeCheck)){ // IF included from another php source

$dbSource = \'' . $dbURL . '\'; // database location/url
$dbName = \'' . $dbName . '\'; // database name
$dbUsername = \'' . $dbUN . '\'; // username for the database
$dbPassword = \'' . $dbPW . '\'; // database password

// Settings for password hashes:
$hashOptions = [
 	\'options\' => [\'cost\' => ' . $hashCost . '],
        \'algo\' => PASSWORD_DEFAULT,
        \'hash\' => null
];


}else{ // ELSE IF included from another php source
	if(headers_sent()){
		if(isset($root)){
			die(include($root . \'/404.html\'));
		}else{
			die(include(\'404.html\'));
		}
	} // if headers were somehow already sent, just die to a custom 404 page

	header(\'HTTP/1.1 404 Not Found\'); // send headers if not sent already

	if(isset($root)){
		die(include($root . \'/404.html\'));
	}else{
		// die to custom 404 page
		die(include(\'404.html\'));
	}
} // END ELSE IF included from another php source
?>';

echo '<p>Attempting to create "config.php" and "dbConfig.php" files....</p>';

$myfile = @fopen("config.php", "w");

if((file_exists("config.php")) && ($myfile!=false)){ // IF config.php was correctly made
	fwrite($myfile, $config);
	fclose($myfile);

	$myfile2 = @fopen("dbConfig.php", "w");

	if((file_exists("dbConfig.php")) && ($myfile2!=false)){ // IF dbConfig.php was correctly made
		fwrite($myfile2, $dbConfig);
		fclose($myfile2);

		echo '<p>config.php and dbConfig.php files made. You can now continue the install.</p>';

	}else{ // ELSE IF dbConfig.php was correctly made
	?>
	<p>Error auto-generating dbConfig.php. Please manually create it using the code below.</p>

	<p>Copy the contents of the box labeled "dbConfig.php" and use it to make a new file called "dbConfig.php".
	You can put the file in the OpenResearch root directory, but for safety, it is best to place it in a non web-facing directroy.
	Then go to OpenResearch's config.php file in root. Change the variable "configPath" to the absolute directory of "dbConfig.php".</p>

	<p>Click the link below when you finish. <b>Warning, you can't regenerate this! Don't lose it!</b></p>

	<b>dbConfig.php:</b><br>
	<textarea cols="135" rows="25"><?php echo $dbConfig; ?></textarea><br><br>
	<?php
	} // END ELSE IF dbConfig.php was correctly made

}else{ // ELSE IF config.php was correctly made
?>
<p>Error auto-generating config.php and dbConfig.php. Please manually create it using the code below.</p>

<p>Below is the code for your new configuration files. Copy the contents of the one labeled "config.php" and use it to
 make a new file called "config.php". It should be made in the OpenResearch root directory.</p>

<p>Then, copy the contents of the one labeled "dbConfig.php" and use it to make a new file called "dbConfig.php".
You can put the file in the OpenResearch root directory, but for safety, it is best to place it in a non web-facing directroy.
Then go to OpenResearch's config.php file in root. Change the variable "configPath" to the absolute directory of "dbConfig.php".</p>

<p>Click the link below when you finish. <b>Warning, you can't regenerate this! Don't lose it!</b></p>

<p><b>config.php:</b></p>
<textarea cols="135" rows="25">
<?php echo $config; ?>
</textarea>

<p>&nbsp;</p>

<p><b>dbConfig.php:</b></p>
<textarea cols="135" rows="25">
<?php echo $dbConfig; ?>
</textarea><br><br>

<?php } // END ELSE IF config.php was correctly made
?>

<form action="./install.php" method="POST">
<input type="hidden" name="phase" value="3">
<input type="hidden" name="lang" value="<?php echo $langNum; ?>">
<input type="submit" value="Click to Continue Install">
</form>

<?php
} //end of phase 2

if($phase==3){?>
<p>Setup complete! <a href="./index.php">Click here</a> to go to see your new installation! Don't forget to delete your install.php file!</p>
<?php
}
?>
</body>
</html>
