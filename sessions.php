<?php
/////////////////////////////////////////////////////////////////
//
//  sessions.php
//  Sets sessions up and manages them.
//
/////////////////////////////////////////////////////////////////

if(isset($includeCheck)){
// IF included from another source

class Sessions{

	public function __construct(){
		session_start();

		if(!$this->check('user_number')){ // IF no user is logged in
			$this->setup(); // default guest account
		}

		if(!$this->check('user_ip')){ // IF no ip has been set
			$this->set('user_ip',$this->getIP()); // set IP
		}else{
			if($this->get('user_ip') != $this->getIP()){
				// IP doesn't match session data. Possible session hijack
				$this->end();
			}
		}
	}

	public function set($key, $value){ // set a session value
		$_SESSION[$key] = $value;
	}

	public function get($key){ // get a session value
		if(isset($_SESSION[$key])){
			return $_SESSION[$key];
		}else{
			return NULL;
		}
	}

	public function check($key){ // if session key exists
		if(isset($_SESSION[$key])){
			return true;
		}else{
			return false;
		}
	}

	public function setup($un = 'Guest', $num = -1){ // setup user data, default to guest
		@session_regenerate_id(true); // Reset session id to mitigate session hijacking. Suppress errors as some servers don't support this command.
		$this->set('user_name', $un);
		$this->set('user_number', $num);
	}

	public function end(){
		$_SESSION = array(); // kills all session variables

		if(ini_get('session.use_cookies')){ // if the config file uses session cookies
			$params = session_get_cookie_params(); // get the cookie parameters and make the cookie expire
			setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
		}

		@session_destroy(); // destroy everything left
		$this->setup();  // reset session as guest
	}

	public function getIP() { // Get the user's ip address
		foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
			if(array_key_exists($key, $_SERVER) === true){
				foreach (explode(',', $_SERVER[$key]) as $ip) {
					if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
						return $ip;
					}
				}
		}
	}
}

} // END Sessions Class

}else{ // ELSE IF included from another php source
if(headers_sent()){ die(include("404.html")); } // If headers were somehow already sent, just die to a custom 404 page
header("HTTP/1.1 404 Not Found"); // Send headers if not sent already
die(include("404.html")); // Die to custom 404 page
} // END ELSE IF included from another source

?>
