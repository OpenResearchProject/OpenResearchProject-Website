<?php
///////////////////////////////////////////////////////////////////////////////////
//
//  index.php (root)
//  The first file loaded for everything in OpenResearch. Everything goes through this.
//
///////////////////////////////////////////////////////////////////////////////////

$root = getcwd(); //Root directory that this file is placed in. Used for other documents to find root.
$includeCheck = 1;  //A check to be sure all future files are loaded through here.

if(!file_exists('config.php')){ // IF OpenResearch hasn't been setup yet or config file is missing
        if(!file_exists('install.php')){ // IF the installer is missing, we may have already installed and corrupted something
                die('config.php file missing!');
        }else{
                // The installer is still there. We probably haven't installed yet.
                die(include('install.php'));
        }
} // END IF OpenResearch hasn't been setup yet or config file is missing

if(!include_once('config.php')){
	die('Missing config.php file!');
}

if(@($_SERVER['HTTPS'] != 'on') && ($forceHttps) && (!headers_sent())){
    header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    exit();
}

if($forceHttps){ //if we are using https, force session cookies to use it!
	ini_set('session.cookie_secure',1);
}

if($showErrors){
	error_reporting(E_ALL);
	ini_set('display_errors',1);
}

// check our database configuration file:
if(!file_exists($configPath . 'dbConfig.php')){
	die('Unable to find dbConfig.php. Please check your config.php settings!<br>A system administrator is most likely working on the website. Please check again later!');
}

switch($dbType){ // Swap out databases depending on given dbType
        case 1: $dbTypeText = 'mySQLiClass.php'; break;
        default: $dbTypeText = 'mySQLiClass.php'; break;
}

// Include our database class file. $dbType is from dbConfig.php
if(!@include_once($dbTypeText)){
	die('Unable to find mySQLiClass.php.<br>A system administrator is most likely working on the website. Please check back later!');
}

$sql = new sqlOR($configPath, $root);
unset($configPath); // No longer needed
unset($dbType); unset($dbTypeText);

if(!include_once('sessions.php')){
	die('Unable to find sessions.php.<br>A system administrator is most likely working on the website. Please check back later!');
}

if(!include_once('user.php')){
	die('Unable to find user.php.<br>A system administrator is most likely working on the website. Please check back later!');
}

$user = new User($sql);

// include our language file:
if(!include_once('./lang/' . $user->getLanguage())){
        die('Missing language file.');
}

if(!include_once('message.php')){
	die('Unable to find message.php.<br>A system administrator is most likely working on the website. Please check back later!');
}

$message = new Message();


if((isset($_GET['p'])) && (is_numeric($_GET['p']))){ // IF the user is loading a page
        $p = (int)$_GET['p'];

        switch($p){
		case 1: include( $root . '/pages/register.php'); break;
                case 2: include( $root . '/pages/login.php'); break;
                case 3: include( $root . '/pages/logout.php'); break;
		case 4: include( $root . '/pages/userLanding.php'); break; // User profile landing
		case 5: include( $root . '/pages/searchLanding.php'); break; // Search landing for papers
		case 6: include( $root . '/pages/advancedSearch.php'); break; //Full search results
		case 7: include( $root . '/pages/paperLanding.php'); break; // View a single paper's metadata and comments
		case 8: include( $root . '/pages/addPaper.php'); break; // User paper uploader
		case 9: include( $root . '/pages/comments.php'); break;
		case 10: include($root . '/pages/myPapers.php'); break; // User Paper view page
		case 11: include($root . '/pages/showPaper.php'); break; // Display a single, selected paper
		case 12: include($root . '/pages/changeInfo.php'); break; //display change info stuff
		case 13: include($root . '/pages/changePassword.php'); break; //display change password
		case 14: include($root . '/pages/review.php'); break; // Review paper page for users
		case 15: include($root . '/pages/userLocalization.php'); break; // User localization settings
		case 21: include( $root . '/pages/userDiscipline.php'); break; // Temporary Number; User Discipline Selector

                default: include($root . '/pages/home.php'); break;
        }

}else{ // ELSE IF the user is loading a page
	if((isset($_GET['i'])) && (is_numeric($_GET['i']))){ // IF the user is loading an image
		include($root . '/pages/image.php');
	}else{ // ELSE IF the user is loading an image
		if(isset($_GET['aac'])){ // IF the user is confirming thier email (Account Activation Code)

			$aacResult = $sql->checkActivationCode($_GET['aac']);

			include($root . '/pages/html/header.html');
			if($aacResult){
				$message->showMessage(10);
			}else{
				$message->showMessage(11);
			}
			include($root . '/pages/html/footer.html');

		}else{ // ELSE IF the user is confirming their email
			if((isset($_GET['a'])) && (is_numeric($_GET['a'])) && ($_GET['a']==(int)$_GET['a'])){ // IF the user is loading the admin panel
				if($user->getNumber() > -1){ // IF the user is logged in
					if($user->getAdmin()){ // IF the current user is an admin
						if($user->getReauth('adminPanel')){ // IF the user has reauthenticated
							if($user->updateAdminSessionTime()){ // Check the admin's session time and update it for extra 5 minutes

								$adminPage = (int)$_GET['a'];

								switch($adminPage){
									case 0: include( $root . '/pages/adminPages/logout.php'); break;
									case 1: include( $root . '/pages/adminPages/reauth.php'); break;
									case 2: include( $root . '/pages/adminPages/landing.php'); break;
									case 3: include( $root . '/pages/adminPages/orSettings.php'); break;
									case 4: include( $root . '/pages/adminPages/orLocalization.php'); break;
									case 5: include( $root . '/pages/adminPages/userSettings.php'); break;
									case 6: include( $root . '/pages/adminPages/addUser.php'); break;

									case 8: include( $root . '/pages/adminPages/paperDisciplines.php'); break;
										case 10: include( $root . '/pages/adminPages/paperAddDiscipline.php'); break;
										case 11: include( $root . '/pages/adminPages/paperRemoveDiscipline.php'); break;
									case 9: include( $root . '/pages/adminPages/paperReviewSettings.php'); break;
									default: include( $root . '/pages/adminPages/reauth.php'); break;
								}
							}else{
								$user->killReauth('adminPanel');
								$loginErrors[] = lang::admin_reauth;
								include( $root . '/pages/adminPages/reauth.php');
							}
						}else{ // ELSE IF the user has reauthenticated
							include( $root . '/pages/adminPages/reauth.php');
						} // END ELSE IF the user has reauthenticated
					}else{ // ELSE IF the current user is an admin
						die(lang::genericError);
					} // END ELSE IF the current user is an admin
				}else{ // ELSE IF the user is logged in
					die(lang::genericError);
				} // END ELSE IF the user is logged in
			}else{ // ELSE IF the user is loading the admin panel
			        include($root . '/pages/home.php');
			} // END ELSE IF the user is loading the admin panel
		} // END ELSE IF the user is confirming their email
	} // END ELSE IF the user is loading an image
} // END ELSE IF the user is loading a page

// Close and cleanup any open database connection
if(isset($stmt)){ @mysqli_stmt_close($stmt); unset($stmt); }
// Supressed errors on mysqli_stmt_close since it may already be closed, but the variable may still exist
if(isset($query)){ unset($query); }
mysqli_close($sql->getLink());

?>
