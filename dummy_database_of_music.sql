-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 27, 2016 at 08:30 PM
-- Server version: 5.6.30-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `OpenResearch`
--

-- --------------------------------------------------------

--
-- Table structure for table `Categories`
--

CREATE TABLE IF NOT EXISTS `Categories` (
  `categoryId` bigint(20) NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(255) DEFAULT NULL,
  `categoryIdRef` bigint(20) NOT NULL,
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `Categories`
--

INSERT INTO `Categories` (`categoryId`, `categoryName`, `categoryIdRef`) VALUES
(1, 'Rock', 0),
(2, 'Ska', 0),
(5, 'Pop', 0),
(6, 'Metal', 1);

-- --------------------------------------------------------

--
-- Table structure for table `GroupComments`
--

CREATE TABLE IF NOT EXISTS `GroupComments` (
  `groupCommentId` bigint(20) NOT NULL AUTO_INCREMENT,
  `groupId` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `content` text,
  PRIMARY KEY (`groupCommentId`),
  KEY `groupId` (`groupId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Groups`
--

CREATE TABLE IF NOT EXISTS `Groups` (
  `groupId` bigint(20) NOT NULL AUTO_INCREMENT,
  `groupName` varchar(255) DEFAULT NULL,
  `groupLeader` bigint(20) NOT NULL,
  PRIMARY KEY (`groupId`),
  KEY `groupLeader` (`groupLeader`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `IPs`
--

CREATE TABLE IF NOT EXISTS `IPs` (
  `ipId` bigint(20) NOT NULL AUTO_INCREMENT,
  `ipAddress` text NOT NULL,
  `userId` bigint(20) NOT NULL,
  PRIMARY KEY (`ipId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `IPs`
--

INSERT INTO `IPs` (`ipId`, `ipAddress`, `userId`) VALUES
(1, '192.168.56.1', 1),
(2, '192.168.56.1', 2),
(3, '192.168.56.1', 3),
(4, '192.168.56.1', 4),
(5, '192.168.56.1', 5),
(6, '192.168.56.1', 6),
(7, '192.168.56.1', 7),
(8, '192.168.56.1', 8),
(9, '192.168.56.1', 9),
(10, '192.168.56.1', 10),
(11, '192.168.56.1', 11);

-- --------------------------------------------------------

--
-- Table structure for table `Journal`
--

CREATE TABLE IF NOT EXISTS `Journal` (
  `journalId` bigint(20) NOT NULL AUTO_INCREMENT,
  `journalName` varchar(255) NOT NULL,
  `publisherId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`journalId`),
  KEY `publisherId` (`publisherId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Logins`
--

CREATE TABLE IF NOT EXISTS `Logins` (
  `loginId` bigint(20) NOT NULL AUTO_INCREMENT,
  `ipAddress` text NOT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `lastLoginAttempt` bigint(20) DEFAULT NULL,
  `loginAttempts` int(11) DEFAULT NULL,
  PRIMARY KEY (`loginId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Logins`
--

INSERT INTO `Logins` (`loginId`, `ipAddress`, `userId`, `lastLoginAttempt`, `loginAttempts`) VALUES
(1, '192.168.56.1', 1, 1461806834, 0);

-- --------------------------------------------------------

--
-- Table structure for table `PaperCategoryAssoc`
--

CREATE TABLE IF NOT EXISTS `PaperCategoryAssoc` (
  `paperCategoryAssocId` bigint(20) NOT NULL AUTO_INCREMENT,
  `paperId` bigint(20) NOT NULL,
  `categoryId` bigint(20) NOT NULL,
  PRIMARY KEY (`paperCategoryAssocId`),
  KEY `paperId` (`paperId`),
  KEY `categoryId` (`categoryId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `PaperCategoryAssoc`
--

INSERT INTO `PaperCategoryAssoc` (`paperCategoryAssocId`, `paperId`, `categoryId`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 2),
(4, 4, 6),
(5, 5, 6),
(6, 6, 1),
(7, 7, 2),
(8, 8, 1),
(9, 9, 1),
(10, 10, 5),
(11, 11, 5),
(12, 12, 2),
(13, 13, 2),
(14, 14, 2),
(15, 15, 2),
(16, 16, 1),
(17, 17, 1),
(18, 18, 5),
(19, 19, 5),
(20, 20, 1),
(21, 21, 1),
(22, 22, 6);

-- --------------------------------------------------------

--
-- Table structure for table `PaperComments`
--

CREATE TABLE IF NOT EXISTS `PaperComments` (
  `paperCommentId` bigint(20) NOT NULL AUTO_INCREMENT,
  `paperId` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `content` longtext,
  `start` bigint(20) DEFAULT NULL,
  `stop` bigint(20) DEFAULT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`paperCommentId`),
  KEY `userId` (`userId`),
  KEY `paperId` (`paperId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `PaperComments`
--

INSERT INTO `PaperComments` (`paperCommentId`, `paperId`, `userId`, `content`, `start`, `stop`, `parentId`) VALUES
(1, 1, 1, 'test', NULL, NULL, NULL),
(2, 1, 1, 'test', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `PaperFootnotes`
--

CREATE TABLE IF NOT EXISTS `PaperFootnotes` (
  `paperFootnotesId` bigint(20) NOT NULL AUTO_INCREMENT,
  `paperId` bigint(20) NOT NULL,
  `pageNumber` bigint(20) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`paperFootnotesId`),
  KEY `paperId` (`paperId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `PaperLinks`
--

CREATE TABLE IF NOT EXISTS `PaperLinks` (
  `paperLinkId` bigint(20) NOT NULL AUTO_INCREMENT,
  `paperId` bigint(20) NOT NULL,
  `paperLinkURL` mediumtext,
  `paperLinkName` mediumtext,
  PRIMARY KEY (`paperLinkId`),
  KEY `paperId` (`paperId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `PaperReferences`
--

CREATE TABLE IF NOT EXISTS `PaperReferences` (
  `paperReferenceId` bigint(20) NOT NULL AUTO_INCREMENT,
  `paperId` bigint(20) NOT NULL,
  `start` bigint(20) NOT NULL,
  `stop` bigint(20) NOT NULL,
  PRIMARY KEY (`paperReferenceId`),
  KEY `paperId` (`paperId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Papers`
--

CREATE TABLE IF NOT EXISTS `Papers` (
  `paperId` bigint(20) NOT NULL AUTO_INCREMENT,
  `paperName` varchar(255) DEFAULT NULL,
  `paperURL` varchar(255) DEFAULT NULL,
  `paperPublishDate` date DEFAULT NULL,
  `publisherId` bigint(20) DEFAULT NULL,
  `abstract` text,
  `sponsor` varchar(255) DEFAULT NULL,
  `digObjIdUrl` varchar(255) DEFAULT NULL,
  `digObjId` varchar(255) DEFAULT NULL,
  `copyRightOwner` varchar(255) DEFAULT NULL,
  `copyRightDate` date DEFAULT NULL,
  `paperText` mediumtext,
  `isEndorsed` int(1) NOT NULL DEFAULT '1',
  `views` bigint(20) DEFAULT NULL,
  fulltext (`paperName`,`abstract`),
  PRIMARY KEY (`paperId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `Papers`
--

INSERT INTO `Papers` (`paperId`, `paperName`, `paperURL`, `paperPublishDate`, `publisherId`, `abstract`, `sponsor`, `digObjIdUrl`, `digObjId`, `copyRightOwner`, `copyRightDate`, `paperText`, `isEndorsed`, `views`) VALUES
(1, 'Test Title', '', '2014-12-30', 0, 'Test Abstract', '', '', '', '', '1970-01-01', 'Test Text', 1, NULL),
(2, 'Pressure Drop', '', '1969-12-31', 0, '"Pressure Drop" is a song recorded in 1969 by The Maytals for producer Leslie Kong. The song appears on their 1970 album Monkey Man (released in Jamaica by Beverley''s Records) and From the Roots (released in the UK by Trojan Records).', '', '', '', '', '1970-01-01', 'It is you, you, you\r\n(Oh yeah)\r\nIt is you, you, you\r\n(Oh yeah)\r\n\r\nI said a pressure drop, oh pressure, oh pressure''s gonna drop on you\r\n(Oh yeah)\r\nI said a pressure drop, oh pressure, oh pressure''s gonna drop on you\r\n(Oh yeah)\r\n\r\nI said a when it drops oh you gonna feel it\r\nOh that you were doin'' it wrong, wrong, wrong\r\n(Oh yeah)\r\nNow when it drops oh you gonna feel it\r\n(Oh yeah)\r\nThat you were doin'' it wrong and how\r\n\r\nI said a when it drops you gonna feel it\r\nThat you''ve been doin'' it wrong\r\nNow when it drop, drop you gonna feel it\r\nNow you doin'' that wrong\r\n\r\nNow when it drop, drop feel it you make the wrong move\r\nNow when it drop, drop you gonna feel it\r\nNow you''re doin'' it wrong, wrong, wrong\r\n\r\nPressure drop, oh pressure, oh pressure''s gonna drop on you\r\n(Oh yeah)\r\nPressure drop, oh pressure, oh pressure''s gonna drop on you\r\n(Oh yeah)\r\n\r\nNow, now, now when it drops on your dirty little head\r\nWhere you gonna go? When it''s gonna drop on you\r\n(Oh yeah)\r\nWhen it drop on you''re gonna feel it\r\nWhat you''re doing is wrong, wrong, wrong\r\n(Oh yeah)\r\n\r\nPressure, pressure, pressure\r\n(Oh yeah)\r\n\r\n\r\n\r\nRead more: The Clash - Pressure Drop Lyrics | MetroLyrics', 1, NULL),
(3, '54-46 That''s My Number', '', '1968-12-31', 0, '"54-46 (That''s My Number)" is a song by Fred "Toots" Hibbert, recorded by Toots & the Maytals and originally released on the Beverly''s label in Jamaica and the Pyramid label in the UK.', '', '', '', '', '1970-01-01', 'I said yeah, listen what I say\r\nI said hear me now, listen what I say\r\n\r\nI said stick it up mister\r\nHear what I say sir\r\nGet your hands in the air, sir\r\nAnd you will get no hurt mister\r\nDo what I say sir\r\nJust what I mean sir\r\nGet your hands in the air, sir\r\nAnd you will get no hurt mister\r\n\r\nSecond thing they say I must join the office\r\n\r\nThird thing they say, son - give me your number\r\nHe''d say "What''s your number?"\r\nI don''t answer\r\nHe''d say "What''s your number, man?"\r\nHe''d say "What''s your number now?"\r\nI said "5446, that''s my number..."\r\n\r\nFirst verses again\r\n\r\n5446 was my number\r\n\r\nStick it up, mister!\r\nHear what I say, sir, yeah...\r\nGet your hands in the air, sir!\r\nAnd you will get no hurt, mister, no no no\r\n\r\nI said yeah\r\nWhat did I say?\r\nDon''t you hear? I said yeah\r\nListen to what I say\r\n\r\nDo you believe I would take something with me\r\nAnd give it to the police man?\r\nI wouldn''t do that) And if I do that, I would\r\nsay "sir, put the charge on me"\r\nI wouldn''t do that\r\nNo, I wouldn''t do that\r\nI''m not a fool to hurt myself\r\nSo I was innocent of what they done to me\r\nThey was wrong\r\nListen to me, they were wrong\r\n\r\nGive it to me one time\r\nGive it to me two time\r\nGive it to me three time\r\nGive it to me four time\r\n\r\n54 46 was my number\r\nRight now, someone else has that number', 1, NULL),
(4, 'Am I Evil?', '', '1980-12-31', 0, '"Am I Evil?" is a song by British heavy metal band Diamond Head released on their 1980 debut album Lightning to the Nations. The song was written by vocalist Sean Harris and guitarist Brian Tatler and released on Happy Face Records, the band''s own label.', '', '', '', '', '1970-01-01', 'My mother was a witch, she was burned alive.\r\nThankless little bitch, for the tears I cried.\r\nTake her down now, don''t want to see her face\r\nAll blistered and burnt, can''t hide my disgrace.\r\n\r\nTwenty-seven, everyone was nice.\r\nGotta see ''em make ''em pay the price.\r\nSee their bodies out on the ice.\r\nTake my time.\r\n\r\nAm I evil? Yes I am.\r\nAm I evil? I am man, yes I am.\r\n\r\nAs I watched my mother die, I lost my head.\r\nRevenge now I sought, to break with my bread.\r\nTaking no chances, you come with me.\r\nI''ll split you to the bone, help set you free.\r\n\r\nTwenty-seven, everyone was nice.\r\nGotta see ''em make ''em pay the price.\r\nSee their bodies out on the ice.\r\nTake my time.\r\n\r\nAm I evil? Yes I am.\r\nAm I evil? I am man, yes I am.\r\n\r\nOn with the action now, I''ll strip your pride.\r\nI''ll spread your blood around, I''ll see you ride.\r\nYour face is scarred with steel, wounds deep and neat.\r\nLike a double dozen before ya, smells so sweet.\r\n\r\nAm I evil? Yes I am.\r\nAm I evil? I am man.\r\n\r\nI''ll make my residence, I''ll watch your fire.\r\nYou can come with me, sweet desire.\r\nMy face is long forgot, my face not my own.\r\nSweet and timely whore, take me home.\r\n\r\nAm I evil? Yes I am.\r\nAm I evil? I am man.\r\n\r\nMy soul is longing for, await my heir.\r\nSent to avenge my mother, sweep myself.\r\nMy face is long forgot, my face not my own.\r\nSweet and timely whore, take me home.\r\n\r\nAm I evil? Yes I am.\r\nAm I evil? I am man.\r\n\r\nAm I evil? Yes I am.\r\nAm I evil? I am man, yeah.', 1, NULL),
(5, 'Shoot Out the Lights', '', '1980-01-02', 0, '"Shoot Out the Lights" is a single by heavy metal band Diamond Head released in 1980 by Happy Face Records, the band''s own label. It was a single A-side with "Shoot Out the Lights" and "Helpless" as the B-side, and was only available on 7", without a picture sleeve in order to reduce production costs.', '', '', '', '', '1970-01-01', '"Shoot Out The Lights"\r\n\r\nCity lights the water as the snow forgets the sky\r\nAnd we''re staring at the bridges just like every other night\r\n\r\nWe''ve been watching for a miracle\r\nWe''re praying for a sign\r\nWhen the cure is made of poison then it''s hard to rest your eyes\r\n\r\nIf it''s time\r\nOh\r\nLord\r\nShoot out the lights\r\nShoot out the lights\r\n\r\nHeavens shake with anger and the clouds\r\nThey curse the ground\r\nAnd I''m screaming for an answer but the darkness blinds me now\r\n\r\nWe will not survive on misery\r\nWe will fill ourselves with love\r\nWe are searching\r\nWe are hopeful\r\nWe are anything but lost\r\n\r\nIf it''s time\r\nOh\r\nLord\r\nShoot out the lights\r\nShoot out the lights\r\nShoot out the lights\r\nShoot out the lights\r\nShoot out the lights\r\n\r\nI see the stars in black and white\r\nI pray to God most every night\r\n\r\nI see the stars in black and white\r\nAnd I pray to God most every night\r\n(Shoot out the lights)\r\n\r\nI see the stars in black and white\r\n(Shoot out the lights)\r\n(Shoot out the lights)\r\nAnd I pray to God most every night\r\n(Shoot out the lights)\r\n(Shoot out the lights)\r\n\r\nAnd I see the stars\r\nI see the stars\r\n(Shoot out the lights)\r\nI see the stars\r\nI see the stars\r\n(Shoot out the lights)\r\nI see the stars most every night\r\n(Shoot out the lights)\r\n\r\n(I see the stars in black and white)\r\nShoot out the lights\r\nShoot out the lights\r\n(I pray to God most every night)\r\nShoot out the lights\r\n\r\nShoot out the lights\r\n(I see the stars in black and white)\r\nShoot out the lights\r\nShoot out the lights\r\n(I pray to God most every night)\r\nShoot out the lights\r\n\r\n(I see the stars)\r\n(I see the stars)\r\n\r\nI see the stars in black and white\r\nAnd I pray to God\r\nI pray to God', 1, NULL),
(6, '747/Shoe Glue', '', '1993-12-31', 0, 'Don''t Know How to Party is the third full-length album by The Mighty Mighty Bosstones, which was released in 1993. Don''t Know How to Party was The Mighty Mighty Bosstones'' major label debut on Mercury Records, their first venture away from their original label Taang! Records. The album reached #187 on the Billboard 200, and spawned several singles, including the Bosstones fan favorite—"Someday I Suppose"', '', '', '', '', '1970-01-01', 'Down the street, around the corner,\r\nover the bridge, that dirty water,\r\nCollege campus, mighty minds\r\nA reactor right behind\r\nPassed the candy factory, Salvation Army, fire station\r\nThrough the square, I hate that place Up the stairs\r\nNext the doors, they never work I never let that bother me\r\nI hope some worker''s not disgruntled\r\nI hope there''s something there for me\r\nAn old man reads the paper loudly everyday now for a year\r\nStamp machines and wanted posters here again,\r\nagain I''m here\r\n737 almost everyday\r\nNice to know that someone hears us\r\nIt''s good to know somebody''s there\r\nTakin the time to say hello\r\nTaking the time to show you care\r\nWrite again and thanks again\r\nFrom the bottom of my heart See you soon and till then\r\n737 is a real big part\r\n737 almost everyday\r\nShoe glue\r\nWhat can I do? A man''s not well dressed\r\nif his shoes are a mess Rock and Roll\r\nI''ve got a hole\r\nRight in my sole\r\nLet''s rock!\r\nIt''s fucking my walk\r\nAnd soaking my sock\r\nWho knew?\r\nIt''s not stopping my step or stepping my stop\r\nWe''ve got it up and we won''t let it drop\r\nBeer here,\r\nDon''t wanna see clear\r\nI see a point in wrecking the joint\r\nWe''re here to quench our thirst a bit\r\nBut we won''t get the worst of it\r\nTurn it up more than a notch\r\nLike a punch to the face or a kick to the crotch\r\nAll night never ending benefiting from a bender\r\nIf nothing''s worrying you that''s the key\r\n''cause nothing''s worrying me\r\nAnd nothing''s worrying me\r\n\r\nRead more: Mighty Mighty Bosstones - 737/ Shoe Glue Lyrics | MetroLyrics', 1, NULL),
(7, 'Hell of a Hat', '', '1994-10-04', 0, 'Question the Answers is the fourth album by The Mighty Mighty Bosstones, released on October 4, 1994', '', '', '', '', '1970-01-01', 'That''s a hell of a hat that you''re wearing\r\nIt goes real well with the earring\r\nFive through the nose, bold and daring\r\nYou''ve got to pardon me if I''m staring\r\n\r\nMan, I really dig your shoes\r\nFat fade and fresh tattoos\r\nBet the females can''t refuse\r\nAnd if I''m staring, please excuse\r\n\r\nSharpest motherf-- in the joint\r\nThe other motherf-- stop and point\r\nThe gear you wear seems complete\r\nWhy you gotta pack the heat?\r\n\r\nWe question one accessory\r\nDon''t think the piece is necessary\r\nWe agree you''re dressed to kill\r\nBut wonder if you will?\r\n\r\nHell of a hat, you''re sporting, Jack\r\nDiamond cuff links and a sweet tie tack\r\nThe pocket watch must''ve set you back\r\nYou''re the man, man, you''re the mack\r\n\r\nPinstriped number, me oh my, that''s rude\r\nAnd that''s a hell of an attitude\r\nYou made the scene and you set the mood\r\nI''m only starin'' ''cause I''m nervous, dude\r\n\r\nSharpest motherf--, we repeat\r\nThe other motherf-- can''t compete\r\nThe clothes you chose are stylish, son\r\nWhy you gotta wear the gun?\r\n\r\nWe question one accessory\r\nDon''t think the piece is necessary\r\nWe agree you''re dressed to kill\r\nWonder if you will?', 1, NULL),
(8, 'You Shook Me All Night Long', '', '1980-10-04', 0, '"You Shook Me All Night Long" is an AC/DC song from the album, Back in Black. The song also reappeared on their later album Who Made Who. It is AC/DC''s first single to feature Brian Johnson as the lead singer and reached number 35 on the USA''s Hot 100 pop singles chart in 1980. The single was re-released internationally in 1986, following the release of the album Who Made Who. The re-released single in 1986 contains the B-side(s): B1. "She''s Got Balls" (Live, Bondi Lifesaver ''77); B2. "You Shook Me All Night Long" (Live ''83 – 12-inch maxi-single only).', '', '', '', '', '1970-01-01', 'She was a fast machine she kept her motor clean\r\nShe was the best damn woman that I ever seen\r\nShe had the sightless eyes telling me no lies\r\nKnocking me out with those American thighs\r\nTaking more than her share\r\nHad me fighting for air\r\nShe told me to come but I was already there\r\nCause the walls start shaking\r\nThe earth was quaking\r\nMy mind was aching\r\nAnd we were making it\r\nAnd you shook me all night long\r\nYeah you shook me all night long\r\n\r\nI''m working double time on the seduction line\r\nShe''s one of a kind she''s just mine all mine\r\nWanted no applause it''s just another course\r\nMade a meal outta me\r\nAnd come back for more\r\nHad to cool me down to take another round\r\nNow I''m back in the ring to take another swing\r\nCause the walls were shaking the earth was quaking\r\nMy mind was aching\r\nAnd we were making it\r\nAnd you shook me all night long\r\nYeah you shook me all night long\r\nKnocked me out I said you\r\nShook me all night long\r\nYou had me shaking and you\r\nShook me all night long\r\nYeah you shook me\r\nWhen you took me\r\n\r\nYou really took me and you\r\nShook me all night long\r\nYou shook me all night long\r\nYeah, yeah, you shook me all night long\r\nYou really got me and you\r\nShook me all night long\r\nYeah you shook me\r\nYeah you shook me\r\nAll night long', 1, NULL),
(9, 'Back in Black', '', '1980-12-21', 0, '"Back in Black" is a song by AC/DC, appearing as the first track on side two of their 1980 album of the same name. Known for its opening guitar riff, the song was AC/DC''s tribute to their former singer Bon Scott.', '', '', '', '', '1970-01-01', 'Back in black\r\nI hit the sack\r\nI''ve been too long I''m glad to be back\r\nYes, I''m let loose\r\nFrom the noose\r\nThat''s kept me hanging about\r\nI''ve been looking at the sky\r\n''Cause it''s gettin'' me high\r\nForget the hearse ''cause I never die\r\nI got nine lives\r\nCat''s eyes\r\nAbusin'' every one of them and running wild\r\n\r\n''Cause I''m back\r\nYes, I''m back\r\nWell, I''m back\r\nYes, I''m back\r\nWell, I''m back, back\r\nWell, I''m back in black\r\nYes, I''m back in black\r\n\r\nBack in the back\r\nOf a Cadillac\r\nNumber one with a bullet, I''m a power pack\r\nYes, I''m in a bang\r\nWith a gang\r\nThey''ve got to catch me if they want me to hang\r\n''Cause I''m back on the track\r\nAnd I''m beatin'' the flack\r\nNobody''s gonna get me on another rap\r\nSo look at me now\r\nI''m just makin'' my play\r\nDon''t try to push your luck, just get out of my way\r\n\r\n''Cause I''m back\r\nYes, I''m back\r\nWell, I''m back\r\nYes, I''m back\r\nWell, I''m back, back\r\nWell, I''m back in black\r\nYes, I''m back in black\r\n\r\nWell, I''m back, yes I''m back\r\nWell, I''m back, yes I''m back\r\nWell, I''m back, back\r\nWell I''m back in black\r\nYes I''m back in black\r\n\r\nHo yeah\r\nOh yeah\r\nYes I am\r\nOh yeah, yeah oh yeah\r\nBack in now\r\nWell I''m back, I''m back\r\nBack, (I''m back)\r\nBack, (I''m back)\r\nBack, (I''m back)\r\nBack, (I''m back)\r\nBack\r\nBack in black\r\nYes I''m back in black\r\nOut of the sight', 1, NULL),
(10, 'Sledgehammer', '', '1986-04-25', 0, '"Sledgehammer" is a song written, composed and performed by English rock musician Peter Gabriel, which appeared on his 1986 album So, produced by Gabriel and Daniel Lanois. It hit No. 1 in Canada on 21 July 1986, where it spent four weeks; No. 1 on the Billboard Hot 100 chart in the United States on 26 July 1986;[4] and No. 4 on the UK Singles Chart, thanks in part to a popular and influential music video.', '', '', '', '', '1970-01-01', 'you could have a steam train\r\nif you''d just lay down your tracks\r\nyou could have an aeroplane flying\r\nif you bring your blue sky back\r\n\r\nall you do is call me\r\nI''ll be anything you need\r\n\r\nyou could have a big dipper\r\ngoing up and down, all around the bends\r\nyou could have a bumper car, bumping\r\nthis amusement never ends\r\n\r\nI want to be your sledgehammer\r\nwhy don''t you call my name\r\noh let me be your sledgehammer\r\nthis will be my testimony\r\nshow me round your fruitcage\r\n''cos I will be your honey bee\r\nopen up your fruitcage\r\nwhere the fruit is as sweet as can be\r\n\r\nI want to be your sledgehammer\r\nwhy don''t you call my name\r\nyou''d better call the sledgehammer\r\nput your mind at rest\r\nI''m going to be-the sledgehammer\r\nthis can be my testimony\r\nI''m your sledgehammer\r\nlet there be no doubt about it\r\n\r\nsledge sledge sledgehammer\r\n\r\nI''ve kicked the habit\r\nshed my skin\r\nthis is the new stuff\r\nI go dancing in, we go dancing in\r\noh won''t you show for me\r\nand I will show for you\r\nshow for me, I will show for you\r\nyea, yeah, yeah, yeah, yeah, yeah, I do mean you\r\nonly you\r\nyou''ve been coming through\r\ngoing to build that powerr\r\nbuild, build up that power, hey\r\nI''ve been feeding the rhythm\r\nI''ve been feeding the rhythm\r\ngoing to feel that power, build in you\r\ncome on, come on, help me do\r\nyeah, yeah, yeah, yeah, yeah, yeah, yeah, yeah, you\r\nI''ve been feeding the rhythm\r\nI''ve been feeding the rhythm\r\nit''s what we''re doing, doing\r\nall day and night', 1, NULL),
(11, 'Solsbury Hill', '', '1977-12-31', 0, '"Solsbury Hill" is a song by English musician Peter Gabriel about a spiritual experience atop Solsbury Hill in Somerset, England. Gabriel wrote the song after his departure from the progressive rock band Genesis, of which he had been the lead singer since its inception. The song was his debut single.', '', '', '', '', '1970-01-01', 'Climbing up on Solsbury Hill\r\nI could see the city light\r\nWind was blowing, time stood still\r\nEagle flew out of the night\r\nHe was something to observe\r\nCame in close, I heard a voice\r\nStanding stretching every nerve\r\nHad to listen had no choice\r\nI did not believe the information\r\n[I] just had to trust imagination\r\nMy heart going boom boom boom\r\n"Son, " he said "Grab your things,\r\nI''ve come to take you home."\r\n\r\nTo keepin'' silence I resigned\r\nMy friends would think I was a nut\r\nTurning water into wine\r\nOpen doors would soon be shut\r\nSo I went from day to day\r\nTho'' my life was in a rut\r\n"Till I thought of what I''d say\r\nWhich connection I should cut\r\nI was feeling part of the scenery\r\nI walked right out of the machinery\r\nMy heart going boom boom boom\r\n"Hey" he said "Grab your things\r\nI''ve come to take you home."\r\n\r\nWhen illusion spin her net\r\nI''m never where I want to be\r\nAnd liberty she pirouette\r\nWhen I think that I am free\r\nWatched by empty silhouettes\r\nWho close their eyes but still can see\r\nNo one taught them etiquette\r\nI will show another me\r\nToday I don''t need a replacement\r\nI''ll tell them what the smile on my face meant\r\nMy heart going boom boom boom\r\n"Hey" I said "You can keep my things,\r\nthey''ve come to take me home."', 1, NULL),
(12, 'Rat Race', '', '1980-10-01', 0, 'More Specials is the second album by ska band The Specials, released in October 1980. The album expanded the 2 tone sound of their self-titled debut to include lounge music and other influences. It featured collaborations with The Go-Go''s members Belinda Carlisle, Charlotte Caffey, and Jane Wiedlin; Rhoda Dakar from The Bodysnatchers; and Lee Thompson from Madness. The lyrics, like the previous album, were often intensely political.', '', '', '', '', '1970-01-01', 'You''re working at your leisure to learn the things you''ll need\r\nThe promises you make tomorrow will carry no guarantee\r\nI''ve seen your qualifications, you''ve got a Ph.D.\r\nI''ve got one art O level, it did nothing for me.\r\n\r\nWorking for the rat race\r\nYou know you''re wasting your time\r\nWorking for the rat race\r\nYou''re no friend of mine\r\n\r\nYou plan your conversation to impress the college bar\r\nJust talking about your mother and daddy''s Jaguar\r\nWear your political t-shirt and sacred college scarf\r\nDiscussing the world''s situation but just for a laugh\r\n\r\nYou''ll be working for the rat race\r\nYou know you''re wasting your time\r\nWorking for the rat race\r\nYou''re no friend of mine\r\n\r\nWorking for the rat race\r\nYou know you''re wasting your time\r\nWorking for the rat race\r\nYou''re no friend of mine\r\n\r\nJust working at your leisure to learn the things you won''t need\r\nThe promises you make tomorrow will carry no guarantee\r\nI''ve seen your qualifications, you''ve got a Ph.D.\r\nI''ve got an art O level, it did nothing for me.\r\n\r\nWorking for the rat race\r\nYou know you''re wasting your time\r\nWorking for the rat race\r\nYou''re no friend of mine', 1, NULL),
(13, 'Concrete Jungle', '', '1979-10-01', 0, 'The Specials is the debut album by British ska revival band the Specials.\r\n\r\nReleased on 19 October 1979 on Jerry Dammers'' 2 Tone label, the album is seen by some as the defining moment in the UK ska scene. Produced by Elvis Costello, the album captures the disaffection and anger felt by the youth of the UK''s "concrete jungle"—a phrase borrowed from Bob Marley''s 1972 album Catch a Fire but equally apposite used here to describe the grim, violent inner cities of 1970s Britain.', '', '', '', '', '1970-01-01', 'I''m going out tonight\r\nI don''t know if I''ll be alright\r\nEveryone wants to hurt me\r\nBaby danger in the city\r\n\r\nI have to carry a knife\r\nBecause there''s people threatening my life\r\nI can''t dress just the way I want\r\nI''m being chased by the national front\r\n\r\nConcrete jungle, animals are after me\r\nConcrete jungle, it ain''t safe on the streets\r\nConcrete jungle, glad I got my mates with me\r\n\r\nI won''t fight for a cause\r\nDon''t want to change the law\r\nLeave me alone, just leave me alone\r\nI want to get out on my own\r\n\r\nI''m walking home tonight\r\nI only walk where there''s lots of lights\r\nIn the alleys and the doorways\r\nSome throw a bottle right in your face\r\n\r\nConcrete jungle, animals are after me\r\nConcrete jungle, it ain''t safe on the streets\r\nConcrete jungle, glad I got my mates with me\r\n\r\nI''m walking home tonight\r\nI only walk where there''s lots of lights\r\nIn the alleys and the doorways\r\nSome throw a bottle right in your face\r\n\r\nI won''t fight for a cause\r\nI don''t want to change the law\r\nLeave me alone, just leave me alone\r\nI want to get out on my own\r\n\r\nConcrete jungle, animals are after me\r\nConcrete jungle, it ain''t safe on the streets\r\nConcrete jungle, glad I got my mates with me', 1, NULL),
(14, 'Special Brew', '', '2016-12-31', 0, 'Bad Manners are an English ska band led by frontman Buster Bloodvessel. Early appearances included Top of The Pops and the live film documentary, Dance Craze.[1]\r\n\r\nThey were at their most popular during the early 1980s, during a period when other ska revival bands such as Madness, The Specials and The Selecter filled the charts. Bad Manners spent 111 weeks in the UK Singles Chart between 1980 and 1983,[2] and they also achieved chart success with their first four studio albums with Gosh It''s... Bad Manners, Loonee Tunes! and Ska ''n'' B being their biggest hits.', '', '', '', '', '1970-01-01', 'I love you, yes i do\r\n''cause i know that you love me too\r\nI love you, yes i do\r\ngona spend all my money on you\r\n\r\neveryday when i say, that i''m not gona take anymore\r\nits ok, don''t go away, i feel bad when you''re closing the door\r\n\r\nI love you\r\n\r\nNeed some more, to restore all the feeling that i get from you\r\ni want more, give me more, all i want is a barrell of you\r\n\r\nI love you, yes i do\r\n''cause i know that you love me too\r\ni love you, yes i do\r\ngona spend all my money on you\r\nwoh woh woh woh\r\n\r\nI don''t care, when they stare, at the way that i''m always with you\r\nwe''re a pair, it''s not fair when they say we''re a special brew!\r\nwoh woh woh woh\r\n\r\nI love you\r\n\r\nhey...hey...hey...hey...\r\nhey...hey...hey...hey...\r\nhey...hey...hey...hey...\r\nhey...hey...hey...hey...\r\n\r\nEveryday, when i say, that i''m not gona take anymore\r\nit''s ok, don''t go away, i feel bad when you''re closing the door\r\n\r\nNeed some more, to restore all the feeling that i get from you\r\ni want more, give me more, all i want is a barrell of you\r\nwoh woh woh woh woh woh woh\r\n\r\nI love you, yes i do\r\n''cause i know that you love me too!!!', 1, NULL),
(15, 'Lip Up Fatty', '', '1980-12-31', 0, '"Lip Up Fatty" is a single released by the ska band Bad Manners in June 1980, which reached No. 15[1] in the UK Singles Chart. It is one of a number of songs by Bad Manners about ''being fat'', (a reference to the round figure of frontman, Buster Bloodvessel). According to Bloodvessel "Lip Up Fatty" was an expression used at his school "to tell people to shut up". Its signature melodic lines were a simple but careful blend of brass instruments and lead harmonica theme, played by Alan Sayag (Winston Bazoomies).', '', '', '', '', '1970-01-01', 'Lip up fatty, ah lip up fatty, for the reggae,\r\nLip up fatty, ah lip up fatty, for the reggae,\r\n\r\nListen to the music, shuffle up your feet,\r\nListen to the music of the fatty beat.\r\n\r\nMoving with the rhythm, sweating with the heat,\r\nMoving with the rhythm of the fatty beat.\r\n\r\nLip up fatty, ah lip up fatty, for the reggae,\r\nTrumpeet.\r\n\r\nListen to the music, shuffle up your feet,\r\nListen to the music of the fatty beat.\r\n\r\nLip up fatty, ah lip up fatty, for the reggae,\r\nTrumpeet.\r\n\r\nLip up fatty, ah lip up fatty, for the reggae,\r\nDon?t call me fat man,\r\nLip up fatty, ah lip up fatty, for the reggae,\r\nFat man don?t like you.', 1, NULL),
(16, 'Doubleback', '', '1990-01-01', 0, '"Doubleback" is a song by ZZ Top from their album Recycler, which was featured in the film Back to the Future Part III. The band had a cameo in the movie playing a hillbilly music version of the song along with some local musicians. The regular version of the song plays over the credits.', '', '', '', '', '1970-01-01', 'I got shot through a space not long ago,\r\nI thought I knew the place so well.\r\nIt wasn''t the same, now it goes to show,\r\nSometime you never can tell.\r\n\r\nI''m lookin'' high and low, don''t know where to go,\r\nI got to double back, my friend.\r\nThe only way to find, what I left behind\r\nI got to double back again, double back again.\r\n\r\nYou know I''m movin on in this fine machine,\r\nRollin'' on through the night.\r\nSeein'' things like I''ve never seen\r\nAnd it''s taking me outta sight.\r\n\r\nLookin'' high and low, don''t know where to go,\r\nI got to double back, my friend.\r\nThe only way to find, what I left behind\r\nI got to double back again, double back again.\r\n\r\nIt''s got me up and down,\r\nI been lost and found,\r\nDown in a deep dark hole.\r\nLooks like my luck has changed,\r\nI been rearranged\r\nAnd I''m coming out on a roll.\r\n\r\nLookin'' high and low, don''t know where to go,\r\nI got to double back, my friend.\r\nThe only way to find, what I left behind\r\nI got to double back again, double back again.', 1, NULL),
(17, 'Sharp Dressed Man', '', '1983-12-31', 0, '"Sharp Dressed Man" is a song performed by ZZ Top from their 1983 album Eliminator. The song was produced by band manager Bill Ham, and recorded and mixed by Terry Manning. Pre-production recording engineer Linden Hudson was very involved in the early stages of this song''s production.[1]', '', '', '', '', '1970-01-01', 'Clean shirt, new shoes\r\nAnd I don''t know where I am goin'' to.\r\nSilk suit, black tie,\r\nI don''t need a reason why.\r\nThey come runnin'' just as fast as they can\r\nCause every girl crazy ''bout a sharp dressed man.\r\n\r\nGold watch, diamond ring,\r\nI ain''t missin'' not a single thing.\r\nAnd cufflinks, stick pin,\r\nWhen I step out I''m gonna do you in.\r\nThey come runnin'' just as fast as they can\r\n''Cause every girl crazy ''bout a sharp dressed man.\r\n\r\nTop coat, top hat,\r\nAnd I don''t worry coz my wallet''s fat.\r\nBlack shades, white gloves,\r\nLookin'' sharp lookin'' for love.\r\nThey come runnin'' just as fast as they can\r\n''Cause every girl crazy ''bout a sharp dressed man.', 1, NULL),
(18, 'Smooth Criminal', '', '1988-10-21', 0, '"Smooth Criminal" is the seventh single from American recording artist Michael Jackson''s 1987 Bad album. The song contains a fast-paced beat intertwined with Jackson''s lyrics about a woman named Annie, who has been violently attacked in her apartment by a "smooth" assailant. First broadcast on television as a video in early October, it was released as a single on October 21, 1988, and peaked at number seven on the Billboard Hot 100.[3] It was re-released on April 10, 2006, as a part of the Visionary: The Video Singles box set. The re-released Visionary single charted at number 19 in the UK. The piece is one of Jackson''s signature songs, and has appeared on numerous greatest hits albums, including Number Ones, The Essential Michael Jackson, Michael Jackson: The Ultimate Collection, King of Pop, This Is It, and Immortal.', '', '', '', '', '1970-01-01', 'As he came into the window\r\nIt was the sound of a crescendo\r\nHe came into her apartment\r\nHe left the bloodstains on the carpet\r\nShe ran underneath the table\r\nHe could see she was unable\r\nSo she ran into the bedroom\r\nShe was struck down, it was her doom\r\n\r\nAnnie, are you ok?\r\nSo, Annie are you ok\r\nAre you ok, Annie\r\nAnnie, are you ok?\r\nSo, Annie are you ok\r\nAre you ok, Annie\r\nAnnie, are you ok?\r\nSo, Annie are you ok?\r\nAre you ok, Annie?\r\nAnnie, are you ok?\r\nSo, Annie are you ok, are you ok Annie?\r\n\r\nAnnie, are you ok?\r\nSo, Annie are you ok?\r\nAre you ok, Annie?\r\nAnnie, are you ok?\r\nSo, Annie are you ok?\r\nAre you ok, Annie?\r\nAnnie, are you ok?\r\nSo, Annie are you ok?\r\nAre you ok, Annie?\r\nYou''ve been hit by\r\nYou''ve been hit by\r\nA smooth criminal\r\n\r\nSo they came into the outway\r\nIt was Sunday - what a black day\r\nMouth to mouth\r\nResuscitation\r\nSounding heartbeats - intimidation\r\n\r\nAnnie, are you ok?\r\nSo, Annie are you ok\r\nAre you ok, Annie\r\nAnnie, are you ok?\r\nSo, Annie are you ok\r\nAre you ok, Annie\r\nAnnie, are you ok?\r\nSo, Annie are you ok?\r\nAre you ok, Annie?\r\nAnnie, are you ok?\r\nSo, Annie are you ok, are you ok Annie?\r\n\r\n(Then you\r\nRan into the bedroom)\r\n(You were struck down)\r\n(It was your doom)\r\n\r\n(You''ve been struck by\r\nA smooth criminal)\r\n\r\nOkay, I want everybody to clear\r\nThe area right now!\r\n\r\nAaow!\r\n(Annie are you ok?)\r\nI don''t know!\r\n(Will you tell us that you''re ok?)\r\nI don''t know!\r\n(There''s a sign in the window)\r\nI don''t know!\r\n(That he struck you - a crescendo Annie)\r\nI don''t know!\r\n(He came into your apartment)\r\nI don''t know!\r\n(Left the bloodstains on the carpet)\r\nI don''t know why baby!\r\n(Then you ran into the bedroom)\r\nI don''t know\r\n(You were struck down)\r\n(It was your doom - Annie!)\r\n\r\nDag gone it - baby!\r\n(Will you tell us that you''re ok?)\r\nDag gone it - baby!\r\n(There''s a sign in the window)\r\nDag gone it - baby!\r\n(That he struck you - a crescendo Annie)\r\nHoo! Hoo!\r\n(He came into your apartment)\r\nDag gone it!\r\n(Left the bloodstains on the carpet)\r\nHoo! Hoo! Hoo!\r\n(Then you ran into the bedroom)\r\nDag gone it!\r\n\r\nAaow!!!', 1, NULL),
(19, 'Bad', '', '1987-08-07', 0, '"Bad" is a song by an American singer Michael Jackson. "Bad" was released by Epic Records on September 7, 1987, as the second single from Jackson''s third major-label and seventh overall studio album of the same name. The song was written, composed, and co-produced by Jackson and produced by Quincy Jones. Jackson stated that the song was influenced by a real-life story he had read about.', '', '', '', '', '1970-01-01', 'Your butt is mine\r\nGonna tell you right\r\nJust show your face\r\nIn broad daylight\r\nI''m telling you\r\nOn how I feel\r\nGonna hurt your mind\r\nDon''t shoot to kill\r\nCome on, come on,\r\nLay it on me\r\nAll right...\r\n\r\nI''m giving you\r\nOn count of three\r\nTo show your stuff\r\nOr let it be...\r\nI''m telling you\r\nJust watch your mouth\r\nI know your game\r\nWhat you''re about\r\n\r\nWell they say the sky''s the limit\r\nAnd to me that''s really true\r\nBut my friend you have seen nothing\r\nJust wait ''til I get through...\r\n\r\nBecause I''m bad, I''m bad - come on\r\n(Bad bad - really, really bad)\r\nYou know I''m bad, I''m bad - you know it\r\n(Bad bad - really, really bad)\r\nYou know I''m bad, I''m bad - come on, you know\r\n(Bad bad - really, really bad)\r\nAnd the whole world has to\r\nAnswer right now\r\nJust to tell you once again,\r\nWho''s bad...\r\n\r\nThe word is out\r\nYou''re doin'' wrong\r\nGonna lock you up\r\nBefore too long,\r\nYour lyin'' eyes\r\nGonna tell you right\r\nSo listen up\r\nDon''t make a fight,\r\nYour talk is cheap\r\nYou''re not a man\r\nYou''re throwin'' stones\r\nTo hide your hands\r\n\r\nBut they say the sky''s the limit\r\nAnd to me that''s really true\r\nBut my friend you have seen nothing\r\nJust wait ''til I get through...\r\n\r\nBecause I''m bad, I''m bad - come on\r\n(Bad bad - really, really bad)\r\nYou know I''m bad, I''m bad - you know it\r\n(Bad bad - really, really bad)\r\nYou know I''m bad, I''m bad - you know it, you know\r\n(Bad bad - really, really bad)\r\nAnd the whole world has to answer right now\r\n(And the whole world has to answer right now)\r\nJust to tell you once again,\r\n(Just to tell you once again)\r\nWho''s bad...\r\n\r\nWe can change the world tomorrow\r\nThis could be a better place\r\nIf you don''t like what I''m sayin''\r\nThen won''t you slap my face..\r\n\r\nBecause I''m bad, I''m bad - come on\r\n(Bad bad - really, really bad)\r\nYou know I''m bad, I''m bad - you know it\r\n(Bad bad - really, really bad)\r\nYou know I''m bad, I''m bad - you know it, you know\r\n(Bad bad - really, really bad)\r\nWoo! Woo! Woo!\r\n(And the whole world has to answer right now\r\nJust to tell you once again...)\r\n\r\nYou know I''m bad, I''m bad - come on\r\n(Bad bad - really, really bad)\r\nYou know I''m bad, I''m bad - you know it - you know it\r\n(Bad bad - really, really bad)\r\nYou know, you know, you know - come on\r\n(Bad bad - really, really bad)\r\nAnd the whole world has to answer right now\r\n(And the whole world has to answer right now)\r\nJust to tell you\r\n(Just to tell you once again)\r\n\r\nYou know I''m smooth - I''m Bad - you know it\r\n(Bad bad - really, really bad)\r\nYou know I''m bad - I''m bad baby\r\n(Bad bad - really, really bad)\r\nYou know, you know, you know it - come on\r\n(Bad bad - really, really bad)\r\nAnd the whole world has to answer right now\r\n(And the whole world has to answer right now)\r\nWoo!\r\n(Just to tell you once again)\r\n\r\nYou know I''m bad, I''m bad - you know it\r\n(Bad bad - really, really bad)\r\nYou know I''m bad - you know - hoo!\r\n(Bad bad - really, really bad)\r\nYou know I''m bad - I''m bad - you know it, you know\r\n(Bad bad - really, really bad)\r\nAnd the whole world has to answer right now\r\n(And the whole world has to answer right now)\r\nJust to tell you once again...\r\n(Just to tell you once again)\r\nWho''s bad?', 1, NULL),
(20, 'Bomber', '', '1979-12-01', 0, '"Bomber" is a song by the English heavy metal band Motörhead, recorded and released in 1979 (see 1979 in music). It is the title track to their album Bomber and was released as a single peaking at 34 on the UK singles chart.\r\n\r\nThe single was released in the UK by Bronze Records as a 7" vinyl single with the first 20,000 copies pressed in blue vinyl and thereafter in black. The band promoted its release with an appearance on the BBC TV show Top of the Pops on 3 December.[1]', '', '', '', '', '1970-01-01', 'Ain''t a hope in hell\r\nNothin'' gonna bring us down\r\nThe way we fly\r\nFive miles off the ground\r\nBecause, we shoot to kill\r\nAnd you know we always will\r\nIt''s a Bomber\r\nIt''s a Bomber\r\nIt''s a Bomber\r\n\r\nScream a thousand miles\r\nFeel the black death rising moan\r\nFirestorm coming closer\r\nNapalm to the bone\r\nBecause, you know we do it right\r\nA mission every night\r\nIt''s a Bomber\r\nIt''s a Bomber\r\nIt''s a Bomber\r\n\r\nNo nightfighter\r\nGonna stop us getting through\r\nSirens make you shiver\r\nYou bet my aim is true\r\nBecause, we aim to please\r\nBring you to your knees\r\nIt''s a Bomber\r\nIt''s a Bomber\r\nIt''s a Bomber', 1, NULL),
(21, 'Ace of Spades', '', '1980-10-27', 0, '"Ace of Spades" is a song by British rock band Motörhead, released in 1980 as a single and the title track to the album Ace of Spades.', '', '', '', '', '1970-01-01', 'If you like to gamble, I tell you I''m your man,\r\nYou win some, lose some, all the same to me,\r\nThe pleasure is to play, makes no difference what you say,\r\nI don''t share your greed, the only card I need is\r\nThe Ace Of Spades\r\n\r\nPlaying for the high one, dancing with the devil,\r\nGoing with the flow, it''s all the game to me,\r\nSeven or Eleven, snake eyes watching you,\r\nDouble up or quit, double stake or split,\r\nThe Ace Of Spades\r\n\r\nYou know I''m born to lose, and gambling''s for fools,\r\nBut that''s the way I like it baby,\r\nI don''t wanna live for ever,\r\nAnd don''t forget the joker!\r\n\r\nPushing up the ante, I know you wanna see me,\r\nRead ''em and weep, the dead man''s hand again,\r\nI see it in your eyes, take one look and die,\r\nThe only thing you see, you know it''s gonna be,\r\nThe Ace Of Spades', 1, NULL),
(22, 'Modern School Project Analysis: A Rope of Sand', '', '2016-12-29', 0, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eu felis eu augue tempor efficitur. Aenean sed placerat odio, at tempor elit. Morbi nec eleifend ex. Pellentesque vitae ipsum condimentum, imperdiet purus quis, efficitur turpis. In vitae porta nulla. Nullam quis porta eros. Donec dignissim dolor vel sapien rhoncus volutpat. Sed nisl velit, accumsan id ligula at, feugiat malesuada magna. In eleifend nibh lectus, non eleifend nibh aliquam ac. Suspendisse sollicitudin varius nisl, et faucibus urna scelerisque eget. Pellentesque sodales tincidunt finibus. In commodo felis quis nisi varius auctor. Curabitur est purus, sollicitudin sed nisl ut, consectetur luctus nulla. Maecenas eu nisi id nisi sollicitudin semper. Donec suscipit fringilla ex, at convallis turpis accumsan non.', '', '', '', '', '1970-01-01', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sit amet nisi ante. Praesent non faucibus tortor. Donec vitae turpis vitae nisi consequat convallis dapibus eu est. Sed posuere auctor ante in vehicula. Morbi efficitur cursus pharetra. Nam volutpat vel sem sit amet volutpat. Aliquam diam sapien, tincidunt eu nunc at, consequat tristique ex. Aliquam augue metus, vulputate luctus dui eget, cursus eleifend augue. Etiam sollicitudin quis ante quis dignissim.\r\n\r\nVestibulum pharetra, augue feugiat scelerisque volutpat, ex mi blandit nulla, vel hendrerit mi mi at diam. Nam vestibulum ligula vel orci tincidunt sodales. Curabitur suscipit finibus purus, consectetur dapibus ligula mattis et. Suspendisse elementum molestie ex sed scelerisque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris facilisis augue vel lacus mattis, sit amet molestie libero eleifend. Pellentesque euismod tortor tellus, at aliquet felis tincidunt vel. Donec erat tellus, egestas sed tincidunt malesuada, ultrices in est. Proin dolor risus, posuere sed nisl vitae, scelerisque mattis neque. Duis eu sem non ex mollis tincidunt. Fusce dapibus, felis id dignissim luctus, sapien sem convallis lectus, varius rutrum lectus nisl ut lacus. Quisque congue eu augue et vulputate. Sed ac quam malesuada, rhoncus erat quis, placerat leo. Ut eget quam nunc.\r\n\r\nDonec consectetur blandit neque, vestibulum imperdiet velit semper id. Nunc ut tortor condimentum odio imperdiet tempor ut a ante. Duis in nunc vitae turpis fermentum fermentum. In fringilla eros ac tempus iaculis. Vestibulum ac arcu interdum, dignissim purus eu, scelerisque risus. Donec nec quam sem. Suspendisse ac velit nec justo rutrum tincidunt vitae vel urna. Cras ipsum neque, pulvinar quis enim a, rhoncus commodo erat. Praesent vitae massa augue.\r\n\r\nSuspendisse eget placerat urna. Ut at risus quis leo facilisis convallis eu et ante. Vestibulum luctus enim eu leo ornare faucibus. Quisque cursus justo eu nunc blandit viverra ac et mauris. Etiam dapibus dictum dolor non semper. Cras a porta mauris, in dictum urna. Nulla pulvinar, risus vitae rutrum dictum, eros dui fermentum tortor, ac varius risus dui id ex. Aliquam convallis risus erat, eget fringilla lectus interdum eu. Ut feugiat vitae orci eget viverra. Vivamus interdum bibendum risus et sodales. Proin porta diam ut orci feugiat, non mollis diam gravida.\r\n\r\nFusce eget metus hendrerit, accumsan turpis vitae, vestibulum lacus. Sed faucibus porta est, vel imperdiet diam pharetra et. Nullam aliquet enim quis vehicula porta. Nam a molestie justo. Proin aliquet diam in massa condimentum, in imperdiet nunc pharetra. Sed lobortis cursus ex. Quisque sagittis neque ut dolor fringilla, vel interdum est varius. Vivamus convallis risus non metus blandit, eu vestibulum ipsum ultricies. Mauris a sem non orci tristique interdum. Phasellus ut volutpat arcu. Integer et neque porttitor, suscipit justo vel, iaculis purus. Aliquam mi nisi, ultrices quis elementum nec, varius id nunc. Phasellus ac arcu odio.\r\n\r\nCurabitur mi lacus, sollicitudin bibendum fermentum et, hendrerit sit amet nisi. Aenean nec ligula diam. Aliquam viverra leo et magna 
finibus, in imperdiet nibh faucibus. Pellentesque vel ante eleifend, laoreet odio at, auctor libero. Nullam mollis nunc nec ex posuere sagittis ut eu erat. Curabitur vitae justo non turpis fermentum dapibus. Etiam euismod mattis tempus. Curabitur velit augue, dapibus ut porttitor molestie, condimentum at dui. Phasellus facilisis mollis libero. Nullam et metus rhoncus, facilisis lacus non, volutpat urna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vel fringilla metus. Pellentesque nisl erat, tincidunt vitae est a, rhoncus lacinia tortor. Fusce quis urna sed lectus efficitur accumsan non eget purus.\r\n\r\nSuspendisse a volutpat eros. Maecenas feugiat, mauris quis gravida ultrices, nibh sem ullamcorper erat, ut auctor libero lacus in dui. Nunc interdum quam vel dolor euismod consequat. Aliquam rutrum lacus felis, in euismod arcu vestibulum eget. Curabitur justo libero, vehicula ac dapibus ac, hendrerit sit amet massa. Maecenas eu nibh erat. Nunc ullamcorper erat ut massa mattis, nec vestibulum felis consequat. Curabitur accumsan vitae mauris id sagittis.\r\n\r\nNam id tincidunt elit. Nunc vitae volutpat elit. Donec vel ex sit amet velit pellentesque maximus. Donec sollicitudin ac purus sed fermentum. Aenean non lacus non est dapibus semper ut aliquam orci. Donec et vestibulum mauris. Vestibulum consequat magna ut ligula faucibus suscipit. Aliquam ut consectetur risus, eu posuere sem. Mauris a mauris maximus, molestie mauris ac, consequat purus. Vestibulum condimentum quam at aliquet venenatis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pellentesque molestie nisi, nec lobortis metus suscipit sit amet. Donec pharetra vitae elit a semper.\r\n\r\nVivamus interdum posuere auctor. Ut suscipit facilisis eros, quis accumsan metus dapibus vel. Nullam sit amet imperdiet odio. Proin vel faucibus eros. Aliquam vestibulum ante quis ipsum efficitur, id pharetra metus mollis. Phasellus felis sem, laoreet quis mi vitae, tincidunt tincidunt nisl. Quisque et dui id arcu dictum accumsan vitae sed purus. Etiam a elit nisi. Morbi ultrices bibendum tempus. Mauris ac dictum tellus, vel rhoncus massa.\r\n\r\nVestibulum mollis et enim et ultrices. Integer aliquam ut sapien ac iaculis. Sed id orci sed eros congue gravida. Praesent porttitor libero sit amet quam mattis suscipit. Donec venenatis in arcu eget tempus. Sed sodales mattis enim vel consequat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed id risus laoreet, accumsan nisl pharetra, lobortis tellus. Integer dapibus risus quis lacus iaculis, et bibendum quam convallis. Donec fermentum et ex nec porttitor. Morbi at massa massa. Ut metus nisi, commodo ac odio eu, cursus congue lacus. Morbi eu elit eu purus ultricies sagittis. Integer dignissim pellentesque sapien non pharetra. Nunc at ipsum nunc. Donec vestibulum est dolor, et posuere ante pharetra vitae.', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `PaperTags`
--

CREATE TABLE IF NOT EXISTS `PaperTags` (
  `paperTagId` bigint(20) NOT NULL AUTO_INCREMENT,
  `paperTagName` varchar(255) DEFAULT NULL,
  `paperId` bigint(20) NOT NULL,
  PRIMARY KEY (`paperTagId`),
  KEY `paperId` (`paperId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `PeerReviews`
--

CREATE TABLE IF NOT EXISTS `PeerReviews` (
  `peerReviewId` bigint(20) NOT NULL AUTO_INCREMENT,
  `paperId` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `comment` longtext,
  `startTime` bigint(20) NOT NULL,
  `isComplete` int(1) NOT NULL DEFAULT '0',
  `isEndorsed` int(1) DEFAULT NULL,
  PRIMARY KEY (`peerReviewId`),
  KEY `userId` (`userId`),
  KEY `paperId` (`paperId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Publisher`
--

CREATE TABLE IF NOT EXISTS `Publisher` (
  `publisherId` bigint(20) NOT NULL AUTO_INCREMENT,
  `publisherName` varchar(255) DEFAULT NULL,
  `publisherUrl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`publisherId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Settings`
--

CREATE TABLE IF NOT EXISTS `Settings` (
  `settingId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `numValue` bigint(20) DEFAULT NULL,
  `textValue` longtext,
  PRIMARY KEY (`settingId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `Settings`
--

INSERT INTO `Settings` (`settingId`, `name`, `numValue`, `textValue`) VALUES
(1, 'paperReviews', 10, ''),
(2, 'paperApprovals', 3, NULL),
(3, 'loginAttempts', 5, ''),
(4, 'supportEmail', 0, ''),
(5, 'registration', 1, ''),
(6, 'language', 1, 'en_us.php'),
(7, 'timezone', 0, 'Pacific/Niue'),
(8, 'loginLockTime', 60, ''),
(9, 'reviewSkipLockTime', 24, ''),
(10, 'reviewSkipCount', 5, ''),
(11, 'reviewTime', 14, ''),
(12, 'enableCSPRNG', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `Taxonomy`
--

CREATE TABLE IF NOT EXISTS `Taxonomy` (
  `taxId` bigint(20) NOT NULL AUTO_INCREMENT,
  `taxName` varchar(255) NOT NULL,
  `taxDepth` bigint(20) NOT NULL,
  `parentId` bigint(20) NOT NULL,
  PRIMARY KEY (`taxId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `UserCategoriesAssoc`
--

CREATE TABLE IF NOT EXISTS `UserCategoriesAssoc` (
  `userCategoryAssocId` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `categoryId` bigint(20) NOT NULL,
  PRIMARY KEY (`userCategoryAssocId`),
  KEY `userId` (`userId`),
  KEY `categoryId` (`categoryId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `UserCategoriesAssoc`
--

INSERT INTO `UserCategoriesAssoc` (`userCategoryAssocId`, `userId`, `categoryId`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 6),
(4, 3, 1),
(5, 4, 1),
(6, 4, 2),
(7, 5, 1),
(8, 6, 5),
(9, 7, 2),
(10, 8, 2),
(11, 9, 1),
(12, 10, 5),
(14, 11, 1),
(15, 11, 6);

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `userId` bigint(20) NOT NULL AUTO_INCREMENT,
  `userName` longtext,
  `password` longtext,
  `emailAddress` varchar(255) DEFAULT NULL,
  `avatar` longtext,
  `authorID` bigint(20) DEFAULT NULL,
  `joinDate` datetime DEFAULT NULL,
  `degree` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `university` varchar(255) DEFAULT NULL,
  `timezone` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `activationCode` longtext,
  `uniqueId` longtext,
  `admin` int(1) DEFAULT '0',
  `paperCount` int(12) DEFAULT NULL,
  `endorserStatus` int(1) NOT NULL DEFAULT '1',
  `userFullName` varchar(255) DEFAULT NULL,
  `skipCount` int(11) DEFAULT '0',
  `skipTime` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`userId`, `userName`, `password`, `emailAddress`, `avatar`, `authorID`, `joinDate`, `degree`, `title`, `university`, `timezone`, `language`, `lastLogin`, `activationCode`, `uniqueId`, `admin`, `paperCount`, `endorserStatus`, `userFullName`, `skipCount`, `skipTime`) VALUES
(1, 'StevenTurner', '$2y$10$.xbyxz3k6X6uJNoS3aBvW.tQ2Cqv9FclpyjcO9u70zbyILUvwkl0K', 'StevenKTurner@gmail.com', NULL, NULL, '2016-04-27 21:38:25', NULL, NULL, NULL, NULL, NULL, '2016-04-28 01:27:14', NULL, 'b7314568b6eddfe6d733d5b9a08a0e66402b1a4732213a7d9b241068841c1ffe', 1, NULL, 1, 'Steven Turner', 0, NULL),
(2, 'Toots', '$2y$10$xfCOA0JsPMiayOS1P7WvhOgUtZfe1sg86jZVkb3kf5QNyuaiRQcS.', 'fake@fake.com', NULL, NULL, '2016-04-27 23:56:04', NULL, NULL, NULL, NULL, NULL, '2016-04-28 00:04:02', NULL, '36adda884b5f5f22d9e361935b19151ab115094abc8e3f0504037c783c7839b4', 0, NULL, 1, 'Toots and the Maytals', 0, NULL),
(3, 'DiamondHead', '$2y$10$ShhyUa8XKlnbL3rWep8taux2Gby61AGmgtmCBmim7ZhuVyvzejL5W', 'fake2@fake.com', NULL, NULL, '2016-04-27 23:56:41', NULL, NULL, NULL, NULL, NULL, '2016-04-28 00:09:33', NULL, '183610be0fa5878f5b150676e7a8eab7b630f65b772a665525d8ef52a9c8df5d', 0, NULL, 1, 'DiamondHead', 0, NULL),
(4, 'Bosstones', '$2y$10$Cxs8La/2Hp43sgF5NPJQAettBS3iUKVTZn6z7vy1ISse0ki7nlCIu', 'fake3@fake.com', NULL, NULL, '2016-04-27 23:57:18', NULL, NULL, NULL, NULL, NULL, '2016-04-28 00:13:50', NULL, '8d2246d7348e87fe7194dc62e003867c56d6b55d3a9da4bf9b62dc470f4def05', 0, NULL, 1, 'The Mighty Mighty Bosstones', 0, NULL),
(5, 'ACDC', '$2y$10$0hcsf4ovPDK3CSZ0I2RpneJJG7l4mTMNT/UOZiGJIjvZ7iUeOBmp.', 'fake4@fake.com', NULL, NULL, '2016-04-27 23:57:49', NULL, NULL, NULL, NULL, NULL, '2016-04-28 00:23:22', NULL, '060948291db2ea86b8afb6cce403353c7504f0064c220b1aeb273da275af187b', 0, NULL, 1, 'AC/DC', 0, NULL),
(6, 'PeterGabriel', '$2y$10$y8Aqdy3WgzXNdi44EbY9Y.1YEvqY1Hi4m.QAuzWHRUC.AO0SEbVme', 'fake5@fake.com', NULL, NULL, '2016-04-27 23:58:40', NULL, NULL, NULL, NULL, NULL, '2016-04-28 00:41:40', NULL, '7d9930aa6a7f5c19ddd6dc465dbc09931661a0568bf4fbe8cfb6a602e38ea380', 0, NULL, 1, 'Peter Gabriel', 0, NULL),
(7, 'TheSpecials', '$2y$10$/VL8PANsO2dp9h184cupOu5MXGV9CWx6X0kd0WDoRDzHMMF0Ta95a', 'fake6@fake.com', NULL, NULL, '2016-04-27 23:59:27', NULL, NULL, NULL, NULL, NULL, '2016-04-28 01:05:16', NULL, 'cfba67a549821b17b866950ef650e2624e68e0a7747b8d194d8e17cedae828fc', 0, NULL, 1, 'The Specials', 0, NULL),
(8, 'BadManners', '$2y$10$N3UoC.kyly5CBH3q9kYAi.ftHl3R5BbX2a5d4oDRrpULRWuSJcxRq', 'badmanners@fake.com', NULL, NULL, '2016-04-27 23:59:48', NULL, NULL, NULL, NULL, NULL, '2016-04-28 01:10:13', NULL, '766f374afe426afc59440534f174728b04c31778760a87b9c19494448868ceb9', 0, NULL, 1, 'Bad Manners', 0, NULL),
(9, 'ZZTop', '$2y$10$kpLEJKtLk1feQlHqq9HiIuAhIieOF97RqTgBV8gq93zy3/4vLyU1i', 'zztop@fake.com', NULL, NULL, '2016-04-28 00:00:31', NULL, NULL, NULL, NULL, NULL, '2016-04-28 01:16:40', NULL, 'e69bb5df87a7a4a229c83826b655f77b111c2681291c20be07a1a1885689d181', 0, NULL, 1, 'ZZ Top', 0, NULL),
(10, 'MichaelJackson', '$2y$10$uxh3rk4kjAj92TtXku0Uv.GqTdcb5XzMg3TYLCL9yMRfziY1d/sCe', 'mj@fake.com', NULL, NULL, '2016-04-28 00:01:53', NULL, NULL, NULL, NULL, NULL, '2016-04-28 01:20:24', NULL, '9669fccf6a77dbc5b998336a09943573ed19a5ee0c2240fafab361a55df15295', 0, NULL, 1, 'Michael Jackson', 0, NULL),
(11, 'Motorhead', '$2y$10$j2icmkKcLS2FljfTVp3fu.ZGJAfS0QRd7rHqHB4qK9k8KBWgKnYoi', 'motorhead@fake.com', NULL, NULL, '2016-04-28 00:03:42', NULL, NULL, NULL, NULL, NULL, '2016-04-28 01:24:07', NULL, '2961f193500f15c78e98d1cd68bfce15d1cd3f6c51b2d9a79b1eba20cd16b5c5', 0, NULL, 1, 'Motorhead', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `UsersGroupsAssoc`
--

CREATE TABLE IF NOT EXISTS `UsersGroupsAssoc` (
  `userGroupAssocId` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `groupId` bigint(20) NOT NULL,
  PRIMARY KEY (`userGroupAssocId`),
  KEY `userId` (`userId`),
  KEY `groupId` (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `UsersPapersAssoc`
--

CREATE TABLE IF NOT EXISTS `UsersPapersAssoc` (
  `userPaperAssocId` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `paperId` bigint(20) NOT NULL,
  `COIDisclosure` text,
  PRIMARY KEY (`userPaperAssocId`),
  KEY `userId` (`userId`),
  KEY `paperId` (`paperId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `UsersPapersAssoc`
--

INSERT INTO `UsersPapersAssoc` (`userPaperAssocId`, `userId`, `paperId`, `COIDisclosure`) VALUES
(1, 1, 1, NULL),
(2, 2, 2, NULL),
(3, 2, 3, NULL),
(4, 3, 4, NULL),
(5, 3, 5, NULL),
(6, 4, 6, NULL),
(7, 4, 7, NULL),
(8, 5, 8, NULL),
(9, 5, 9, NULL),
(10, 6, 10, NULL),
(11, 6, 11, NULL),
(12, 7, 12, NULL),
(13, 7, 13, NULL),
(14, 8, 14, NULL),
(15, 8, 15, NULL),
(16, 9, 16, NULL),
(17, 9, 17, NULL),
(18, 10, 18, NULL),
(19, 10, 19, NULL),
(20, 11, 20, NULL),
(21, 11, 21, NULL),
(22, 1, 22, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `UsersPapersEndorseAssoc`
--

CREATE TABLE IF NOT EXISTS `UsersPapersEndorseAssoc` (
  `userPaperEndorseId` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `paperId` bigint(20) NOT NULL,
  `COIDisclosure` text,
  PRIMARY KEY (`userPaperEndorseId`),
  KEY `userId` (`userId`),
  KEY `paperId` (`paperId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `GroupComments`
--
ALTER TABLE `GroupComments`
  ADD CONSTRAINT `GroupComments_ibfk_1` FOREIGN KEY (`groupId`) REFERENCES `Groups` (`groupId`),
  ADD CONSTRAINT `GroupComments_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `Users` (`userId`);

--
-- Constraints for table `Groups`
--
ALTER TABLE `Groups`
  ADD CONSTRAINT `Groups_ibfk_1` FOREIGN KEY (`groupLeader`) REFERENCES `Users` (`userId`);

--
-- Constraints for table `IPs`
--
ALTER TABLE `IPs`
  ADD CONSTRAINT `IPs_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `Users` (`userId`);

--
-- Constraints for table `Journal`
--
ALTER TABLE `Journal`
  ADD CONSTRAINT `Journal_ibfk_1` FOREIGN KEY (`publisherId`) REFERENCES `Publisher` (`publisherId`);

--
-- Constraints for table `PaperCategoryAssoc`
--
ALTER TABLE `PaperCategoryAssoc`
  ADD CONSTRAINT `PaperCategoryAssoc_ibfk_1` FOREIGN KEY (`paperId`) REFERENCES `Papers` (`paperId`),
  ADD CONSTRAINT `PaperCategoryAssoc_ibfk_2` FOREIGN KEY (`categoryId`) REFERENCES `Categories` (`categoryId`);

--
-- Constraints for table `PaperComments`
--
ALTER TABLE `PaperComments`
  ADD CONSTRAINT `PaperComments_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `Users` (`userId`),
  ADD CONSTRAINT `PaperComments_ibfk_2` FOREIGN KEY (`paperId`) REFERENCES `Papers` (`paperId`);

--
-- Constraints for table `PaperFootnotes`
--
ALTER TABLE `PaperFootnotes`
  ADD CONSTRAINT `PaperFootnotes_ibfk_1` FOREIGN KEY (`paperId`) REFERENCES `Papers` (`paperId`);

--
-- Constraints for table `PaperLinks`
--
ALTER TABLE `PaperLinks`
  ADD CONSTRAINT `PaperLinks_ibfk_1` FOREIGN KEY (`paperId`) REFERENCES `Papers` (`paperId`);

--
-- Constraints for table `PaperReferences`
--
ALTER TABLE `PaperReferences`
  ADD CONSTRAINT `PaperReferences_ibfk_1` FOREIGN KEY (`paperId`) REFERENCES `Papers` (`paperId`);

--
-- Constraints for table `PaperTags`
--
ALTER TABLE `PaperTags`
  ADD CONSTRAINT `PaperTags_ibfk_1` FOREIGN KEY (`paperId`) REFERENCES `Papers` (`paperId`);

--
-- Constraints for table `PeerReviews`
--
ALTER TABLE `PeerReviews`
  ADD CONSTRAINT `PeerReviews_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `Users` (`userId`),
  ADD CONSTRAINT `PeerReviews_ibfk_2` FOREIGN KEY (`paperId`) REFERENCES `Papers` (`paperId`);

--
-- Constraints for table `UserCategoriesAssoc`
--
ALTER TABLE `UserCategoriesAssoc`
  ADD CONSTRAINT `UserCategoriesAssoc_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `Users` (`userId`),
  ADD CONSTRAINT `UserCategoriesAssoc_ibfk_2` FOREIGN KEY (`categoryId`) REFERENCES `Categories` (`categoryId`);

--
-- Constraints for table `UsersGroupsAssoc`
--
ALTER TABLE `UsersGroupsAssoc`
  ADD CONSTRAINT `UsersGroupsAssoc_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `Users` (`userId`),
  ADD CONSTRAINT `UsersGroupsAssoc_ibfk_2` FOREIGN KEY (`groupId`) REFERENCES `Groups` (`groupId`);

--
-- Constraints for table `UsersPapersAssoc`
--
ALTER TABLE `UsersPapersAssoc`
  ADD CONSTRAINT `UsersPapersAssoc_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `Users` (`userId`),
  ADD CONSTRAINT `UsersPapersAssoc_ibfk_2` FOREIGN KEY (`paperId`) REFERENCES `Papers` (`paperId`);

--
-- Constraints for table `UsersPapersEndorseAssoc`
--
ALTER TABLE `UsersPapersEndorseAssoc`
  ADD CONSTRAINT `UsersPapersEndorseAssoc_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `Users` (`userId`),
  ADD CONSTRAINT `UsersPapersEndorseAssoc_ibfk_2` FOREIGN KEY (`paperId`) REFERENCES `Papers` (`paperId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

