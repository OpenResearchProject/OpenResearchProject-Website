The OpenResearch Project is dedicated to getting important research where it belongs! This is the website for OpenResearch.
This project should NOT be used yet. It is still in the alpha stage.

Minimum System Requirements:
PHP 5.5+
MySQL 5.6+
Apache 2.2+

TO DO:
- Uploads of PDF documents.
- Secure PDF downloader.
- User groups to allow for collaberation on papers.
- More search parameters.
- Related papers section.
- Citations need to be automatically pulled from PDF's.
- Citation statistics.
- API to search for and pull papers.
- API to pass papers that pass peer review onto other installations that accept them.
- API to pass on OpenResearch's settings to research peer reviews themselves.