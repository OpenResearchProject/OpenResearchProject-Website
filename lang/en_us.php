<?php
if(isset($includeCheck)){
// IF included from another source

abstract class lang
{
	//OpenResearch Error Messages:
	const genericError = 'Error Loading Website!<br><a href="./index.php">Reload</a>';
	const error = 'ERROR!';

	// Generic Messages:
        const welcome = 'Welcome to OpenResearch';
	const or_footer = 'The Open Research Project: Clearing the Turbid Waters of Research.';
	const success = 'Success!';

        // Login Messages:
        const login_default = 'Welcome to our site!';
        const login_alreadyLoggedIn = 'You are already logged in!<br><a href="./index.php">Return</a>';
        const login_good = 'Login Successful!<br><a href="./index.php">Continue</a>';
        const logout = 'You have been logged out.<br><a href="./index.php">Return</a>';
        const login_bad = 'Your username or password is incorrect.';
        const ban1 = 'You have been banned until ';
        const ban2 = '. Reason given:<br>';
        const activation = 'You can\'t login because your account hasn\'t been activated yet! Check your email for the activation link.';
        const invalidMessage = 'Invalid Message.';
	const login_maxAttempts1 = 'You have used all of your login attempts. Please try again after ';
	const login_maxAttempts2 = ' minutes.';
	const login_title = 'Login:';
	const login_username = 'Username: ';
	const login_password = 'Password: ';
	const login_stay = 'Stay logged in for 2 weeks?';

	// Admin Login Messages:
	const admin_loginGood = 'Login Successful!<br><a href="./index.php?a=2">Continue</a>';
        const admin_logout = 'You have been logged out of the admin panel.<br><a href="./index.php">Continue</a>';
        const admin_loginBad = 'Your username or password is incorrect.';
	const admin_maxAttempts = 'You have used all of your login attampts. You have been logged out. Try again later.<br><a href="./index.php">Continue</a>';
	const admin_reauth = 'Your session has expired. You need to login again to continue.';

	// Admin Settings:
	const admin_settingGood = 'Your settings have been changed.';
	const admin_settingBad = 'Something went wrong when adding your settings!';
	const admin_cacheWarn = 'Warning: Users may need to login/logout or close their browser to see the changes from these settings since they are cached on the user\'s browser!';
	const admin_disciplineExists = 'A discipline with that name already exists!';
	const admin_disciplineTooShort = 'Your discipline\'s name is too short! It needs to be at least 2 characters long!';
	const admin_disciplineAddBad = 'Something went wrong when adding your discipline!';
	const admin_disciplineAddGood = 'Your discipline has been added!';
	const admin_disciplineRemoveGood = 'That discipline has been deleted';
	const admin_disciplineRemoveBad = 'Something went wrong when deleting that discipline';
	const admin_register_success = 'User Account Created<br><a href="./index.php?a=6">Continue</a>';

	// Admin addUser:
	const adminAddUser_title = 'Add a New User:';
        const adminAddUser_fullName = 'Full Name: ';
        const adminAddUser_userName = 'Username: ';
        const adminAddUser_password = 'Password: ';
        const adminAddUser_passwordAgain = 'Password Again: ';
        const adminAddUser_email = 'Email: ';
        const adminAddUser_admin = 'Make user an admin? ';
        const adminAddUser_button = 'Register';

        const admin_back = '< Back';
	const admin_header = 'Admin Panel';
	const adminPaperDisciplines_title = 'Add/Remove Disciplines:';

	// Admin Landing:
        const adminLanding_orSettings = 'OpenResearch Settings';
        const adminLanding_generalSettings = 'General Settings';
        const adminLanding_localization = 'Localization';
        const adminLanding_users = 'Users';
        const adminLanding_addUser = 'Add User';
        const adminLanding_papers = 'Papers';
        const adminLanding_disciplineSettings = 'Categories & Diciplines';
        const adminLanding_reviewSettings = 'Review Settings';
        const adminLanding_logout = 'Logout of Admin Panel';

	// Admin Localization:
        const adminLocalization_title = 'Localization Settings:';
        const adminLocalization_language = 'Langauge: ';
        const adminLocalization_timezone = 'Timezone: ';
        const adminLocalization_button = 'Change Settings';

	// Admin paperAddDiscipline:
        const adminPaperAddDiscipline_title = 'Add New Discipline:';
        const adminPaperAddDiscipline_create1 = 'This will create a new discipline under ';
        const adminPaperAddDiscipline_create2 = 'This will create a new top-level discipline.';
        const adminPaperAddDiscipline_disciplineName = 'Discipline Name: ';
        const adminPaperAddDiscipline_button = 'Add Discipline';

	// Admin paperRemoveDiscipline:
        const adminPaperRemoveDiscipline_title = 'Delete Discipline:';
        const adminPaperRemoveDiscipline_warn1 = '<h3>WARNING!</h3> This will permanently delete ';
        const adminPaperRemoveDiscipline_warn2 = ' from your database. All papers that use this will be replaced with whatever you set here.';
        const adminPaperRemoveDiscipline_replace = 'Replace with: ';
        const adminPaperRemoveDiscipline_warn3 = 'Double check! This is your last chance to be sure you selected the right item!';
        const adminPaperRemoveDiscipline_button = 'Delete Discipline';

	// Admin paperDisciplines:
	const adminPaperDisciplines_addNew = 'Add New Discipline';
	const adminPaperDisciplines_delete = 'Delete';

	// Admin Reauth:
        const adminReauth_title = 'Admin Login:';
        const adminReauth_password = 'Password: ';
        const adminReauth_button = 'Login';

	// Admin OpenResearch Settings:
        const adminOrSettings_title = 'General OpenResearch Settings:';
        const adminOrSettings_supportEmail = 'Support Email: ';
        const adminOrSettings_warn = 'This is the email that is displayed to users whenever they need help or support. Don\'t leave it blank!';
        const adminOrSettings_button = 'Change Settings';

	// Admin paperReviewSettings:
        const adminPaperReviewSettings_title = 'Paper Review Settings:';
        const adminPaperReviewSettings_paperReviews = 'Paper Reviews: ';
        const adminPaperReviewSettings_paperReviewsEx = 'The number of review chances a paper gets.';
        const adminPaperReviewSettings_paperApprovals = 'Paper Approvals: ';
        const adminPaperReviewSettings_paperApprovalsEx = 'The number of positive reviews a paper must get before it is considered as accepted.';
        const adminPaperReviewSettings_reviewTime = 'Review Time: ';
        const adminPaperReviewSettings_reviewTimeEx = 'The number of days a user has to submit a review for a paper.';
        const adminPaperReviewSettings_skipCount = 'Skip Count: ';
        const adminPaperReviewSettings_skipCountEx = 'The number of papers a user can skip per lock time. The option below configures the lock time.<br/>(This prevents reviewers from just skipping to a friend\'s paper and upholds the random nature of reviews.)';
        const adminPaperReviewSettings_skipLockTime = 'Skip Lock Time: ';
        const adminPaperReviewSettings_skipLockTimeEx = 'The amount of time, in hours, that a user is locked out from being able to do reviews after using too many skips.';
        const adminPaperReviewSettings_enableCSPRNG = 'Enable CSPRNG: ';
        const adminPaperReviewSettings_enableCSPRNGEx = 'This enables the cryptographically secure random number generator used to assign reviews to users. It is <b>highly</b> recommended that you enable this! Without it, some numbers generated may be predictable to a well-informed party. This means that someone could potentially game the system into giving them the papers they want to review.';
        const adminPaperReviewSettings_CSPRNGdisabled = 'Enable CSPRNG: <b>DISABLED, CSPRNG NOT INSTALLED</b>';
        const adminPaperReviewSettings_CSPRNGdisabledEx = 'The random number generator is currently not cryptographically secure, meaning that some numbers generated may be predictable to a well-informed party. This means that someone could potentially game the system into giving them the papers they want to review. If you want your system to be as fair as possible you should install <a href="https://secure.php.net/manual/en/ref.csprng.php" target="_blank">CSPRNG functions</a> on your server. Specifically, you would want the function <a href="https://secure.php.net/manual/en/function.random-int.php" target="_blank">random_int</a>.';
        const adminPaperReviewSettings_button = 'Change Settings';

	// Admin userSettings:
        const adminUserSettings_title = 'General User Settings:';
        const adminUserSettings_regType = 'Registration Type: ';
        const adminUserSettings_regType1 = 'Registration Disabled, Admin Add Only';
        const adminUserSettings_regType2 = 'Registration without Email Check';
        const adminUserSettings_regType3 = 'Registration with Email Check';
        const adminUserSettings_loginAttempts = 'Login Attempts: ';
        const adminUserSettings_loginAttemptsEx = 'The number of times a user can get a password wrong before they are locked out.';
        const adminUserSettings_loginLockTime = 'Login Lock Time (in minutes): ';
        const adminUserSettings_loginLockTimeEx = 'The time a user is locked out if they run out of attempts.';
        const adminUserSettings_button = 'Change Settings';

	// Registeration Messages:
	const register_userNameInUse = 'That username has already been taken. Please use another.';
        const register_emailInUse = 'That email address is already being used by another account.';
	const register_emailValid = 'That email address doesn\'t seem valid.';
	const register_userNameLength = 'Your username MUST be between 3 and 25 characters long!';
	const register_passwordLength = 'Your password MUST be between 8 and 70 characters long!';
	const register_passwordMatch = 'Your password didn\'t match! It IS case sensitive!';
	const register_success = 'Your account has been registered!<br><a href="./index.php">Continue</a>';
	const register_emailCodeError = 'Error sending activation code. Please try again later.';
	const register_accountActivationGood = 'Your account has been activated. You can now login.';
	const register_accountActivationBad = 'There seems to be a problem with your activation code. If it keeps failing, try copying and pasting it into your browser.';
	const register_fullNameLength = 'You must provide your full name.';
	const register_emailSubject = 'Thank you for joining OpenResearch!';
	const register_emailBody1 = 'Thank you for joining OpenResearch, ';
	const register_emailBody2 = '! To verify your account, please click the link below:<br>';

	// Paper:
	const paper_UploadSuccess = 'Your paper has successfully been uploaded.<br><a href="./index.php?p=10">View Your Papers</a><br><a href="./index.php">Return Home</a>';

	// Review:
	const review_missingDisciplines = 'You need to add a discipline to your profile before you can use this page. <a href="./index.php?p=4">Click here</a> to go to your profile.';
	const review_description1 = '<p>This page will let you review other people\'s papers. It will give you a paper at random from your disciplines and any disciplines beneath those in the disciplines list. Be sure to double check that your disciplines are correct! They are listed here:</p>';
	const review_description2 = '<p>If you aren\'t suited to review a given paper, skip it using the skip button! However, you are only given ';
	const review_description3 = ' skips per ';
	const review_description4 = ' hours.</p>';
	const review_button = 'Review a Random Paper';
	const review_title = 'Review a Paper';
	const review_addFailure = 'Failed to assign a review. Please try again. If the problem persists, please contact an administrator.';
	const review_daysRemaining = 'Days left to finish Review: ';
	const review_comments = 'Hidden Reviewer\'s Comments on Paper:<br>(Only the author of the paper and site admin\'s can see this text after you submit it.)';
	const review_endorseCheckbox = 'Endorse Paper: ';
	const review_paperTitle = 'Paper: ';
	const review_saveGood = 'Your review has been saved!';
	const review_saveBad = 'Error saving your review! Please try again!';
	const review_submit = 'Your review has been submitted! Thank you!';
	const review_outOfPapers = 'There aren\'t any papers in your discipline to review at the moment.';
	const review_skipMax1 = 'You have used all of your review skips. You can review again in ';
	const review_skipMax2 = ' hours.';
	const review_submitButton = 'Submit Review';
	const review_skipButton = 'Skip this Review';
	const review_saveButton = 'Save and Finish Later';

	// User Settings:
	const userLocalization_good = 'Your settings have been changed!';
	const userLocalization_bad = 'There was an error saving your settings. Please try again.';
	const changePassword_issue = 'Password issue.  Please check and reenter.  Passwords must be between 8 and 70 characters long.';
	const changePassword_bad = 'Incorrect password';
	const changePassword_good = 'Password updated';

	//Add Paper:
	const addPaper_uploadHeader = 'Upload Your Research Paper';
	const addPaper_titleLabel = '*Paper Title';
	const addPaper_thirdURLLabel = 'Third Party Publisher\'s Paper URL';
	const addPaper_publishedDateLabel = '*Published Date';
	const addPaper_publisherIDLabel = 'Publisher ID';
	const addPaper_abstractLabel = '*Abstract';
	const addPaper_sponsorLabel = 'Sponsor';
	const addPaper_doiURLLabel = '*Digital Object Identifier URL';
	const addPaper_doiLabel = '*Digital Object Identifier';
	const addPaper_copyrightOwnerLabel = 'Copyright Owner';
	const addPaper_copyrightDateLabel = 'Copyright Date';
	const addPaper_disciplineLabel = '*Discipline';
	const addPaper_fullTextLabel = '*Full Text (Not including abstract)';
	const addPaper_legalese = 'By clicking below, you are taking full responsibility for ensuring the proper copyright/intellectual property protections are being followed.';
	const addPaper_button = 'Submit';

	//Change Info:
	const changeInfo_fullName = 'Full Name: ';
	const changeInfo_email = 'Email: ';
	const changeInfo_degree = 'Degree: ';
	const changeInfo_title = 'Title: ';
	const changeInfo_university = 'University: ';
	const changeInfo_password = 'Reenter Password: ';
	const changeInfo_updateButton = 'Update Info';
	const changeInfo_emailInUse = 'Sorry, that email address is already in use.';
	const changeInfo_updated = 'Info updated';
	const changeInfo_badPassword = 'Incorrect password';

	//Change Password:
	const changePass_newPass = 'New Password: ';
	const changePass_reenterNewPass = 'ReEnter New Password: ';
	const changePass_currentPass = 'Current Password: ';
	const changePass_updateButton = 'Update Password';

	//Header
	const header_myPapers = 'My Papers';
	const header_logout = 'Logout';
	const header_addPaper = 'Add Paper';
	const header_review = 'Review';
	const header_admin = 'Admin Panel Login';
	const header_login = 'Login';
	const header_register = 'Register';

	//home
	const home_searchHeader = 'Search for a Paper'; //not sure if we're keeping this...
	const searchLanding_title = 'Search for a Paper';
	const searchLanding_button = 'Search';

	//Register page
	const registerPage_title = 'Register for an Account';
	const registerPage_fullNameLabel = 'Full Name: ';
	const registerPage_usernameLabel = 'Username: ';
	const registerPage_password = 'Password: ';
	const registerPage_reenterPassword = 'Password Again: ';
	const registerPage_email = 'Email: ';
	const registerPage_registerButton = 'Register';

	//userDiscipline
	const userDiscipline_title = 'Disciplines';
	const userDiscipline_yourDisciplines = '  Your Disciplines (Remove)';
	const userDiscipline_noneAdded = 'No Disciplines Added Yet.';
	const userDiscipline_allDisciplines = '  All Disciplines (Add)';
	const userDiscipline_addNote = 'If you are going to include a discipline that has sub-disciplines listed, make sure you know them all! You will be able to peer review all of the sub-disciplines of your selected discipline!';
	const userDiscipline_delete = 'Delete';

	//userLanding
	const userLanding_title = 'Account Info';
	const userLanding_lastLogin = 'Last Login: ';
	const userLanding_emailLabel = 'Email: ';
	const userLanding_updateInfo = 'Update Info';
	const userLanding_changePassword = 'Change Password';
	const userLanding_localization = 'Localization';
	const userLanding_disciplines = 'Disciplines';
	const userLanding_noDisciplines = 'No Disciplines Added Yet.';
	const userLanding_updateDisciplines = 'Update Disciplines';
	const userLanding_publishedPapers = 'Published Papers';
	const userLanding_noPublished = 'No published papers';

	//user localization
	const userLoc_timezone = 'Timezone:';
	const userLoc_language = 'Language:';
	const userLoc_button = 'Update Info';

	//PHP file messages

	//changeInfo.php
	const changeInfoPHP_emailInUse = 'Sorry, that email address is already in use';
	const changeInfoPHP_infoUpdated = 'Info updated';
	const changeInfoPHP_wrongPass = 'Incorrect password';

	//changePassword.php
	const changePassPHP_updated = 'Password updated';
	const changePassPHP_incorrect = 'Incorrect password';
	const changePassPHP_issue = 'Password issue.  Please check and reenter.  Passwords must be between 8 and 70 characters long.';

	//comments
	const comments_pleaseLogin = 'Please log in to comment.';
	const comments_added = 'Comment added.';
	const comments_error = 'Error adding comment, comment not added.';
	const comments_title = 'Notes and Feedback';
	const comments_noComments = 'No Comments Yet.';
	const comments_newComment = 'Enter a new comment here';
	const comments_reply = 'Reply';
	const comments_button = 'Submit';

	//myPapers
	const myPapers_published = 'Published and Reviewed';
	const myPapers_noPublished = 'No published papers';
	const myPapers_underReview = 'Under Review';
	const myPapers_noReview = 'No papers under review.';

	//paperUtil
	const paperUtil_published = 'Published ';
	const paperUtil_isEndorsed1 = 'This paper is awaiting review.';
	const paperUtil_isEndorsed2 = 'This paper has passed review.';
	const paperUtil_isEndorsed3 = 'This paper has failed review.';
	const paperUtil_isEndorsedError = 'Error reading endorsement.';
	const paperUtil_cited = 'Cited ';
	const paperUtil_feedback = 'Notes and Feedback';
	const paperUtil_fullPaper = 'Full Paper';
	const paperUtil_fullText = 'Full Text:';

	//showPaper
	const showPaper_lostPaper = 'I\'m sorry, I couldn\'t find that paper for you.';

	//userDiscipline
	const userDiscipline_missingDisciplines = 'This admin hasn\'t added any disciplines to the website yet.';
	const userDiscipline_add = 'Add';

}

}else{ // ELSE IF included from another php source
if(headers_sent()){ die(include("404.html")); } // If headers were somehow already sent, just die to a custom 404 page
header("HTTP/1.1 404 Not Found"); // Send headers if not sent already
die(include("404.html")); // Die to custom 404 page
} // END ELSE IF included from another source

?>
