<?php
if(isset($includeCheck)){
// IF included from another source

abstract class lang
{
	//OpenResearch Error Messages:
	const genericError = '¡Lo Siento! Error al Cargar Paginas.<br><a href="./index.php">Refrescar</a>';
	const error = '¡ERROR!';

	// Generic Messages:
        const welcome = 'Bienvenido a OpenResearch';
	const or_footer = 'El Open Research Project: Clarificando el Agua Túrbido de Ciencia.';
	const success = '¡Éxito!';

        // Login Messages:
        const login_default = '¡Bienvenido a nuestro sitio!';
        const login_alreadyLoggedIn = 'Esta bien, ya se ha autentificado.<br><a href="./index.php">Regreso</a>';
        const login_good = 'Inicio de sesión correcto!<br><a href="./index.php">Continuar</a>';
        const logout = 'Has sido desconectado.<br><a href="./index.php">Regreso</a>';
        const login_bad = 'Su nombre de usuario o contraseña es incorrecta. Por favor, compruebe la tecla Caps Lock.';
        const ban1 = 'Se le ha prohibido hasta ';
        const ban2 = '. Razón dada:<br>';
        const activation = 'No se puede iniciar sesión debido a que su cuenta no se ha activado todavía! Consultar su correo electrónico para el enlace de activación.';
        const invalidMessage = 'Mensaje no válido.';
	const login_maxAttempts1 = 'Ha utilizado todos sus intentos de inicio de sesión. Por favor, inténtelo de nuevo después de ';
	const login_maxAttempts2 = ' minutos.';
	const login_title = 'iniciar sesión: ';
	const login_username = 'Nombre de Usuario: ';
	const login_password = 'Contraseña: ';
	const login_stay = '¿Permanecer conectado durante 2 semanas?';

	// Admin Login Messages:
	const admin_loginGood = 'Inicio de sesión correcto!<br><a href="./index.php?a=2">Continuar</a>';
        const admin_logout = 'Se le ha cerrado la sesión del panel de administración.<br><a href="./index.php">Continuar</a>';
        const admin_loginBad = 'Su nombre de usuario o contraseña es incorrecta.';
	const admin_maxAttempts = 'Ha utilizado todos sus intentos de inicio de sesión. Has sido desconectado. Inténtelo de nuevo más tarde.<br><a href="./index.php">Continuar</a>';
	const admin_reauth = 'Su sesión ha caducado. Es necesario volver a iniciar sesión para continuar.';

	// Admin Settings:
	const admin_settingGood = 'Sus ajustes se han cambiado.';
	const admin_settingBad = 'Algo salió mal al añadir la configuración!';
	const admin_cacheWarn = 'Advertencia: Los usuarios pueden necesitar Iniciar sesión / cierre de sesión o cerrar su navegador para ver los cambios de estos archivos ya que se almacenan en caché en el navegador del usuario!';
	const admin_disciplineExists = 'No puedo añadir la disciplina, ya existe.';
	const admin_disciplineTooShort = 'El nombre de su disciplina es muy corto! La longitud mínima es dos caracteres.';
	const admin_disciplineAddBad = 'Algo salió mal al agregar su disciplina!';
	const admin_disciplineAddGood = 'Su disciplina se ha añadido!';
	const admin_disciplineRemoveGood = 'Que la disciplina ha sido borrada';
	const admin_disciplineRemoveBad = 'Algo salió mal cuando se elimina esa disciplina';
	const admin_register_success = 'Cuenta de usuario creada<br><a href="./index.php?a=6">Continuar</a>';

	// Admin addUser:
	const adminAddUser_title = 'Añadir un nuevo usuario: ';
        const adminAddUser_fullName = 'Nombre completo: ';
        const adminAddUser_userName = 'Nombre de usuario: ';
        const adminAddUser_password = 'Contraseña: ';
        const adminAddUser_passwordAgain = ' Volver a Escribir la Contraseña : ';
        const adminAddUser_email = 'Correo Electrónico: ';
        const adminAddUser_admin = '¿Administrar Cuentas de Usuario? ';
        const adminAddUser_button = 'Registrar';

        const admin_back = '< Retroceso';
	const admin_header = 'Panel de administrador';
	const adminPaperDisciplines_title = 'Añadir / Eliminar Disciplinas: ';

	// Admin Landing:
        const adminLanding_orSettings = 'OpenResearch Configuración';
        const adminLanding_generalSettings = 'Configuración General';
        const adminLanding_localization = 'Localización';
        const adminLanding_users = 'Usuarios';
        const adminLanding_addUser = 'Agregar Usuario';
        const adminLanding_papers = 'Trabajos';
        const adminLanding_disciplineSettings = 'Categorías & Disciplinas';
        const adminLanding_reviewSettings = 'Configuración Revisar';
        const adminLanding_logout = 'Salir del Panel de Administración';

	// Admin Localization:
        const adminLocalization_title = 'Configuración de Localización: ';
        const adminLocalization_language = 'Idioma: ';
        const adminLocalization_timezone = 'Zona Horaria: ';
        const adminLocalization_button = 'Cambiar Configuración';

	// Admin paperAddDiscipline:
        const adminPaperAddDiscipline_title = 'Añadir Nueva Disciplina: ';
        const adminPaperAddDiscipline_create1 = 'Esto creará una nueva disciplina bajo ';
        const adminPaperAddDiscipline_create2 = 'Esto creará una nueva disciplina de nivel superior.';
        const adminPaperAddDiscipline_disciplineName = 'Nombre de la Disciplina: ';
        const adminPaperAddDiscipline_button = 'Añadir disciplina';

	// Admin paperRemoveDiscipline:
        const adminPaperRemoveDiscipline_title = 'Eliminar la Disciplina:';
        const adminPaperRemoveDiscipline_warn1 = '<H3>¡ADVERTENCIA!</H3> Esto eliminará de forma permanente ';
        const adminPaperRemoveDiscipline_warn2 = ' a partir de su base de datos. Todos los trabajos que utilizan este será reemplazado con lo que se establezca aquí.';
        const adminPaperRemoveDiscipline_replace = 'Remplazar con: ';
        const adminPaperRemoveDiscipline_warn3 = '¡Comprobar Dos Veces! Esta es su última oportunidad para asegurarse de que ha seleccionado el artículo correcto!';
        const adminPaperRemoveDiscipline_button = 'Eliminar la Disciplina';

	// Admin paperDisciplines:
	const adminPaperDisciplines_addNew = 'Añadir Nueva Disciplina';
	const adminPaperDisciplines_delete = 'Borrar';

	// Admin Reauth:
        const adminReauth_title = 'Administración de Sesión: ';
        const adminReauth_password = 'Contraseña: ';
        const adminReauth_button = 'Iniciar Sesión';

	// Admin OpenResearch Settings:
        const adminOrSettings_title = 'Configuración General OpenResearch:';
        const adminOrSettings_supportEmail = 'Correo Electrónico por Asistencia: ';
        const adminOrSettings_warn = 'Este es el correo electrónico que se muestra a los usuarios cada vez que necesitan ayuda o apoyo. No lo deje en blanco!';
        const adminOrSettings_button = 'Cambiar ajustes';

	// Admin paperReviewSettings:
        const adminPaperReviewSettings_title = 'Comprobar la Configuración del Trabajo:';
        const adminPaperReviewSettings_paperReviews = 'Los Repasos de Trabajo: ';
        const adminPaperReviewSettings_paperReviewsEx = 'El número de posibilidades de repaso de un trabajo se queda.';
        const adminPaperReviewSettings_paperApprovals = 'Las aprobaciones de trabajo: ';
        const adminPaperReviewSettings_paperApprovalsEx = 'El número de comentarios positivos de un trabajo debe conseguir antes de que se considera como aceptado.';
        const adminPaperReviewSettings_reviewTime = 'Tiempo de revisión: ';
        const adminPaperReviewSettings_reviewTimeEx = 'El número de días que un usuario tiene que someter una reseña para un trabajo.';
        const adminPaperReviewSettings_skipCount = 'Saltar Conde: ';
        const adminPaperReviewSettings_skipCountEx = 'El número de documentos de un usuario puede saltar por tiempo de bloqueo. La siguiente opción configura el tiempo de bloqueo. <br/> (Esto evita que los colaboradores a sólo ignorar el trabajo de un amigo y defiende el carácter aleatorio de comentarios.)';
        const adminPaperReviewSettings_skipLockTime = 'Saltar Bloqueo Tiempo: ';
        const adminPaperReviewSettings_skipLockTimeEx = 'La cantidad de tiempo, en horas, que un usuario está bloqueada de ser capaz de hacer comentarios después de usar demasiados saltos.';
        const adminPaperReviewSettings_enableCSPRNG = 'Habilitar CSPRNG: ';
        const adminPaperReviewSettings_enableCSPRNGEx = 'Esto permite que el generador de números aleatorios criptográficamente seguro utilizado para asignar comentarios a los usuarios. Es <b> muy </ b> recomendado que habilita esta! Sin ella, algunos números generados pueden ser predecibles a una fiesta bien informada. Esto significa que alguien podría potencialmente jugar con el sistema que les entreguen los documentos que desea revisar.';
        const adminPaperReviewSettings_CSPRNGdisabled = 'Habilitar CSPRNG: <b> desactivado, CSPRNG NO INSTALADO </b>';
        const adminPaperReviewSettings_CSPRNGdisabledEx = 'El generador de números aleatorios no está en criptográficamente seguro, lo que significa que algunos números generados pueden ser predecibles a una fiesta bien informada. Esto significa que alguien podría potencialmente jugar con el sistema que les entreguen los documentos que desea revisar. Si desea que su sistema sea lo más justo posible, debe instalar <a href="https://secure.php.net/manual/en/ref.csprng.php" target="_blank">CSPRNG funciones</a>
en su servidor. En concreto, le gustaría que la función <a href="https://secure.php.net/manual/en/function.random-int.php" target="_blank">random_int</a>.';
        const adminPaperReviewSettings_button = 'Cambiar ajustes';

	// Admin userSettings:
        const adminUserSettings_title = 'Configuración de usuario generales:';
        const adminUserSettings_regType = 'Tipo de registro: ';
        const adminUserSettings_regType1 = 'Deshabilitado el registro, Admin Añadir Sólo';
        const adminUserSettings_regType2 = 'Registro sin correo electrónico Comprobar';
        const adminUserSettings_regType3 = 'Registro con el correo electrónico Comprobar';
        const adminUserSettings_loginAttempts = 'Intentos de acceso: ';
        const adminUserSettings_loginAttemptsEx = 'El número de veces que un usuario puede obtener una contraseña incorrecta antes de que sean bloqueados.';
        const adminUserSettings_loginLockTime = 'Tiempo de Sesión de bloqueo (en minutos): ';
        const adminUserSettings_loginLockTimeEx = 'El tiempo que un usuario quedará bloqueado si se quedan sin intentos.';
        const adminUserSettings_button = 'Cambiar ajustes';

	// Registration Messages:
	const register_userNameInUse = 'Ese nombre de usuario ya ha sido tomada. Por favor utilice otro.';
        const register_emailInUse = 'Esa dirección de correo electrónico ya está siendo utilizado por otra cuenta.';
	const register_emailValid = 'Esa dirección de correo electrónico no parece válida.';
	const register_userNameLength = 'Su nombre de usuario debe tener entre 3 y 25 caracteres de longitud!';
	const register_passwordLength = 'Su clave tiene que tener entre 8 y 70 caracteres de longitud!';
	const register_passwordMatch = 'Su contraseña No se han encontrado! Se distingue entre mayúsculas y minúsculas!';
	const register_success = 'Su cuenta ha sido registrada!<br><a href="./index.php">Continuar</a>';
	const register_emailCodeError = 'Error al enviar el código de activación. Por favor, inténtelo de nuevo más tarde.';
	const register_accountActivationGood = 'Tu cuenta ha sido activada. Ahora puede iniciar sesión.';
	const register_accountActivationBad = 'Parece que hay un problema con el código de activación. Si sigue fallando, intente copiar y pegar en su navegador.';
	const register_fullNameLength = 'Debe proporcionar su nombre completo.';
	const register_emailSubject = 'Gracias por unirse a OpenResearch!';
	const register_emailBody1 = 'Gracias por unirse a OpenResearch, ';
	const register_emailBody2 = '! Para verificar su cuenta, por favor haga clic en el siguiente enlace:<br>';

	// Paper:
	const paper_UploadSuccess = 'Su trabajo con éxito se ha cargado.<br><a href="./index.php?p=10">Ver sus trabajos</a><br><a href="./index.php">Volver a casa</a>';

	// Review:
	const review_missingDisciplines = 'Es necesario añadir una disciplina a su perfil antes de poder utilizar esta página. <a href="./index.php?p=4">Haga clic aquí </a> para ir a su perfil.';
	const review_description1 = '<p>Esta página le permitirá revisar los trabajos de otras personas. Se le dará un trabajo al azar de sus disciplinas y las disciplinas debajo de los de la lista disciplinas. Asegúrese de corroborar que sus disciplinas son correctas! Se enumeran aquí</p>';
	const review_description2 = '<p>Si no está indicado para revisar un documento dado, evitarlo usando el botón de salto! Sin embargo, sólo se le da ';
	const review_description3 = ' salta por ';
	const review_description4 = ' horas.</p>';
	const review_button = 'Revisar un artículo al azar';
	const review_title = 'Revisar un Libro';
	const review_addFailure = 'No se ha podido asignar una opinión. Por favor, inténtelo de nuevo. Si el problema persiste, póngase en contacto con un administrador.';
	const review_daysRemaining = 'Días para terminar la opinión: ';
	const review_comments = 'Los Comentarios del Revisor Confidencial: <br> (El autor del trabajo y del sitio de administración puede ver este texto después de enviar el trabajo.)';
	const review_endorseCheckbox = 'endoso de efectos: ';
	const review_paperTitle = 'Trabajo: ';
	const review_saveGood = 'Su comentario ha sido guardado!';
	const review_saveBad = 'Error al guardar sus comentarios! Por favor, inténtelo de nuevo!';
	const review_submit = 'Su comentario ha sido enviado! ¡Gracias!';
	const review_outOfPapers = 'No hay ningún documento en su disciplina para revisar en este momento.';
	const review_skipMax1 = 'Ha utilizado todos sus saltos de revisión. Puede revisar de nuevo en ';
	const review_skipMax2 = ' horas.';
	const review_submitButton = 'Enviar opinión';
	const review_skipButton = 'Omita esta revisión';
	const review_saveButton = 'Guardar y terminar más tarde';

	// User Settings:
	const userLocalization_good = 'Sus ajustes se han cambiado!';
	const userLocalization_bad = 'Se ha producido un error al guardar la configuración. Por favor, inténtelo de nuevo.';
	const changePassword_issue = 'tema contraseña. Por favor, compruebe y vuelva a introducir. Las contraseñas deben tener entre 8 y 70 caracteres de longitud.';
	const changePassword_bad = 'contraseña incorrecta';
	const changePassword_good = 'La contraseña se actualizó';

	//Add Paper:
	const addPaper_uploadHeader = 'Someterse trabajo de investigación';
	const addPaper_titleLabel = '*Título del artículo';
	const addPaper_thirdURLLabel = 'URL del Terceros Parte Editor';
	const addPaper_publishedDateLabel = '*Fecha de Publicación';
	const addPaper_publisherIDLabel = 'ID de editor';
	const addPaper_abstractLabel = '*Abstracto';
	const addPaper_sponsorLabel = 'Patrocinador';
	const addPaper_doiURLLabel = '*URL del Identificador de Objetos Digitales';
	const addPaper_doiLabel = '*Identificador de Objetos Digitales';
	const addPaper_copyrightOwnerLabel = 'Propietario del Copyright';
	const addPaper_copyrightDateLabel = 'Fecha de la Copyright';
	const addPaper_disciplineLabel = '*Disciplina';
	const addPaper_fullTextLabel = '*Texto completo (no incluyendo resumen)';
	const addPaper_legalese = 'Al hacer clic a continuación, usted está tomando toda la responsabilidad de garantizar los derechos de autor apropiada / protección de la propiedad intelectual se están siguiendo.';
        const addPaper_button = 'Enviar';

	//Change Info:
	const changeInfo_fullName = 'Nombre Completo: ';
	const changeInfo_email = 'Correo Electrónico: ';
	const changeInfo_degree = 'La Licenciatura: ';
	const changeInfo_title = 'Título: ';
	const changeInfo_university = 'Universidad: ';
	const changeInfo_password = 'Escriba la Contraseña Otra Vez: ';
	const changeInfo_updateButton = 'Actualizar Información';
	const changeInfo_emailInUse = 'Lo sentimos, dirección de correo electrónico que ya está en uso.';
	const changeInfo_updated = 'Información Actualizada';
	const changeInfo_badPassword = 'Contraseña Incorrecta';

	//Change Password:
	const changePass_newPass = 'Nueva Contraseña: ';
	const changePass_reenterNewPass = 'Vuelva a escribir la nueva contraseña: ';
	const changePass_currentPass = 'Contraseña Actual: ';
	const changePass_updateButton = 'Actualiza Contraseña';

	//Header
	const header_myPapers = 'Mi Trabajos';
	const header_logout = 'Cerrar sesión';
	const header_addPaper = 'Añadir Trabajo';
	const header_review = 'Revisión';
	const header_admin = 'Panel de administración de sesión';
	const header_login = 'Iniciar sesión';
	const header_register = 'Registro';

	//home
	const home_searchHeader = 'Búsqueda de un Libro';
	const searchLanding_title = 'Búsqueda de un Libro';
        const searchLanding_button = 'Buscar';

	//Register page
	const registerPage_title = 'Registrar Una Cuenta';
	const registerPage_fullNameLabel = 'Nombre Completo: ';
	const registerPage_usernameLabel = 'Nombre de Usuario: ';
	const registerPage_password = 'Contraseña: ';
	const registerPage_reenterPassword = 'Contraseña de nuevo: ';
	const registerPage_email = 'correo electrónico: ';
	const registerPage_registerButton = 'Registro';

	//userDiscipline
	const userDiscipline_title = 'disciplinas';
	const userDiscipline_yourDisciplines = '  Sus Disciplinas (Quitar)';
	const userDiscipline_noneAdded = 'Sin embargo Disciplinas Añadido.';
	const userDiscipline_allDisciplines = '  Todas las disciplinas (Añadir)';
	const userDiscipline_addNote = 'Si usted va a incluir una disciplina que tiene sub-disciplinas de la lista, asegúrese de que las conozca! Podrá evaluación inter pares de todas las subdisciplinas de su disciplina seleccionada!';
	const userDiscipline_delete = 'Borrar';

	//userLanding
	const userLanding_title = 'Informacion de Cuenta';
	const userLanding_lastLogin = 'Último Acceso: ';
	const userLanding_emailLabel = 'Correo Electrónico: ';
	const userLanding_updateInfo = 'Actualizar Información';
	const userLanding_changePassword = 'Cambia la Contraseña';
	const userLanding_localization = 'Localización';
	const userLanding_disciplines = 'Disciplinas';
	const userLanding_noDisciplines = 'Disciplinas No Crean Aún.';
	const userLanding_updateDisciplines = 'Disciplinas Actualizar';
	const userLanding_publishedPapers = 'Los Trabajos Publicados';
	const userLanding_noPublished = 'No Hay Trabajos Publicados';

	//user localization
	const userLoc_timezone = 'Zona Horaria: ';
	const userLoc_language = 'Idioma: ';
	const userLoc_button = 'Actualizar Información';

	//PHP file messages

	//changeInfo.php
	const changeInfoPHP_emailInUse = 'Lo sentimos, dirección de correo electrónico que ya está en uso';
	const changeInfoPHP_infoUpdated = 'Información Actualizada';
	const changeInfoPHP_wrongPass = 'Contraseña Incorrecta';

	//changePassword.php
	const changePassPHP_updated = 'La Contraseña Se Actualizó';
	const changePassPHP_incorrect = 'Contraseña Incorrecta';
	const changePassPHP_issue = 'Error de contraseña de validación. Por favor, compruebe y vuelva a introducir. Las contraseñas deben tener entre 8 y 70 caracteres de longitud.';

	//comments
	const comments_pleaseLogin = 'Por favor inicie sesión para comentar.';
	const comments_added = 'Comentario Añadido.';
	const comments_error = 'Error al añadir comentario, no comentario añadido.';
	const comments_title = 'Notas y Comentarios';
	const comments_noComments = 'Sin comentarios aún.';
	const comments_newComment = 'Introduzca un nuevo comentario aquí';
	const comments_reply = 'Respuesta';
    const comments_button = 'Enviar';


	//myPapers
	const myPapers_published = 'Publicados y revisados';
	const myPapers_noPublished = 'No hay artículos publicados';
	const myPapers_underReview = 'Someterse Revisión';
	const myPapers_noReview = 'No hay documentos en revisión.';

	//paperUtil
	const paperUtil_published = 'Publicado ';
	const paperUtil_isEndorsed1 = 'Este documento está pendiente de revisión.';
	const paperUtil_isEndorsed2 = 'En este trabajo se ha pasado la revisión.';
	const paperUtil_isEndorsed3 = 'En este trabajo se ha fallado revisión.';
	const paperUtil_isEndorsedError = 'Error respaldo lectura.';
	const paperUtil_cited = 'Citado ';
	const paperUtil_feedback = 'Notas y Comentarios';
	const paperUtil_fullPaper = 'Documento Completo';
	const paperUtil_fullText = 'Texto Completo:';

	//showPaper
	const showPaper_lostPaper = 'Lo siento, no puedo encontrar ese trabajo para usted.';

	//userDiscipline
	const userDiscipline_missingDisciplines = 'Este administrador no ha añadido ningún disciplinas a la página web todavía.';
	const userDiscipline_add = 'Añadir';

}

}else{ // ELSE IF included from another php source
if(headers_sent()){ die(include("404.html")); } // If headers were somehow already sent, just die to a custom 404 page
header("HTTP/1.1 404 Not Found"); // Send headers if not sent already
die(include("404.html")); // Die to custom 404 page
} // END ELSE IF included from another source

?>
