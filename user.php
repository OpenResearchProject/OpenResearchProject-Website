<?php
/////////////////////////////////////////////////////////////////
//
// user.php
// Sets up user data and manages it.
//
/////////////////////////////////////////////////////////////////

if(isset($includeCheck)){
// IF included from another source

class User{

	private $session, $sql;

	public function __construct($sql){
		// Sessions are attached to users, so define it here.
		$this->session =  new Sessions();
		$this->sql = $sql;
	}

	public function getIP(){
		foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
        		if (array_key_exists($key, $_SERVER) === true) {
            			foreach (explode(',', $_SERVER[$key]) as $ip) {
                			if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                    				return $ip;
                			}
            			}
        		}
    		}

		return false;
	}


	public function login($un, $pw, $stay){
		// Returns an array.
		// ['login'] = true if good, false if bad
		// ['status'] =  user's id number if good, -1 if incorrect login, and -2 if blocked for too many login attempts.


		$login = array();
		$num = $this->sql->login($un, $pw, $this->getIP());

		if($num > -1){
			$this->session->setup($un, $num); // give our session the username and user id

			if($stay === true){
				//setup cookies
			}

			$login['login'] = true;
			$login['status'] = $num;
			return $login;
		}else{
			$login['login'] = false;
			$login['status'] = $num;
			return $login;
		}
	}

	public function reauth($pw){
		// Like login, but it re-checks the current logged in user for security

		$login = array();
		$num = $this->sql->login($this->getName(), $pw, $this->getIP());

		if(($num > -1) && ($num === $this->getNumber())){
			$login['login'] = true;
                        $login['status'] = $num;
                        return $login;
                }else{
			$login['login'] = false;
                        $login['status'] = $num;
                        return $login;
                }
	}

	public function updateLastLogin($userNumber){
		// Updates the user's last login information
		return $this->sql->updateLastLogin($userNumber);
	}

	public function recordIP(){
		// Adds the user's IP to the IPs table; returns true/false on success/failure
		return $this->sql->addIP($this->getIP(), $this->getNumber());
	}

	public function logout(){
		$this->session->end();
	}

	public function getName(){
		return $this->session->get('user_name');
	}

	public function getRealName(){
		if($this->session->check('real_name')){
			return $this->session->get('real_name');
		}else{
			$userData = $this->sql->getUserData($this->getNumber());
			$this->session->set('real_name', $userData['userFullName']);
			unset($userData);
			return $this->session->get('real_name');
		}
	}

	public function getNumber(){
		// Returns the user's number
		return $this->session->get('user_number');
	}

	public function getTimezone(){
		// Gets the timezone from the database
		// This is in user since future versions should allow users to set their own timezones.
		// They are in the database as UTC and converted to the timezone given from this on print/echo.

		if($this->session->check('timezone')){  // Check if it is stored in session data
                 	return $this->session->get('timezone');
               	}else{
			$userData = $this->sql->getUserData($this->getNumber());

			if((isset($userData['timezone'])) && (!is_null($userData['timezone'])) && (strlen($userData['timezone']) > 2)){ // IF user has a preference for timezone
				$timezone = $userData['timezone'];
			}else{
				// if not, just get the default timezone
	                      	$timezone = $this->sql->getSetting('timezone');
				$timezone = $timezone['text'];
			}

			unset($userData);
                     	$this->session->set('timezone', $timezone);
                      	return $timezone;
             	}
	}

	public function setTimezone($timezone){
		// Sets the timezone for a user
		// Returns true/false on success/failure

		if($this->sql->setUserTimezone($this->getNumber(), $timezone)){
			$this->session->set('timezone', $timezone);
			return true;
		}else{
			return false;
		}
	}

	public function getLanguage(){
                // Gets the preferred language from the user. If it isn't set it gets the default language from the database.

                if($this->session->check('language')){  // Check if it is stored in session data
                        return $this->session->get('language');
                }else{

			$userData = $this->sql->getUserData($this->getNumber());

                        if((isset($userData['language'])) && (!is_null($userData['language'])) && (strlen($userData['language']) > 2)){ // IF user has a preference for a language
                                $language = $userData['language'];
                        }else{
                                // if not, just get the default language
                                $language = $this->sql->getSetting('language');
                                $language = $language['text'];
                        }

                        unset($userData);
                        $this->session->set('language', $language);
                        return $language;
                }
        }

	public function setLanguage($language){
                // Sets the language for a user
                // Returns true/false on success/failure

                if($this->sql->setUserLanguage($this->getNumber(), $language)){
			$this->session->set('language', $language);
                        return true;
                }else{
                        return false;
                }
        }

	public function setSessionLanguage($language){
		// Used on login to set the language to the user's preference

		$this->session->set('language', $language);
	}

	public function setSessionTimezone($timezone){
		// Used on login to set the timezone to the user's preference

		$this->session->set('timezone', $timezone);
	}

	public function getAccountActivation(){
		// Returns true if the user's email account has been verified
			$userData = $this->sql->getUserData($this->getNumber());

			if(strlen($userData['activationCode'])===0){
				return true;
			}else{
				return false;
			}
	}

	public function getBan($userNumber){
		// Returns true if the user is banned
		if(is_numeric($userNumber)){
			if($userNumber != (int)$userNumber){ die(lang::genericError); }

				$userData = $this->sql->getUserData($userNumber);
				if(!isset($userData['banDate'])){ // IF user hasn't been banned
					return false;
				}else{ // ELSE IF user hasn't been banned
					$banDate = date_create($userData['banDate']);
					$now = date_create();

					if($banDate <= $now){  // IF the user should be unbanned
						$this->sql->setBan($userNumber, null, null);
						return false;
					}else{  // ELSE IF the user should be unbanned
						return true;
					}  // END ELSE IF the user should be unbanned
				}  // END ELSE IF user hasn't been banned

		}else{
			die(lang::genericError);
		}
	}


	public function getBanInfo($userNumber){
		// Returns an array of ban information [banDate,banReason]
                if(is_numeric($userNumber)){
                        if($userNumber != (int)$userNumber){ die(lang::genericError); }
			$userData = $this->sql->getUserData($userNumber);
                     	$smallerArray = [ 'banDate' => $userData['banDate'], 'banReason' => $userData['banReason'] ];
			unset($userData);
			return $smallerArray;
                }else{
                        die(lang::genericError);
                }
	}

	public function getAvatar($userNumber){
		// Returns a user's avatar

		if(is_numeric($userNumber)){
                        if($userNumber != (int)$userNumber){ die(lang::genericError); }

			if($userNumber == $this->session->get('user_number')){  // IF we are getting our own avatar
				if($this->session->check('avatar')){  // Check if it is stored in session data
					return $this->session->get('avatar');
				}else{
					$userData = $this->sql->getUserData((int)$userNumber); // Set it in session data and return it
		                        $this->session->set('avatar', $userData['avatar']);
	                        	return $userData['avatar'];
				}
                	}else{ // ELSE IF we are getting our own avatar
				$userData = $this->sql->getUserData((int)$userNumber);
				return $userData['avatar'];
			} // END ELSE IF we are getting our own avatar
		}
		// Not having an else for is_numeric on getAvatar is fine since image.php will just show the default.png avatar.
	}

	public function addDiscipline($disciplineId){
		// Adds a discipline to the user's list and returns true/false on success/failure

		if((!is_numeric($disciplineId)) || ($disciplineId != (int)$disciplineId)){
			die(lang::genericError);
		}

		if($this->sql->addUserDiscipline($disciplineId, $this->getNumber())){
			return true;
		}else{
			return false;
		}

	}

	public function removeDiscipline($disciplineId){
		// Removes a discipline to the user's list and returns true/false on success/failure

		if((!is_numeric($disciplineId)) || ($disciplineId != (int)$disciplineId)){
                        die(lang::genericError);
                }

                if($this->sql->removeUserDiscipline($disciplineId, $this->getNumber())){
                        return true;
                }else{
                        return false;
                }

	}

	// Tokens for forms below:

	public function setToken($postfix = ''){
		$token = password_hash(uniqid(rand(), TRUE), PASSWORD_DEFAULT);
		$this->session->set('token_' . $postfix, $token);
	}

	public function checkToken($postfix = ''){
		return $this->session->check('token_' . $postfix);
	}

	public function getToken($postfix = ''){
		return $this->session->get('token_' . $postfix);
	}

	public function killToken($postfix = ''){
		$this->session->set('token_' . $postfix, null);
	}

	public function setAdminSessionTime($minutes = 5){
		// Sets the session time; if sessionTime < time on page load, reset user's reauth; session timeout
		$this->session->set('sessionTime', time() + ($minutes*60));
	}

	public function updateAdminSessionTime($minutes = 10){
		// Updates the session time; returns false if sessionTime < time

		if(time() > $this->session->get('sessionTime')){
			return false;
		}else{
			$this->session->set('sessionTime', time() + ($minutes*60));
			return true;
		}
	}

	public function getReauth($postfix = ''){
		// Returns if a user has been reauthenticated, aka logged in again
		if($this->session->check('reauthentication' . $postfix) == 1){
			return true;
		}else{
			return false;
		}
	}

	public function setReauth($postfix = ''){
		// Tells the system that the user has reauthenticated for security purposes
		$this->session->set('reauthentication' . $postfix, 1);
	}

	public function killReauth($postfix = ''){
		// Tells the system that the user needs to reauthenticate; session expired
		$this->session->set('reauthentication' . $postfix, null);
	}

	public function setAdmin(){
		$this->session->set('isAdmin', 1);
	}

	public function getAdmin(){
		// If the current user is an admin
		if($this->session->get('isAdmin')===1){
			return true;
		}else{
			$userData = $this->sql->getUserData($this->getNumber());

			if($userData['admin'] == 1){
				$this->setAdmin();
				unset($userData);
				return true;
			}else{
				unset($userData);
				return false;
			}

			unset($userData);
		}
	}

	public function killAdmin(){
		$this->session->set('isAdmin', null);
	}

} // END User Class

}else{ // ELSE IF included from another php source
if(headers_sent()){ die(include("404.html")); } // If headers were somehow already sent, just die to a custom 404 page
header("HTTP/1.1 404 Not Found"); // Send headers if not sent already
die(include("404.html")); // Die to custom 404 page
} // END ELSE IF included from another source

?>
