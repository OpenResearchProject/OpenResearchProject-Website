<?php

class Message{

	private $message = '';

	public function __construct(){
	}

	public function showMessage($messageNum){

		if(is_numeric($messageNum)){

			switch((int)$messageNum){
				case 1: $message = lang::genericError; break;
				case 2: $message = lang::login_alreadyLoggedIn; break;
				case 3: ''; break;
				case 4: $message = lang::login_good; break;
				case 5: $message = lang::logout; break;
				case 6:
					$banInfo = $banInfo = $this->user->getBanInfo($this->user->getNumber());
					$banDate = date_create($banInfo['banDate']);
					$message = lang::ban1 . date_format($banDate,"F d, Y") . lang::ban2 . $banInfo['banReason']; break;
				case 7: $message = lang::activation;break;
				case 8: $message = lang::register_success;break;
				case 9: $message = lang::paper_UploadSuccess; break;

				case 10: $message = lang::register_accountActivationGood; break;
				case 11: $message = lang::register_accountActivationBad; break;

				// Admin Panel Messages:
				case 30: $message = lang::admin_loginGood; break;
				case 31: $message = lang::admin_logout; break;
				case 32: $message = lang::admin_maxAttempts; break;
				case 33: $message = lang::admin_register_success; break;
				default: $message = lang::invalidMessage; break;
			}

			include('./pages/html/message.html');

		}else{
			$message = lang::invalidMessage;
			include('./pages/html/message.html');
		}

	}

	public function showLoginLockTime($timer){
        	$message = lang::login_maxAttempts1 . (int)$timer . lang::login_maxAttempts2;
		include('./pages/html/message.html');
	}

	public function showBan($banDate, $banReason){
                $theDate = date_create($banDate);
               	$message = lang::ban1 . date_format($theDate,"F d, Y") . lang::ban2 . $banReason;
		include('./pages/html/message.html');
	}

	public function showSkipLockTime($time){
		$message = lang::review_skipMax1 . (int)$time . lang::review_skipMax2;
		include('./pages/html/message.html');
	}

}

?>
