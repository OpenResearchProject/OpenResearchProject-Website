<?php

//header('Content-type: image/png'); // Force the Content Type


if((isset($_GET['i'])) &&  (is_numeric($_GET['i'])) && ($_GET['i'] == (int)$_GET['i'])) { // IF GET data is a valid number

	$userNumber = (int)$_GET['i'];
	$avatar = $user->getAvatar($userNumber);

	if(strlen($avatar) > 0){ // IF user has an avatar
		$size = getimagesize($uploads . 'avatars/' . $avatar);
		$fp = fopen($uploads . 'avatars/' . $avatar, "rb");

		if ($size && $fp) {
			// valid image
			$image_mime = image_type_to_mime_type(exif_imagetype($uploads . 'avatars/' . $avatar));

				if((strcmp($image_mime, 'image/png')===0) || (strcmp($image_mime, 'image/jpeg')===0) ||  (strcmp($image_mime, 'image/gif')===0)){
					header("Content-type: {$size['mime']}"); // Force the content type
					fpassthru($fp);
					fclose($fp);
				}else{
					fclose($fp);
					header('Content-type: image/png'); // Force the Content Type
					unlink($avatar);
					$newFp = fopen($uploads . 'avatars/invalid.png',"rb");
					fpassthru($newFp);
				}
		} else {
			// not a valid image
			fclose($fp);
			$newFp = fopen($uploads . 'avatars/invalid.png',"rb");
			header('Content-type: image/png'); // Force the Content Type
			fpassthru($newFp);
		}

	}else{ // ELSE IF user has an avatar
		$newFp = fopen($uploads . 'avatars/default.png',"rb");
		header('Content-type: image/png'); // Force the Content Type
		fpassthru($newFp);
	} // END ELSE IF user has an avatar


}else{ // ELSE IF GET data is a valid number
	$newFp = fopen($uploads . 'avatars/invalid.png',"rb");
	header('Content-type: image/png'); // Force the Content Type
	include($newFp);
} // END ELSE IF GET data is a valid number

?>
