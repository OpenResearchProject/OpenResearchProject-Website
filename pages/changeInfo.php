<?php
/////////////////////////////////////////////
//
//  changeInfo.php
//  allows user to update their User Info
//
//
////////////////////////////////////////////

	include($root . '/pages/html/header.html');

	echo '<div id="content">';

	//check if user is logged in
	if($user->getNumber() != -1){ // IF the user is logged in

		//get user info
		$userInfo = $sql->getUserData($user->getNumber());
		$userID = $userInfo['userId'];
		$username = $userInfo['userName'];
		$userFullName = $userInfo['userFullName'];
		$email = $userInfo['emailAddress'];
		$degree = $userInfo['degree'];
		$title = $userInfo['title'];
		$university = $userInfo['university'];
		$dbpassword = $userInfo['password'];

		//if user hasn't submitted changes yet...
		if(!isset($_POST['submit'])) {
			// Set security token:
			if(!$user->checkToken('changeInfo')){
				$user->setToken('changeInfo');
			}
			//display normal form
			include($root . '/pages/html/changeInfo.html');

		} else {//if they have submitted, then let's start to parse their info.
			//check to see what was entered...
			if(isset($_POST['userFullName']) && !empty(trim($_POST['userFullName']))) {
				$newUserFullName = $sql->db_safe($_POST['userFullName']);
			} else $newUserFullName = $userFullName;

			if(isset($_POST['emailAddress']) && !empty(trim($_POST['emailAddress']))) {
				$newEmail = $sql->db_safe($_POST['emailAddress']);
			} else $newEmail = $email;

			if(isset($_POST['degree']) && !empty(trim($_POST['degree']))) {
				$newDegree = $sql->db_safe($_POST['degree']);
			} else $newDegree = $degree;

			if(isset($_POST['title']) && !empty(trim($_POST['title']))) {
				$newTitle = $sql->db_safe($_POST['title']);
			} else $newTitle = $title;

			if(isset($_POST['university']) && !empty(trim($_POST['university']))) {
				$newUniversity = $_POST['university'];
			} else $newUniversity = $university;

			if(isset($_POST['password'])) { $password = $sql->db_safe($_POST['password']);}

			//check to make sure new email isn't already being used
			if($sql->getUserByEmail($newEmail)>=0 && ($newEmail != $email)){

				echo '<div class="material"><p>' . lang::changeInfo_emailInUse . '</p></div>';

			} else {
			//check to make sure password is correct
			if((password_verify($password, $dbpassword)) && ($_POST['token'] == $user->getToken('changeInfo'))){

				// Kill our security token so it can't be reused
				$user->killToken('changeInfo');

				//update database
				$sql->updateUserInfo($userID, $newUserFullName, $newEmail, $newDegree, $newTitle, $newUniversity);
				//display success message
				echo '<div class="material"><p>' . lang::changeInfo_updated . '</p></div>';
			} else {//end validate password
				echo '<div class="material"><p>' . lang::changeInfo_badPassword . '</p></div>';
			} //end else validate password
			} //end else username in use
		}

	} else {//if the user isn't logged in
		die(lang::genericError);
	}

	echo '</div>';
	include($root . '/pages/html/footer.html');
?>
