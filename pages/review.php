<?php
/////////////////////////////////////////////
//
//  review.php
//  This page allows users to request, edit,
//  skip, and continue reviews for papers.
//
////////////////////////////////////////////

include($root . '/pages/html/header.html');

$sql->removeExpiredReviews(); // Remove expired reviews from the database; ie. Reviews that ran out of time

if((isset($_POST['skip'])) && ($_POST['token'] == $user->getToken('review'))){ // IF the user is skipping
	$reviewData = $sql->getUserReview($user->getNumber());
        $reviewId = (int)$reviewData['peerReviewId'];

	if($sql->removeUserReview($reviewId)){ // IF review removed from database
		$userData = $sql->getUserData($user->getNumber());

		$userSkipCount = $userData['skipCount'];
		$sql->setReviewSkip($user->getNumber(), $userSkipCount+1, time());
	} // END IF review removed from database

	$user->killToken('review');
} // END IF the user is skipping

$userData = $sql->getUserData($user->getNumber()); // Get user data to check for skips; ALWAYS after the skip check above!

$userSkipCount = $userData['skipCount']; // get skipCount and Time
$userSkipTime = $userData['skipTime'];
unset($userData);

$skipCount = $sql->getSetting('reviewSkipCount'); // get maximum allowed skips and skip time
$skipTime = $sql->getSetting('reviewSkipLockTime');
$skipTime = $skipTime['num']*60*60; // convert to seconds

if(($userSkipTime + $skipTime) < time()){ // IF skipLock has expired
       	$sql->setReviewSkip($user->getNumber(), 0, null); // reset skip counter
	$skipLock = false;
	$userSkipCount = 0;
} // END IF skipLock has expired

if($userSkipCount >= $skipCount['num']){ // IF the user has used all of their skips
	$lockTime = ($userSkipTime + $skipTime) - time();
	$lockTime = ($lockTime/60)/60; // convert to hours
	die($message->showSkipLockTime((int)$lockTime));
}else{ // ELSE IF the user has used all of their skips
	$skipLock = false;
} // END ELSE IF the user has used all of their skips

if($skipLock == false){ // IF the user isn't locked for skips
	if((isset($_POST['submit'])) && ($_POST['token'] == $user->getToken('review'))){ // IF the user wants to review a paper
	        $setReview = $sql->setUserReview($user->getNumber()); // give the user a paper to review
		$user->killToken('review');
	}

	$reviewOut = $sql->checkUserReview($user->getNumber()); // if the user is currently reviewing a paper, this should always be AFTER setting a review

	if($reviewOut){
		$reviewData = $sql->getUserReview($user->getNumber());
		$reviewId = (int)$reviewData['peerReviewId'];
		$paperData = $sql->getPaperInfo($reviewData['paperId']);

		$maxReviewTimeSetting = $sql->getSetting('reviewTime');
		$maxReviewTime = $maxReviewTimeSetting['num']*24*60*60; // Convert days to seconds
		$reviewTimeRemaining = $maxReviewTime - ( time() - $reviewData['startTime'] );
		$reviewTimeRemaining = (($reviewTimeRemaining/60)/60)/24; // Convert seconds to days
	}

	if(((isset($_POST['saveForLater'])) || (isset($_POST['submitReview']))) && ($_POST['token'] == $user->getToken('review'))){ // IF saving or submitting
		if(isset($_POST['endorse'])){ $endorse = 1; }else{ $endorse = 0; }
		if(isset($_POST['submitReview'])){ $done = 1; }else{ $done = 0; }

		if(($done == 0) && ($reviewData['isEndorsed'] == $endorse) && (strcmp($reviewData['comment'], $_POST['comments']) == 0)){
			$updated = true; // Nothing has changed, just tell the user that it is saved
		}else{
			// Update the database with the new settings:
			$updated = $sql->updateUserReview($reviewId, $_POST['comments'], $done, $endorse);

			// Double check reviewOut since it may have changed
			$reviewOut = $sql->checkUserReview($user->getNumber());

			if($reviewOut){
				// Get Review Data again:
				$reviewData = $sql->getUserReview($user->getNumber());
			}
		}

		$user->killToken('review');
	} // END IF saving or submitting
} // END IF the user isn't locked for skips

$skips = $sql->getSetting('reviewSkipCount');
$skipTimeout = $sql->getSetting('reviewSkipLockTime');
$disciplines = $sql->getUserCategoryList($user->getNumber());
$canReview = $sql->getFullUserCategoryList($user->getNumber());

// Check if the user has disciplines added:
if(!is_null($disciplines)){
   	if(count($disciplines) > 0){
        	$hasDisciplines = true;
      	}else{
            	$hasDisciplines = false;
     	}
}else{
    	$hasDisciplines = false;
}

// Its important that the token setter isn't in the submit! That way the token isn't valid for attacks.
if(!$user->checkToken('review')){
$user->setToken('review');
}

include($root . '/pages/html/review.html');
include($root . '/pages/html/footer.html');

?>
