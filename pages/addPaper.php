<?php
/////////////////////////////////////////////
//
//  addPaper.php
//  This page adds a paper in text form for
//  initial functionality and for later
//  functionality to mine data.
//
////////////////////////////////////////////

if($user->getNumber() > -1){ // IF the user is logged in
	//#these values can be null
	$publisherId = 1;
	$sponsor = NULL;
	$paperURL = NULL;
	$copyRightOwner = NULL;
	$copyRightDate = NULL;
	//#if selection only checks for mandatory fields, others should be null if not set.
	if ((isset($_POST['paperName']) && (isset($_POST['paperPublishDate'])) && (isset($_POST['digObjId'])) && (isset($_POST['digObjIdUrl'])) && (isset($_POST['abstract'])) && ($_POST['token'] == $user->getToken('addPaper')) && (isset($_POST['discipline'])) && (is_numeric($_POST['discipline'])) && ($_POST['discipline'] == (int)$_POST['discipline']) && (isset($_POST['paperText']))))
	{
	        $paperName = trim($_POST['paperName']);
	        $paperURL = ($_POST['paperURL']);
	        $paperPublishDate = trim($_POST['paperPublishDate']);
	        $publisherId = ($_POST['publisherId']);
	        $abstract = trim($_POST['abstract']);
	        $sponsor = ($_POST['sponsor']);
	        $digObjIdUrl = ($_POST['digObjIdUrl']);
	        $digObjId = trim($_POST['digObjId']);
	        $copyRightOwner = trim($_POST['copyRightOwner']);
	        $copyRightDate = trim($_POST['copyRightDate']);
	        $paperText = trim($_POST['paperText']);
		$discipline = $_POST['discipline'];
	        $paperId = $sql->addPaper($paperName, $paperURL, $paperPublishDate, $publisherId, $abstract, $sponsor, $digObjIdUrl, $digObjId, $copyRightOwner, $copyRightDate, $paperText);
	        //if added
	        if ($paperId > -1)
	        {
			include($root . '/pages/html/header.html');
			$user->killToken('addPaper');

			$userId = $user->getNumber();
			$success = $sql->addAuthor($userId, $paperId); //create associative entry between paper and author

			$success2 = $sql->addPaperToCategory($paperId, $discipline);

			if(($success > -1) && ($success2 > -1)){
				$message->showMessage(9); // Show upload success message
			}else{
				include($root . '/pages/html/header.html');
	                        $message->showMessage(1); // Show generic error message
			}
	        }else{
			include($root . '/pages/html/header.html');
	        	$message->showMessage(1); // Show generic error message
	        }
	}else{

		// Its important that the token setter isn't in the submit! That way the token isn't valid for attacks.
               	if(!$user->checkToken('addPaper')){
                 	$user->setToken('addPaper');
             	}

		$disciplines = $sql->getFullUserCategoryList($user->getNumber()); // Get the user's disciplines
		$disciplineSelector = '<select name="discipline">' . "\n";

		if(count($disciplines) > 0){
			foreach($disciplines as $item){
				$disciplineSelector .= '<option value="' . $item['id'] . '">' . $item['name'] . '</option>' . "\n";
			}
		}

		$disciplineSelector .= '</select>' . "\n";

		include($root . '/pages/html/header.html');
		include($root . '/pages/html/addPaper.html');
	}

	include($root . '/pages/html/footer.html');
}else{ // ELSE IF the user is logged in
	include($root . '/pages/login.php');
} // END IF the user is logged in
?>
