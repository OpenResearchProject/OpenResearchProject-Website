<?php
/////////////////////////////////////////////
//
//  
//  
//  
//
////////////////////////////////////////////

include($root . '/pages/paperUtil.php');

include($root . '/pages/html/header.html');
//include($root . '/pages/html/comments.html'); //most of the html is php generated, so I've moved that code here -Turner
?>

<script src="pages/commentDisplay.js" ></script>
<div id="content">

<?php

//Code for taking in POST data and pushing a new comment to the database here:

if(isSet($_POST['commentText']) && isSet($_POST['paperid']) && is_numeric($_POST['paperid'])){
	//declare and set the necessary variables
	$commentPaperID = $sql->db_safe($_POST['paperid']);
	$commentUserID = $user->getNumber();
	$commentContent = $sql->db_safe($_POST['commentText']);
	
	if(isSet($_POST['parentid']) && is_numeric($_POST['parentid'])){
		$commentParent = $sql->db_safe($_POST['parentid']);
	} else {
		$commentParent = NULL;
	}
	
	//safety check
	if($commentUserID < 0){
		echo '<div class="material"><p>' . lang::comments_pleaseLogin . '</p></div>';
	} else {
		//input the data to the database
		$check = $sql->addComment($commentPaperID, $commentUserID, $commentContent, $commentParent);
		if($check){
			echo '<div class="material"><p>' . lang::comments_added . '</p></div>';
		} else {
			echo '<div class="material"><p>' . lang::comments_error . '</p></div>';
		}
	}
}

//Code for displaying the current comments here:
//pull in paperID as GET data
// use is_numeric(), cast to int and check if int is equal to get
if (!isSet($_GET['paper']) || !is_numeric($_GET['paper'])){
	//error code here
	die(lang::genericError);
}else {
	$paperID = (int) $_GET['paper'];
	if($paperID == 0){
		//error code here
		die(lang::genericError);
	} else {
		//Pull paperInfo into variables to populate page using paperID
		$paperInfo = $sql->getPaperInfo($paperID);
		//display main paperinfo
		displayContent($paperInfo, $user);
		echo '<h1>' . lang::comments_title . '</h1>';
		echo '<div class="paperInfo">';
		echo '<div class="commentSection">';
		//Store comment information as an array
		$comments = $sql->getPaperComments($paperID);
		//populate page with comments based on array
		$parents = array();
		$children = array();
		
		//show "No Comments Yet" if no comments exist
		if (count($comments) <= 0){
			echo '<p>' . lang::comments_noComments . '</p>';
		} else { //if there are comments...
		
			//assign comments as parents or children
			foreach ($comments as $comment){
				if ($comment['parent_id'] === NULL){
					$parents[$comment['paperCommentId']][] = $comment;
				} else {
					$children[$comment['parent_id']][] = $comment;
				}
			}
		
			//display the comments
			foreach ($parents as $commentList){
				displayComment($commentList, $parents, $children);
			}
		}
		
		echo '</div><!--end commentSection -->';
		echo '</div><!--end paperInfo -->';
		
		
		//Show the root comment entry form
		displayThreadStarter($paperID);
		
	}
}
?>
</div><!-- end Content -->
<?php

include($root . '/pages/html/footer.html');
//End of page

/////////////
//HTML generation functions below:
/////////////

function displayComment($commentList, $parents, $children){
	foreach($commentList as $comment){
		//markup html
		displayCommentBeginning($comment);
		//display subComments
		if (isset($children[$comment['paperCommentId']])){
			displayComment($children[$comment['paperCommentId']], $parents, $children);
		}
	//end comment div
	echo '</div> <!-- end comment -->';
	}
}

function displayContent($paperInfo, $user){
	$authors = parseAuthors($paperInfo);
	
	//display paper
	displayPaperInfo($paperInfo[0], $authors[$paperInfo[0]['paperID']], $user, false);
}

function displayCommentBeginning($comment){
	
	//get comment info
	$username = $comment['username'];
	$quote;
	$text = $comment['content']; //assume $text contains correct html formatting (such as p tags)
	$paperID = $comment['paperID'];
	$parent = $comment['parent_id'];
	$commentID = $comment['paperCommentId'];
    $userID = $comment['userID'];
	//echo out html with comment info included (NOT YET DONE)
?>
<div class="comment">
	<img class="fleft" src="index.php?i=<?php echo $userID; ?>" height="64" width="45" alt="User Image"/>
    <div class="commentContent">
        <h4><a href=""></a><?php echo $username; ?></h4>
        <!-- <h4><a href="">On: </a></h4> --> <!-- future feature: specific line the comment responds to -->
        <?php echo '<pre>'.$text.'</pre>'; //assuming $text has correct html formatting (including p tags)?> 
        <div class="flatButtons"> <a href="#commentReply<?php echo $commentID; ?>" id="<?php echo $commentID; ?>"><?php echo lang::comments_reply; ?></a> </div>
    </div>
    <div class="commentReply" id="commentReply<?php echo $commentID; ?>">
       	<form method="POST" action="?p=9&paper=<?php echo $paperID; ?>" >
           	<input type="hidden" value="<?php echo $commentID; ?>" name="parentid"/>
            <input type="hidden" value="<?php echo $paperID; ?>" name="paperid"/>
            <textarea name="commentText" rows="4"></textarea>
            <input type="submit" value="<?php echo lang::comments_button; ?>"/>
        </form>
  </div><!-- end commentReply -->
<?php
	
}

function displayThreadStarter($paperID){
	
?>

    <div class="paperInfo">
    	<p><?php echo lang::comments_newComment; ?></p>
    	<div class="commentReply" style="display:block">
          	<form method="POST" action="?p=9&paper=<?php echo $paperID; ?>" >
            	<input type="hidden" value="" name="parentid"/>
                <input type="hidden" value="<?php echo $paperID; ?>" name="paperid"/>
                <textarea name="commentText" rows="4"></textarea>
                <input type="submit" value="<?php echo lang::comments_button; ?>"/>
            </form>
          </div><!-- end commentReply -->
    </div><!--end PaperInfo -->
	
<?php
}
?>
