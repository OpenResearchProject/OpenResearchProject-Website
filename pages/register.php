<?php
///////////////////////////////
//
//  register.php
//  New users register using this
//  page when enabled in the admin
//  panel. Included by index.php
//
//////////////////////////////

$registrationSetting = $sql->getSetting('registration');

if(($registrationSetting['num'] == 1) || ($registrationSetting['num'] == 2)){ // IF user registration enabled; 1 = no verification; 2 = email verification
	if($user->getNumber()!=-1){ // IF user is already logged in
		include($root . '/pages/html/header.html');
	        $message->showMessage(2);
	        include($root . '/pages/html/footer.html');
	}else{ // ELSE IF user is already logged in
		if((isset($_POST['submit'])) && ($_POST['token'] == $user->getToken('register'))){ // IF post data was sent

			$registerErrors = [];

			$userFullName = $sql->db_safe($_POST['userFullName']);
		        $userName = $sql->db_safe($_POST['userName']);
		        $password = $sql->db_safe($_POST['password']);
			$passwordVerify = $sql->db_safe($_POST['passwordVerify']);
			$email = $sql->db_safe($_POST['emailAddress']);

			if((strlen($userName)<3) || (strlen($userName)>25)){ array_push($registerErrors, lang::register_userNameLength); }
	                if((strlen($password)<8) || (strlen($password)>70)){ array_push($registerErrors, lang::register_passwordLength); }
	                if(strcmp($password, $passwordVerify) !== 0){ array_push($registerErrors, lang::register_passwordMatch); }
	                if(strlen($email)<3){ array_push($registerErrors, lang::register_emailValid); }
			if(strlen($userFullName)<4){ array_push($registerErrors, lang::register_fullNameLength); }

			// Check if username or email is already in use:
			if($sql->getUserByUsername($userName)>=0){ array_push($registerErrors, lang::register_userNameInUse); }
			if($sql->getUserByEmail($email)>=0){ array_push($registerErrors, lang::register_emailInUse); }


			if(count($registerErrors)>0){ // IF there is an error
				unset($password); unset($passwordVerify);
				include($root . '/pages/html/header.html');
	                        include($root . '/pages/html/register.html');
	                        include($root . '/pages/html/footer.html');
			}else{ // ELSE IF there is an error

				$user->killToken('register');
				date_default_timezone_set('UTC'); // UTC is the database's default timezone. Convert it on echo/print.
				$joinDate = date("Y-m-d H:i:s");
		            	$uid = hash('sha256', mt_rand(0,100000) . rand(1,1000) . $joinDate . $userName);
				$ip = $user->getIP();

				if($registrationSetting['num'] == 2){
					// IF users need to verify their email address
					$activationCode = hash('sha256', mt_rand(0,100000) . rand(1,1000) . $joinDate . $userName);

					$subject = lang::register_emailSubject;
			                $body = lang::register_emailBody1 . $userName . lang::register_emailBody2;
			                $body .= '<a href="' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?aac=' . $activationCode . '" target="_blank">' .$_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?aac=' . $activationCode . '</a>';
			                $headers = 'MIME-Version: 1.0' . "\r\n";
			                $headers .= 'Content-type:text/html;charset=UTF-8' . "\r\n";
			                $issent = mail($email, $subject, $body, $headers);
					if(!$issent){ die(lang::register_emailCodeError); }
				}else{
					$activationCode = null;
				}

				// Insert user into database, addUser hashes the password for us:
				$userNumber = $sql->addUser($userName, $password, $joinDate, $email, $uid, $activationCode, 0, $userFullName);

				unset($password); unset($passwordVerify); unset($uid); unset($ip);


				if($userNumber > -1){ // IF registration worked, ie. returned > -1, the user's number

					include($root . '/pages/html/header.html');
		                        $message->showMessage(8); // User Registered Successfully
		                        include($root . '/pages/html/footer.html');

				}else{ // ELSE IF registration worked, ie. returned > -1, the user's number
					die(lang::genericError);
				} // END ELSE IF registration worked, ie. returned > -1, the user's number

			} //END ELSE IF there is an error
		}else{ // ELSE IF post data was sent


			// Its important that the token setter isn't in the submit! That way the token isn't valid for attacks.
	                if(!$user->checkToken('register')){
	               		$user->setToken('register');
	                }

			include($root . '/pages/html/header.html');
	                include($root . '/pages/html/register.html');
	                include($root . '/pages/html/footer.html');
		} // END ELSE IF post data was sent
	} // END ELSE IF user is already logged in
} // END IF user registration enabled
?>
