<?php
//html display functions
function displayPaperInfo($paperInfo, $authorArray, $user, $notesButton = true, $fullButton = true){
	//get paper info
	$paperID = $paperInfo['paperID'];
	$paperName = $paperInfo['paperName']; 
	$date = $paperInfo['paperDate'];
	$abstract = $paperInfo['paperAbstract'];
	$citations = 0; //get number of citations - not yet working, UPDATE GETPAPERINFO TO INCLUDE CITATIONS LATER
	$isEndorsed = $paperInfo['isEndorsed'];
	//plug values in below (NOT YET DONE)
?>
    <div class='paperInfo'>  <img class='paperImage' src="pages/img/defaultPaperImage.png" alt="Default Paper Image" width="45" height="64"/>
      <div class="paperTitleInfo">
        <h2 class="pTitle"><?php echo $paperName; ?></h2>
        <h3 class="pAuthors"><?php 
		
		//echo implode(', ', $authorArray); 
		$aCount = 0;
		foreach($authorArray as $author){
			if($aCount > 0) echo ', ';
			echo '<a href="?p=4&u=' . $author['userId'] . '">' . $author['name'] . '</a>';
			$aCount++;
		}
		
		?></h3>
        <h4 class="pYear"><?php echo lang::paperUtil_published;
		$datetime = new DateTime($date); // Get paperPublishDate as datetime object
		$timezone = new DateTimeZone($user->getTimezone()); // Set our new timezone to convert to
		$datetime->setTimezone($timezone); // Convert the timezone
		echo $datetime->format('Y-m-d'); ?></h4>
<?php
switch($isEndorsed){
	case 1: echo lang::paperUtil_isEndorsed1; break;
	case 2: echo lang::paperUtil_isEndorsed2; break;
	case 3: echo lang::paperUtil_isEndorsed3; break;
	default: echo lang::paperUtil_isEndorsedError; break;
}
?>
        <!--<h4 class="pCitations"><?php //echo lang::paperUtil_cited . $citations; ?> times</h4> -->
      </div>
      <!--End paperTitleInfo -->
      <p class="abstract"><?php echo $abstract; ?></p>
       <div class="paperButtons">
                	<ul><?php
                      if($notesButton){?>
                	  <li><a href="?p=9&paper=<?php echo $paperID; ?>"><?php echo lang::paperUtil_feedback; ?></a></li>
                      <?php } 
					  if($fullButton){?>
                      <li><a href="?p=11&paperId=<?php echo $paperID; ?>"><?php echo lang::paperUtil_fullPaper; ?></a></li>
                      <?php } ?>
                	</ul>
      </div><!--End PaperButtons-->
      </div><!-- end paperInfo -->

<?php
}

function parseAuthors($paperInfo){
	$paperAuthor;
	if(count($paperInfo) > 0){
		foreach($paperInfo as $paper){
			//creating the author array
			if (!isset($paperAuthor[$paper['paperID']])){
				$paperAuthor[$paper['paperID']][] = array('name' => $paper['author'], 'userId' => $paper['userId']);	
			} else {
				array_push($paperAuthor[$paper['paperID']], array('name' => $paper['author'], 'userId' => $paper['userId'])); 
			}
		}
		return $paperAuthor;
	} else return -1;
}



function displayFullPaper($paperInfo, $authorArray, $user){
	//get paper info
	$paperID = $paperInfo['paperID'];
	$paperName = $paperInfo['paperName']; 
	$date = $paperInfo['paperDate'];
	$abstract = $paperInfo['paperAbstract'];
	$paperText = $paperInfo['paperText'];
	$citations = 0; //get number of citations - not yet working, UPDATE GETPAPERINFO TO INCLUDE CITATIONS LATER
	$isEndorsed = $paperInfo['isEndorsed'];
?>
    <div class='paperInfo'>  <img class='paperImage' src="pages/img/defaultPaperImage.png" alt="Default Paper Image" width="45" height="64"/>
      <div class="paperTitleInfo">
        <h2 class="pTitle"><?php echo $paperName; ?></h2>
        <h3 class="pAuthors"><?php 
		
		//echo implode(', ', $authorArray); 
		$aCount = 0;
		foreach($authorArray as $author){
			if($aCount > 0) echo ', ';
			echo '<a href="?p=4&u=' . $author['userId'] . '">' . $author['name'] . '</a>';
			$aCount++;
		}
		
		?></h3>
        <h4 class="pYear"><?php echo lang::paperUtil_published;
				$datetime = new DateTime($date); // Get paperPublishDate as datetime object
		                $timezone = new DateTimeZone($user->getTimezone()); // Set our new timezone to convert to
		                $datetime->setTimezone($timezone); // Convert the timezone
		                echo $datetime->format('Y-m-d');  ?></h4>
        <!--<h4 class="pCitations">Cited <?php //echo lang::paperUtil_cited . $citations; ?> times</h4> -->
<?php
switch($isEndorsed){
        case 1: echo lang::paperUtil_isEndorsed1; break;
        case 2: echo lang::paperUtil_isEndorsed2; break;
        case 3: echo lang::paperUtil_isEndorsed3; break;
        default: echo lang::paperUtil_isEndorsedError; break;
}
?>
      </div>
      <!--End paperTitleInfo -->
      <p class="abstract"><?php echo $abstract; ?></p>
       <div class="paperButtons">
                	<ul>
                	  <li><a href="?p=9&paper=<?php echo $paperID; ?>"><?php echo lang::paperUtil_feedback; ?></a></li>
                      <!--<li><a href="?p=11&paperId=<?php //echo $paperID; ?>"><?php echo lang::paperUtil_fullPaper; ?></a></li>-->
                	</ul>
      </div><!--End PaperButtons-->
      <h2 style="clear:both;"><?php echo lang::paperUtil_fullText; ?></h2>
      <div class="paperText"><?php echo nl2br($paperText); ?></div>
      </div><!-- end paperInfo -->

<?php
}

?>
