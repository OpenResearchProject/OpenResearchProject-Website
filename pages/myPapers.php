<?php
/////////////////////////////////////////////
//
//  myPapers.php
//  Display's a user's own uploaded papers.
//
////////////////////////////////////////////
include($root . '/pages/paperUtil.php'); //displayPaperInfo is defined here

if($user->getNumber() != -1){ // IF the user is logged in
	include($root . '/pages/html/header.html');
	//include($root . '/pages/html/myPapers.html'); //so much html is php generated, I'm not sure if I can really put together an html file for this -Turner
?>
<div id="content">

<?php
$currentUser = $user->getNumber();
$publishedPapers = array();
$underReviewPapers = array();
$paperAuthor[] = array();

if($currentUser != -1){
	$paperInfo = $sql->getPaperInfoByUserID($currentUser);
	//sort an Authors array for easier processing and display of data
	//and sort the results into published and unpublished
	if(count($paperInfo) > 0){
		foreach($paperInfo as $paper){
			//creating the author array
			if (!isset($paperAuthor[$paper['paperID']])){
				$paperAuthor[$paper['paperID']][] = array('name' => $paper['author'], 'userId' => $paper['userId']);	
			} else {
				array_push($paperAuthor[$paper['paperID']], array('name' => $paper['author'], 'userId' => $paper['userId'])); 
			}
			//sorting published and not published
			if($paper['endorsed'] == 2){
				$publishedPapers[] = $paper; //creating the published array
			} else {
				$underReviewPapers[] = $paper; //creating the unpublished array
			}
		}
	}

} else {
	die(lang::genericError);
}
?>

<h1><?php echo lang::myPapers_published; ?></h1>
<?php

	if(count($publishedPapers) > 0){
		$prevID = -1; //check for redundant papers in the array;
		foreach($publishedPapers as $paper){
			if ($prevID != $paper['paperID']){ //we only display the paper if it's a new paper.  This assumes the getPaperByUserID method gives papers in order of paperID
				displayPaperInfo($paper, $paperAuthor[$paper['paperID']], $user); //pass the paper info, along with the correct Author array for that paper
				$prevID = $paper['paperID'];
			}
		}
	} else {
		echo '<div class="material"><p>' . lang::myPapers_noPublished . '</p></div>';
	}

?>

<h1><?php echo lang::myPapers_underReview; ?></h1>

<?php

	if(count($underReviewPapers) > 0){
		$prevID = -1; //check for redundant papers in the array;
		foreach($underReviewPapers as $paper){
			if ($prevID != $paper['paperID']){ //we only display the paper if it's a new paper.  This assumes the getPaperByUserID method gives papers in order of paperID
				displayPaperInfo($paper, $paperAuthor[$paper['paperID']], $user); //pass the paper info, along with the correct Author array for that paper
				$prevID = $paper['paperID'];
			}
		}
	} else {
		echo '<div class="material"><p>' . lang::myPapers_noReview . '</p></div>';
	}

?>

</div> <!--end Content-->

<?php
	
	include($root . '/pages/html/footer.html');
}else { // ELSE IF the user is logged in
	include($root . '/pages/login.php');
} // END ELSE IF the user is logged in

