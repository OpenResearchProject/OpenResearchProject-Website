<?php
/////////////////////////////////////////////
//
//  userDiscipline.php
//  A page to let users select their discipline
//  of study.
//
////////////////////////////////////////////

if($user->getNumber() >= 0){

	include($root . '/pages/html/header.html');


	if((isset($_GET['add'])) && (isset($_GET['ud'])) && ($_GET['ud'] == $user->getToken('userDiscipline'))){ // IF user is adding

		$newDiscipline = (int)$_GET['add'];

		$userDisciplines = $sql->getUserCategoryList($user->getNumber());
		$exists = false;

		if(count($userDisciplines) > 0){
			foreach($userDisciplines as $item){ // check if user already has it
				if($item['id'] == $newDiscipline){
					$exists = true;
				}
			}
		}

		if(!$exists){ // IF it wasn't found in the user's list

			$disciplines = $sql->getCategoryList();

			foreach($disciplines as $item){  // make sure it exists in the disciplines list
				if($item['id'] == $newDiscipline){
					$exists = true;
				}
			}

			if($exists){ // IF it was found in the all disciplines list
				if(!$user->addDiscipline($newDiscipline)){
					die(lang::genericError);
				}
			} // END IF it was found in the all disciplines list
		} // END IF it wasn't found in the user's list
	} // END IF user is adding


	if((isset($_GET['delete'])) && (isset($_GET['ud'])) && ($_GET['ud'] == $user->getToken('userDiscipline'))){ // IF user is deleteing

		$disciplineToDelete = (int)$_GET['delete'];

		if(!$user->removeDiscipline($disciplineToDelete)){
                   	die(lang::genericError);
               	}
        } // END IF user is deleteing


	// Load below AFTER add/delete to get new list and reset tokens!

		$userDisciplines = $sql->getUserCategoryList($user->getNumber());
		$disciplines = $sql->getCategoryList();

		if(!$user->checkToken('userDiscipline')){
                	$user->setToken('userDiscipline');
        	}


		function getAllDisciplines($array, $refNumber = 0, $depth = 0, $token){
	                // Recursive function that turns our array into a nice list
	                // Runs in O(n^2). There is probably a better way to do this.

	                $list = '';
	                $count = count($array);

	                $parent = true;
	                $child = false;

			if($count > 0){ // IF there are items in the array
				foreach($array as $item){
		                        if($item['reference'] == (int)$refNumber){

		                                if($parent == true){ // If we are on a parent row
		                                        $list .= "\n" . '<ul>' . "\n";
		                                        $parent = false;
		                                        $child = true;
		                                }


		                                $list .= '<li><a href="./index.php?p=5&dis=' . $item['id'] . '" target="_blank">' . $item['name'] . '</a>&nbsp;|&nbsp;<a class="add" href="./index.php?p=21&add=' . $item['id'] . '&ud=' . $token . '">' . lang::userDiscipline_add . '</a></li>';


		                                $newList = getAllDisciplines($array, $item['id'], $depth + 1, $token);

		                                if(strlen($newList) > 1){
		                                        $list .= $newList . "\n";
		                                }
		                        }
		                }

		                if($child == true){
		                        $list .= '</ul>' . "\n";
		                }

				}else{ // ELSE IF there are items in the array
		                        $list = lang::userDiscipline_missingDisciplines;
		                } // END ELSE IF there are items in the array

		                return $list;
		        } // END function


	include($root . '/pages/html/userDiscipline.html');
	include($root . '/pages/html/footer.html');
}else{
        include($root . '/pages/login.php');
}

?>
