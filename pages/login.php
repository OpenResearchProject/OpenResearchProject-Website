<?php
//////////////////////////////
//
// login.php
//
//////////////////////////////

$loginErrors = []; // initialize an errors array

if($user->getNumber() >= 0){ // IF we are somehow already logged in
	include($root . '/pages/html/header.html');
	$message->showMessage(2);
	include($root . '/pages/html/footer.html');

}else{ // ELSE IF we are somehow already logged in
	if((isset($_POST['submit'])) && (isset($_POST['username'])) && (isset($_POST['password']))){ // IF we are logging in
		if($_POST['token'] == $user->getToken('login')){ // IF token is good

			$username = $sql->db_safe($_POST['username']);
		        $password = $sql->db_safe($_POST['password']);
		        if(isset($_POST['stay'])){ $stay=true; }else{ $stay=false; }

			$login = $user->login($username, $password, $stay); // $user->login returns an array and sets up session data. ['login'] = true/false; ['status'] = error messages or user id
			if($login['login']){
				$user->killToken('login');

				if($user->getAccountActivation()){ // IF the account is activated
					if(!$user->getBan((int)$user->getNumber())){ // IF the user isn't banned
						$user->updateLastLogin($user->getNumber());
						$user->recordIP();

						$userData = $sql->getUserData($user->getNumber());

						if(!is_null($userData['language'])){
		 					$user->setSessionLanguage($userData['language']);
						}
						if(!is_null($userData['timezone'])){
							$user->setSessionTimezone($userData['timezone']);
						}
						unset($userData);


						include($root . '/pages/html/header.html');
					        $message->showMessage(4); // good login message
					        include($root . '/pages/html/footer.html');
					}else{ // ELSE IF the user isn't banned
						$banInfo = $user->getBanInfo($user->getNumber());
						$user->logout();
						include($root . '/pages/html/header.html');
                                                $message->showBan($banInfo['banDate'], $sql->xssafe($banInfo['banReason'])); // ban message
						$user->logout();
                                                include($root . '/pages/html/footer.html');
					} // END ELSE IF the user isn't banned
				}else{ // ELSE IF the account is activated
					//account needs activation
					$user->logout();
					include($root . '/pages/html/header.html');
                                        $message->showMessage(7); // account activation message
                                        include($root . '/pages/html/footer.html');
				} // END ELSE IF the account is activated
			}else{
				if($login['status'] == -1){ // Bad Login
					include($root . '/pages/html/header.html');
					array_push($loginErrors, lang::login_bad);
		                        include($root . '/pages/html/login.html');
		                        include($root . '/pages/html/footer.html');
				}

				if($login['status'] == -2){ // Max Login Attempts
					include($root . '/pages/html/header.html');
					$timer = $sql->getSetting('loginLockTime');
					$message->showLoginLockTime($timer['num']);
                                        include($root . '/pages/html/footer.html');
				}
			}
		}else{ // ELSE IF token is good
                	include($root . '/pages/html/header.html');
	                $message->showMessage(1);
	                include($root . '/pages/html/footer.html');
	        } // END ELSE IF token is good

	}else{ // ELSE IF we are logging in (aka POST data NOT sent):

			// Its important that the token setter isn't in the submit! That way the token isn't valid for attacks.
			if(!$user->checkToken('login')){
				$user->setToken('login');
			}

			include($root . '/pages/html/header.html');
			include($root . '/pages/html/login.html');
			include($root . '/pages/html/footer.html');
	} // END ELSE IF we are logging in
} //END ELSE IF we are somehow already logged in

?>
