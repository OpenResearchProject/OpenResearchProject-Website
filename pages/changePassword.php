<?php
/////////////////////////////////////////////
//
//  changePassword.php
//  allows user to update their Password
// 
//
////////////////////////////////////////////

	include($root . '/pages/html/header.html');
	
	echo '<div id="content">';
	
	//check if user is logged in
	if($user->getNumber() != -1){ // IF the user is logged in
		
		//get user info
		$userInfo = $sql->getUserData($user->getNumber());
		$userID = $userInfo['userId'];
		$dbpassword = $userInfo['password'];
		$newPass1 = "";
		$newPass2 = "";
		
		//if user hasn't submitted changes yet...
		if(!isset($_POST['submit'])) {
			//display normal form
			include($root . '/pages/html/changePassword.html');	
			
		} else {//if they have submitted, then let's start to parse their info.
			//check to see what was entered...
			if(isset($_POST['newPass1']) && !empty(trim($_POST['newPass1']))) { 
				$newPass1 = $sql->db_safe(trim($_POST['newPass1']));	
			}
			
			if(isset($_POST['newPass2']) && !empty(trim($_POST['newPass2']))) { 
				$newPass2 = $sql->db_safe(trim($_POST['newPass2']));
			}
			
			if(isset($_POST['password'])) { $password = $sql->db_safe($_POST['password']);}
			
			//check that both passwords match
			if(($newPass1 === $newPass2) && ((strlen($newPass1) >= 8) && (strlen($newPass1) <= 70))){
			
				//check to make sure password is correct
				if(password_verify($password, $dbpassword)){
				
					//update password
					$sql->passwordUpdate($userID, $newPass1);
					//display success message
					echo '<div class="material"><p>' . lang::changePassword_good . '</p></div>';
				} else {//end validate password
					echo '<div class="material"><p>' . lang::changePassword_bad . '</p></div>';
				} //end else validate password
			} else { //end both passwords match
				echo '<div class="material"><p>' . lang::changePassword_issue . '</p></div>';
			}//end else both passwords match
		} 
		
	} else {//if the user isn't logged in
		die(lang::genericError);
	}
	
	
	echo '</div>';
	include($root . '/pages/html/footer.html');
?>
