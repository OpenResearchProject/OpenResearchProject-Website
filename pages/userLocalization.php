<?php
/////////////////////////////////////////////
//
//  userLocalization.php
//  A page that allows users to change their
//  localization settings.
//
////////////////////////////////////////////

$userLanguage = $user->getLanguage();
$userTimezone = $user->getTimezone();

if((isset($_POST['submit'])) && (isset($_POST['timezone'])) && (isset($_POST['language'])) && ($_POST['token'] == $user->getToken('userLocalization'))){
	$user->killToken('userLocalization');

	$success = true;
	if(strcmp($userLanguage,$_POST['language']) != 0){ $langUpdate = $user->setLanguage($_POST['language']); if(!$langUpdate){ $success = false; } }
	if(strcmp($userTimezone,$_POST['timezone']) != 0){ $tzUpdate = $user->setTimezone($_POST['timezone']); if(!$tzUpdate){ $success = false; } }
}

$languageFiles = scandir( $root . '/lang/'); // Get all of the language files
$languageOptions = '<option value="' . $userLanguage . '">' . substr($userLanguage,0,strlen($userLanguage)-4) . '</option>' . "\n";

foreach($languageFiles as $langOP){ // Go through each
     	if( strcmp(substr(strrchr($langOP,'.'),1), 'php') == 0 ){ // Check if they are php files
              	if(strcmp($langOP, $userLanguage) != 0){ // Don't re-list the file we are already using
                        $languageOptions .= '<option value="' . $langOP . '">' . substr($langOP,0,strlen($langOP)-4) . '</option>' . "\n"; // insert into new array
        	}
	}
}


/*
Thanks very much to Toland Hon from StackOverflow for the timezone code below! (I've only slightly modified it.)
https://stackoverflow.com/questions/1727077/generating-a-drop-down-list-of-timezones-with-php
*/

$timeList = DateTimeZone::listIdentifiers(DateTimeZone::ALL);

$timezone_offsets = array();

foreach( $timeList as $timezone )
{
        $tz = new DateTimeZone($timezone);
        $timezone_offsets[$timezone] = $tz->getOffset(new DateTime);
}

// sort timezone by offset
asort($timezone_offsets);

$timezone_list = array();

foreach( $timezone_offsets as $timezone => $offset )
{
        $offset_prefix = $offset < 0 ? '-' : '+';
        $offset_formatted = gmdate( 'H:i', abs($offset) );

        $pretty_offset = "UTC${offset_prefix}${offset_formatted}";

        $timezone_list[$timezone][0] = "(${pretty_offset}) $timezone";
        $timezone_list[$timezone][1] = $timezone;
}

$timezoneOptions = '<option value="' . $userTimezone . '">' . $userTimezone . '</option>' . "\n";

foreach($timezone_list as $timezone){
        $timezoneOptions .= '<option value="' . $timezone[1] . '">' . $timezone[0] . '</option>' . "\n";
}


// Its important that the token setter isn't in the submit! That way the token isn't valid for attacks.
if(!$user->checkToken('userLocalization')){
	$user->setToken('userLocalization');
}

include($root . '/pages/html/header.html');
include($root . '/pages/html/userLocalization.html');
include($root . '/pages/html/footer.html');

?>
