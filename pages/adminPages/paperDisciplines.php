<?php
/////////////////////////////////////////////
//
//  paperDisciplines.php
//  Lists all disciplines and gives admins
//  options for each.
//
////////////////////////////////////////////

if($includeCheck === 1){ // IF included from index.php
	include( $root . '/pages/adminPages/html/header.html');

	$disciplines = $sql->getCategoryList();


	if((isset($_POST['submit'])) && (isset($_POST['r'])) && (isset($_POST['d'])) && (is_numeric($_POST['r'])) && (is_numeric($_POST['d'])) && ($_POST['r'] == (int)$_POST['r']) && ($_POST['d'] == (int)$_POST['d'])){ // IF we are deleteing a discipline/category
		if($_POST['token'] == $user->getToken('paperDeleteDiscipline')){
	                $delete = (int)$_POST['d'];
			$replace = (int)$_POST['r'];
	                $isUpdated = true;

	                if(!$sql->removeDiscipline($delete,$replace)){ $isUpdated = false; }

	                if($isUpdated){ $user->killToken('paperDeleteDiscipline'); }
		}else{
			$user->logout();
			die(lang::genericError);
		}
        } // END IF we are deleteing a discipline/category

	$disciplines = $sql->getCategoryList(); // Grab the new list


	function getList($array, $refNumber = 0, $depth = 0){
		// Recursive function that turns our array into a nice list
		// Runs in O(n^2). There is probably a better way to do this.

		$list = '';
		$parent = true;
		$child = false;
		$count = count($array);

		if($count > 0){ // IF there is at least one discipline
			foreach($array as $item){
				if($item['reference'] == (int)$refNumber){

					if($parent == true){ // If we are on a parent row
						$i = 0;
		                                while($i < $depth){
		                                        $list .= "\t"; // Add tabs, depending on our depth, for a nice look in html source.
		                                        $i = $i + 1;
		                                }

						$list .= '<ul>' . "\n";
						$parent = false;
						$child = true;

						$i = 0;
	                                        while($i < $depth+1){
	                                                $list .= "\t"; // Add tabs, depending on our depth, for a nice look in html source.
	                                                $i = $i + 1;
	                                        }

						$list .= '<li><a href="./index.php?a=10&dis=' . $refNumber . '" class="add">' . lang::adminPaperDisciplines_addNew . '</a></li>' . "\n";
					}

					$i = 0;
	                               	while($i < $depth+1){
	                                 	$list .= "\t"; // Add tabs, depending on our depth, for a nice look in html source.
	                                      	$i = $i + 1;
	                                }

					if($count > 1){
						$list .= '<li><a href="./index.php?p=5&dis=' . $item['id'] . '" target="_blank">' . $item['name'] . '</a>&nbsp;|&nbsp;<a class="delete" href="./index.php?a=11&d=' . $item['id'] . '">' . lang::adminPaperDisciplines_delete . '</a></li>' . "\n";
					}else{
						$list .= '<li><a href="./index.php?p=5&dis=' . $item['id'] . '" target="_blank">' . $item['name'] . '</a></li>' . "\n";
					}

					$i = 0;
	                                while($i < $depth+2){
	                                	$list .= "\t"; // Add tabs, depending on our depth, for a nice look in html source.
	                                      	$i = $i + 1;
	                                }

					$newList = getList($array, $item['id'], $depth + 1);

					if(strlen($newList) > 1){
						$list .= $newList . "\n";
					}else{
						$list .= '<ul><li><a href="./index.php?a=10&dis=' . $item['id'] . '" class="add">' . lang::adminPaperDisciplines_addNew . '</a></li></ul>' . "\n";
					}
				}
			}

			if($child == true){
				$i = 0;
	                       	while($i < $depth){
	                           	$list .= "\t"; // Add tabs, depending on our depth, for a nice look in html source.
	                              	$i = $i + 1;
	                       	}
				$list .= '</ul>' . "\n";
			}

		}else{ // ELSE IF there is at least one discipline
			$list .= '<li><a href="./index.php?a=10&dis=0" class="add">' . lang::adminPaperDisciplines_addNew . '</a></li>';
		} // END ELSE IF there is at least one discipline

		return $list;
	}


	include( $root . '/pages/adminPages/html/paperDisciplines.html');
	include( $root . '/pages/adminPages/html/footer.html');
}else{ // ELSE IF included from index.php
	die(lang::genericError);
} // END ELSE IF included from index.php

?>
