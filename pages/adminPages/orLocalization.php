<?php
/////////////////////////////////////////////
//
//  orLocalization.php
//  Contains localization settings for admins.
//
////////////////////////////////////////////

if($includeCheck === 1){ // IF included from index.php
	include( $root . '/pages/adminPages/html/header.html');


	if((isset($_POST['language'])) && (isset($_POST['timezone'])) && ($_POST['token'] == $user->getToken('orLocalization'))){ // IF form submitted

		$language['text'] = $_POST['language'];
		$timezone['text'] = $_POST['timezone'];

		$isUpdated = true;

		// Get the current settings:
		$oldLanguage = $sql->getSetting('language');
                $oldTimezone = $sql->getSetting('timezone');

		// Update Database

		if(strcmp($oldLanguage['text'], $language['text']) != 0){ // IF the setting has been changed
			if(!$sql->updateSetting('language', $language['text'], null)){ $isUpdated = false; }
		}
		if(strcmp($oldTimezone['text'], $timezone['text']) != 0){ // IF the setting has been changed
			if(!$sql->updateSetting('timezone', $timezone['text'], null)){ $isUpdated = false; }
		}

		if($isUpdated){
			$user->killToken('orLocalization');
		}

	}else{ // ELSE IF form submitted
		// Query data from database
		$language = $sql->getSetting('language');
		$timezone = $sql->getSetting('timezone');
	} // END ELSE IF form submitted


	$languageFiles = scandir( $root . '/lang/'); // Get all of the language files
        $languageOptions = [];
        $i = 1;

        foreach($languageFiles as $langOP){ // Go through each
              	if( strcmp(substr(strrchr($langOP,'.'),1), 'php') == 0 ){ // Check if they are php files
			if(strcmp($langOP, $language['text']) != 0){ // Don't re-list the file we are already using
                 		$languageOptions[$i] = '<option value="' . $langOP . '">' . $langOP . '</option>' . "\n"; // insert into new array
                      		$i = $i + 1;
			}
         	}
       	}

	/*
		Thanks very much to Toland Hon from StackOverflow for the timezone code below! (I've only slightly modified it.)
		https://stackoverflow.com/questions/1727077/generating-a-drop-down-list-of-timezones-with-php
	*/

	$timeList = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
	$timezone_offsets = array();

	foreach( $timeList as $timezonez ){
	        $tz = new DateTimeZone($timezonez);
	        $timezone_offsets[$timezonez] = $tz->getOffset(new DateTime);
	}

	// sort timezone by offset
	asort($timezone_offsets);
	$timezone_list = array();

	foreach( $timezone_offsets as $timezonez => $offset ){
	        $offset_prefix = $offset < 0 ? '-' : '+';
	        $offset_formatted = gmdate( 'H:i', abs($offset) );

	        $pretty_offset = "UTC${offset_prefix}${offset_formatted}";

	        $timezone_list[$timezonez][0] = "(${pretty_offset}) $timezonez";
	        $timezone_list[$timezonez][1] = $timezonez;
	}

	$timeOptions = [];
	$i = 1;

	foreach($timezone_list as $timezonez){
	        $timeOptions[$i] = '<option value="' . $timezonez[1] . '">' . $timezonez[0] . '</option>' . "\n";
		$i = $i + 1;
	}

	// Its important that the token setter isn't in the submit! That way the token isn't valid for attacks.
       	if(!$user->checkToken('orLocalization')){
      	    $user->setToken('orLocalization');
       	}


	include( $root . '/pages/adminPages/html/orLocalization.html');
	include( $root . '/pages/adminPages/html/footer.html');
}else{ // ELSE IF included from index.php
	die(lang::genericError);
} // END ELSE IF included from index.php

?>
