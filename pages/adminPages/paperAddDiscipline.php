<?php
/////////////////////////////////////////////
//
//  paperAddDisciplines.php
//  Mid-page that allows admins to add disciplines.
//
////////////////////////////////////////////

if($includeCheck === 1){ // IF included from index.php
	include( $root . '/pages/adminPages/html/header.html');

	if((isset($_POST['disciplineName'])) && (isset($_POST['parent'])) && ($_POST['token'] == $user->getToken('paperAddDiscipline'))){ // IF form submitted

		$disciplineName = $_POST['disciplineName'];
		$parentId = $_POST['parent'];

		$isUpdated = true;
		$Errors = [];

		// Check if name already exists:
		$disciplines = $sql->getCategoryList();

		if(strlen($disciplineName)<2){ // IF the name is too short
			$isUpdated = false;
			$Errors[] = lang::admin_disciplineTooShort;
		} // END IF the name is too short

		if(count($disciplines) > 0){ // IF there are any disciplines
			foreach($disciplines as $item){ // IF discipline already exists
	                      	if(strcmp($item['name'], $disciplineName) == 0){
	                           	$isUpdated = false;
					$Errors[] = lang::admin_disciplineExists;
	                     	}
	               	} // END IF discipline already exists
		} // END IF there are any disciplines

		if($isUpdated == true){ // IF there are no errors
			// Update Database
			if(!$sql->addDiscipline($disciplineName, $parentId)){ $isUpdated = false; }else{ $user->killToken('paperAddDiscipline'); }
		} // END IF there are no errors

	} // END IF form submitted

	if((isset($_GET['dis'])) && (is_numeric($_GET['dis'])) && ($_GET['dis']==(int)$_GET['dis'])){

		$parentId = (int)$_GET['dis'];

		if($parentId != 0){ // IF we are not adding to the top discipline
			$disciplines = $sql->getCategoryList();

			foreach($disciplines as $item){
				if($item['id'] == $parentId){ // find the parent discipline's name
					$parentDiscipline = $item['name'];
				}
			}
		} // END IF we are not adding to the top discipline

		// Its important that the token setter isn't in the submit! That way the token isn't valid for attacks.
		if(!$user->checkToken('paperAddDiscipline')){
			$user->setToken('paperAddDiscipline');
		}

		include( $root . '/pages/adminPages/html/paperAddDiscipline.html');
	}else{
		$user->logout();
		die( lang::genericError);
	}
	include( $root . '/pages/adminPages/html/footer.html');
}else{ // ELSE IF included from index.php
	die(lang::genericError);
} // END ELSE IF included from index.php

?>
