<?php
/////////////////////////////////////////////
//
//  paperReviewSettings.php
//  Contains admin settings specific to
//  the review process for papers.
//
////////////////////////////////////////////

if($includeCheck === 1){ // IF included from index.php
	include( $root . '/pages/adminPages/html/header.html');


	if((isset($_POST['paperReviews'])) && (isset($_POST['paperApprovals'])) && (isset($_POST['reviewTime'])) && (isset($_POST['reviewSkipCount'])) && (isset($_POST['reviewSkipLockTime'])) && ($_POST['token'] == $user->getToken('paperReviewSettings'))){ // IF form submitted

		if((!is_numeric($_POST['paperApprovals'])) || ($_POST['paperApprovals'] != (int)$_POST['paperApprovals'])){
			die(lang::genericError);
		}else{
			$paperApprovals['num'] = $_POST['paperApprovals'];
		}

		if((!is_numeric($_POST['paperReviews'])) || ($_POST['paperReviews'] != (int)$_POST['paperReviews'])){
                        die(lang::genericError);
                }else{
                        $paperReviews['num'] = $_POST['paperReviews'];
                }

		if((!is_numeric($_POST['reviewTime'])) || ($_POST['reviewTime'] != (int)$_POST['reviewTime'])){
                        die(lang::genericError);
                }else{
                        $reviewTime['num'] = $_POST['reviewTime'];
                }

		if((!is_numeric($_POST['reviewSkipCount'])) || ($_POST['reviewSkipCount'] != (int)$_POST['reviewSkipCount'])){
                        die(lang::genericError);
                }else{
                        $reviewSkipCount['num'] = $_POST['reviewSkipCount'];
                }

		if((!is_numeric($_POST['reviewSkipLockTime'])) || ($_POST['reviewSkipLockTime'] != (int)$_POST['reviewSkipLockTime'])){
                        die(lang::genericError);
                }else{
                        $reviewSkipLockTime['num'] = $_POST['reviewSkipLockTime'];
                }

		if(isset($_POST['enableCSPRNG'])){
			$enableCSPRNG['num'] = 1;
		}else{
			$enableCSPRNG['num'] = 0;
		}

		$isUpdated = true;

		$oldPaperApprovals = $sql->getSetting('paperApprovals');
                $oldPaperReviews = $sql->getSetting('paperReviews');
		$oldReviewTime = $sql->getSetting('reviewTime');
		$oldReviewSkipCount = $sql->getSetting('reviewSkipCount');
		$oldReviewSkipLockTime = $sql->getSetting('reviewSkipLockTime');
		$oldEnableCSPRNG = $sql->getSetting('enableCSPRNG');

		// Update Database
		if($oldPaperApprovals['num'] != $paperApprovals['num']){
			if(!$sql->updateSetting('paperApprovals', null, $paperApprovals['num'])){ $isUpdated = false; }
		}
		if($oldPaperReviews['num'] != $paperReviews['num']){
			if(!$sql->updateSetting('paperReviews', null, $paperReviews['num'])){ $isUpdated = false; }
		}
		if($oldReviewTime['num'] != $reviewTime['num']){
                        if(!$sql->updateSetting('reviewTime', null, $reviewTime['num'])){ $isUpdated = false; }
                }
		if($oldReviewSkipCount['num'] != $reviewSkipCount['num']){
                        if(!$sql->updateSetting('reviewSkipCount', null, $reviewSkipCount['num'])){ $isUpdated = false; }
                }
		if($oldReviewSkipLockTime['num'] != $reviewSkipLockTime['num']){
                        if(!$sql->updateSetting('reviewSkipLockTime', null, $reviewSkipLockTime['num'])){ $isUpdated = false; }
                }
		if($oldEnableCSPRNG['num'] != $enableCSPRNG['num']){
                        if(!$sql->updateSetting('enableCSPRNG', null, $enableCSPRNG['num'])){ $isUpdated = false; }
                }

		if($isUpdated){
			$user->killToken('paperReviewSettings');
		}

	}else{ // ELSE IF form submitted
		// Query data from database
		$paperApprovals = $sql->getSetting('paperApprovals');
		$paperReviews = $sql->getSetting('paperReviews');
		$reviewTime = $sql->getSetting('reviewTime');
		$reviewSkipCount = $sql->getSetting('reviewSkipCount');
		$reviewSkipLockTime = $sql->getSetting('reviewSkipLockTime');
		$enableCSPRNG = $sql->getSetting('enableCSPRNG');
	} // END ELSE IF form submitted

	// Its important that the token setter isn't in the submit! That way the token isn't valid for attacks.
	if(!$user->checkToken('paperReviewSettings')){
		$user->setToken('paperReviewSettings');
	}

	include( $root . '/pages/adminPages/html/paperReviewSettings.html');
	include( $root . '/pages/adminPages/html/footer.html');
}else{ // ELSE IF included from index.php
	die(lang::genericError);
} // END ELSE IF included from index.php

?>
