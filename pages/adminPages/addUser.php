<?php
///////////////////////////////
//
//  addUser.php
//  Admin's add new users using
//  this page.
//
//////////////////////////////

if(isset($_POST['submit'])){

	if($_POST['token'] != $user->getToken('adminAddUser')){ $user->logout(); die(lang::genericError); }

			$registerErrors = [];
			$userFullName = $sql->db_safe($_POST['userFullName']);
                        $userName = $sql->db_safe($_POST['userName']);
                        $password = $sql->db_safe($_POST['password']);
                        $passwordVerify = $sql->db_safe($_POST['passwordVerify']);
                        $email = $sql->db_safe($_POST['emailAddress']);
			if(isset($_POST['isAdmin'])){ $isAdmin = 1; }else{ $isAdmin = 0; }

                        if((strlen($userName)<3) || (strlen($userName)>25)){ array_push($registerErrors, lang::register_userNameLength); }
                        if((strlen($password)<8) || (strlen($password)>70)){ array_push($registerErrors, lang::register_passwordLength); }
                        if(strcmp($password, $passwordVerify) !== 0){ array_push($registerErrors, lang::register_passwordMatch); }
                        if(strlen($email)<3){ array_push($registerErrors, lang::register_emailValid); }
                        if(strlen($userFullName)<4){ array_push($registerErrors, lang::register_fullNameLength); }

                        // Check if username or email is already in use:
                        if($sql->getUserByUsername($userName)>=0){ array_push($registerErrors, lang::register_userNameInUse); }
                        if($sql->getUserByEmail($email)>=0){ array_push($registerErrors, lang::register_emailInUse); }


                        if(count($registerErrors)>0){ // IF there is an error
                                unset($password); unset($passwordVerify);
                                include($root . '/pages/adminPages/html/header.html');
                                include($root . '/pages/adminPages/html/addUser.html');
                                include($root . '/pages/adminPages/html/footer.html');
                        }else{ // ELSE IF there is an error
                                $user->killToken('adminAddUser');
                                date_default_timezone_set('UTC'); // UTC is the database's default timezone. Convert it on echo/print.
                                $joinDate = date("Y-m-d H:i:s");
                                $uid = hash('sha256', mt_rand(0,100000) . rand(1,1000) . $joinDate . $userName);

				$userNumber = $sql->addUser($userName, $password, $joinDate, $email, $uid, null, $isAdmin, $userFullName);

                                unset($password); unset($passwordVerify); unset($uid);


                                if($userNumber > -1){ // IF registration worked, ie. returned > -1, the user's number

                                        include($root . '/pages/adminPages/html/header.html');
                                        $message->showMessage(33); // User Registered Successfully
                                        include($root . '/pages/adminPages/html/footer.html');

                                }else{ // ELSE IF registration worked, ie. returned > -1, the user's number
                                        die(lang::genericError);
                                } // END ELSE IF registration worked, ie. returned > -1, the user's number

                        } //END ELSE IF there is an error
}else{

	// Its important that the token setter isn't in the submit! That way the token isn't valid for attacks.
	if(!$user->checkToken('adminAddUser')){
		$user->setToken('adminAddUser');
	}

  	include($root . '/pages/adminPages/html/header.html');
      	include($root . '/pages/adminPages/html/addUser.html');
    	include($root . '/pages/adminPages/html/footer.html');

}

?>
