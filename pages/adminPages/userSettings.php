<?php
/////////////////////////////////////////////
//
//  userSettings.php
//  Contains general user settings for admins.
//
////////////////////////////////////////////

if($includeCheck === 1){ // IF included from index.php
	include( $root . '/pages/adminPages/html/header.html');


	if((isset($_POST['loginLockTime'])) && (isset($_POST['registrationType'])) && (isset($_POST['loginAttempts'])) && ($_POST['token'] == $user->getToken('userSettings'))){ // IF form submitted

		if((!is_numeric($_POST['registrationType'])) || ($_POST['registrationType'] != (int)$_POST['registrationType'])){
			die(lang::genericError);
			// Something fishy is going on...
		}else{
			$registrationType['num'] = (int)$_POST['registrationType'];
		}

		if((!is_numeric($_POST['loginAttempts'])) || ($_POST['loginAttempts'] != (int)$_POST['loginAttempts'])){
                        die(lang::genericError);
                        // Something fishy is going on...
                }else{
                        $loginAttempts['num'] = (int)$_POST['loginAttempts'];
                }

		if((!is_numeric($_POST['loginLockTime'])) || ($_POST['loginLockTime'] != (int)$_POST['loginLockTime'])){
                        die(lang::genericError);
                        // Something fishy is going on...
                }else{
                        $loginLockTime['num'] = (int)$_POST['loginLockTime'];
                }

		$isUpdated = true;

		// Get current settings:
		$oldRegistrationType = $sql->getSetting('registration');
                $oldLoginAttempts = $sql->getSetting('loginAttempts');
                $oldLoginLockTime = $sql->getSetting('loginLockTime');

		// Update Database
		if($oldRegistrationType['num'] != $registrationType['num']){
			if(!$sql->updateSetting('registration', null, $registrationType['num'])){ $isUpdated = false; }
		}
		if($oldLoginAttempts['num'] != $loginAttempts['num']){
			if(!$sql->updateSetting('loginAttempts', null, $loginAttempts['num'])){ $isUpdated = false; }
		}
		if($oldLoginLockTime['num'] != $loginLockTime['num']){
			if(!$sql->updateSetting('loginLockTime', null, $loginLockTime['num'])){ $isUpdated = false; }
		}

		if($isUpdated){
			$user->killToken('userSettings');
		}

	}else{ // ELSE IF form submitted
		// Query data from database
		$registrationType = $sql->getSetting('registration');
		$loginAttempts = $sql->getSetting('loginAttempts');
		$loginLockTime = $sql->getSetting('loginLockTime');
	} // END ELSE IF form submitted


	// Its important that the token setter isn't in the submit! That way the token isn't valid for attacks.
	if(!$user->checkToken('userSettings')){
		$user->setToken('userSettings');
	}

	include( $root . '/pages/adminPages/html/userSettings.html');
	include( $root . '/pages/adminPages/html/footer.html');
}else{ // ELSE IF included from index.php
	die(lang::genericError);
} // END ELSE IF included from index.php

?>
