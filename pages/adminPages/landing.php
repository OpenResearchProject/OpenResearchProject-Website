<?php
/////////////////////////////////////////////
//
//  landing.php
//  The admin landing page that has links to
//  different admin settings and features.
//
////////////////////////////////////////////

if($includeCheck === 1){ // IF included from index.php
	include( $root . '/pages/adminPages/html/header.html');
	include( $root . '/pages/adminPages/html/landing.html');
	include( $root . '/pages/adminPages/html/footer.html');
}else{ // ELSE IF included from index.php
	die(lang::genericError);
} // END ELSE IF included from index.php

?>
