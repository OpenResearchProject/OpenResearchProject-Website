<?php
/////////////////////////////////////////////
//
//  reauth.php
//  Recheck's an admin's login
//
////////////////////////////////////////////

if($includeCheck === 1){ // IF included from index.php


	if((isset($_POST['submit'])) && (isset($_POST['password']))){ // IF the form has been submitted
		$password = $sql->db_safe($_POST['password']);
		unset($_POST['password']);

		if($_POST['token'] == $user->getToken('adminReauth')){ // IF token is good
			$reauth = $user->reauth($password); // like login: ['login'] = true/false on good/bad login; ['status'] = user id if good, -1 if bad, -2 on max attempts
			if($reauth['login']){ // IF login is good
				$user->setReauth('adminPanel');
				$user->setAdminSessionTime();
				unset($password);

				include( $root . '/pages/adminPages/html/header.html');
				$message->showMessage(30);
				include( $root . '/pages/adminPages/html/footer.html');
			}else{ // ELSE IF login is good

				unset($password);

				if($reauth['status'] == -1){
					$loginErrors[] = lang::admin_loginBad;
					include($root . '/pages/adminPages/html/header.html');
					include($root . '/pages/adminPages/html/reauth.html');
					include($root . '/pages/adminPages/html/footer.html');
				}

				if($reauth['status'] == -2){
					$user->logout();
					include($root . '/pages/adminPages/html/header.html');
                                        $message->showMessage(32);
                                        include($root . '/pages/adminPages/html/footer.html');
				}

			} // END ELSE IF login is good
		}else{ // ELSE IF token is good
			unset($password); unset($_POST['password']);
			$user->logout();
			die(lang::genericError);
		} // END IF token is good
	}else{ // ELSE IF the form has been submitted

		if(!$user->checkToken('adminReauth')){
            		$user->setToken('adminReauth');
                }

		include($root . '/pages/adminPages/html/header.html');
		include($root . '/pages/adminPages/html/reauth.html');
		include($root . '/pages/adminPages/html/footer.html');
	} // END ELSE IF the form has been submitted

}else{ // ELSE IF included from index.php
        die(lang::genericError);
} // END ELSE IF included from index.php
?>
