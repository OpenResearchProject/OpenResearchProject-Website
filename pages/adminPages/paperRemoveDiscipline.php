<?php
/////////////////////////////////////////////
//
//  paperRemoveDiscipline.php
//  Mid-page that deletes disciplines for admins.
//
////////////////////////////////////////////

if($includeCheck === 1){ // IF included from index.php
        include( $root . '/pages/adminPages/html/header.html');

	// The delete code is handled by the paperDiscipline.php page

	if((isset($_GET['d'])) && (is_numeric($_GET['d'])) && ($_GET['d'] == (int)$_GET['d'])){

		$delete = (int)$_GET['d'];
		$disciplines = $sql->getCategoryList();

		foreach($disciplines as $item){
			if($item['id'] == $delete){
				$deleteName = $item['name'];
			}
		}

		function returnRefTree($array, $startId, $currentId){
			// Function that returns the id's of every discipline below our starting id number

		        $banArray = [];
		        $banArray[] = $currentId;

		        foreach($array as $item){
		                if($item['reference'] == $currentId){
		                        $banArray[] = returnRefTree($array, $startId, $item['id']);
		                }
		        }

			if($currentId == $startId){
				return $banArray;
			}else{
				return $currentId;
			}

		} // END function

		// Get our banned id's:
		$banArray = returnRefTree($disciplines, $delete, $delete);
		$disciplineReplacements = [];

		// Make a new array without our banned id's:
		foreach($disciplines as $item){
			$keep = true; // flag to keep item

			foreach($banArray as $ban){
				if(($keep == true) && ($item['id'] == $ban)){
					$keep = false;
				}
			}

			if($keep == true){
				$disciplineReplacements[] = $item;
			}
		}

		// Its important that the token setter isn't in the submit! That way the token isn't valid for attacks.
                if(!$user->checkToken('paperDeleteDiscipline')){
                        $user->setToken('paperDeleteDiscipline');
                }

		include( $root . '/pages/adminPages/html/paperRemoveDiscipline.html');
	}else{
		$user->logout();
		die(lang::genericError);
	}
	include( $root . '/pages/adminPages/html/footer.html');

}else{ // ELSE IF included from index.php
        die(lang::genericError);
} // END ELSE IF included from index.php

?>
