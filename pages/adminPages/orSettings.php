<?php
/////////////////////////////////////////////
//
//  orSettings.php
//  Contains admin settings specific to
//  main OpenResearch items.
//
////////////////////////////////////////////

if($includeCheck === 1){ // IF included from index.php
	include( $root . '/pages/adminPages/html/header.html');


	if((isset($_POST['supportEmail'])) && ($_POST['token'] == $user->getToken('orSettings'))){ // IF form submitted

		$supportEmail['text'] = $_POST['supportEmail'];
		$isUpdated = true;

		// Get the current setting:
		$oldSupportEmail = $sql->getSetting('supportEmail');

		// Update Database
		if(strcmp($oldSupportEmail['text'], $supportEmail['text']) != 0){ // IF the setting has been changed
			if(!$sql->updateSetting('supportEmail', $supportEmail['text'], null)){ $isUpdated = false; }
		}

		if($isUpdated){
			$user->killToken('orSettings');
		}

	}else{ // ELSE IF form submitted
		// Query data from database
		$supportEmail = $sql->getSetting('supportEmail');
	} // END ELSE IF form submitted


	// Its important that the token setter isn't in the submit! That way the token isn't valid for attacks.
	if(!$user->checkToken('orSettings')){
		$user->setToken('orSettings');
	}

	include( $root . '/pages/adminPages/html/orSettings.html');
	include( $root . '/pages/adminPages/html/footer.html');
}else{ // ELSE IF included from index.php
	die(lang::genericError);
} // END ELSE IF included from index.php

?>
