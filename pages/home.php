<?php
/////////////////////////////////////////////
//
//  home.php
//  The first page of OpenResearch. Loaded
//  by index.php in root.
//
////////////////////////////////////////////

include($root . '/pages/html/header.html');

// Its important that the token setter isn't in the submit! That way the token isn't valid for attacks.
if(!$user->checkToken('search')){
	$user->setToken('search');
}

include($root . '/pages/html/home.html');
include($root . '/pages/html/footer.html');

?>
