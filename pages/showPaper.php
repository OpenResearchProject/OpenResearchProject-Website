<?php
/////////////////////////////////////////////
//
//  showPaper.php
//  This page displays a paper in text form
//  with accompanying metadata about the
//  paper.
//
////////////////////////////////////////////

//include paperUtil file which contains paper display functions
include($root . '/pages/paperUtil.php');


include($root . '/pages/html/header.html');
$currentUser = $user->getNumber();

echo "<div id=\"content\">";

	if (is_numeric($_GET['paperId']))
	{
		$paperId = (int)$_GET['paperId'];
		if ((isset($paperId)) && ($paperId > 0))
		{
			$paperInfo = $sql->getPaperInfo($paperId);//still need to check for empty

			if(isset($paperInfo[0])){
				$authors = parseAuthors($paperInfo);
				displayFullPaper($paperInfo[0], $authors[$paperInfo[0]['paperID']], $user);

			}else //paper doesn't exist
		{
			echo '<h1>' . lang::showPaper_lostPaper . '</h1>';
		}
		}else //error loading page
		{
			die(lang::genericError);
		}
	}

include($root . '/pages/html/footer.html');
?>
