<?php
/////////////////////////////////////////////
//
//  searchLanding.php
//  This page displays search results for papers.
//
////////////////////////////////////////////

include($root . '/pages/html/header.html');

if((isset($_POST['search'])) && ($_POST['token'] == $user->getToken('search'))){ // IF the search was submitted

	$user->killToken('search');
	$results = $sql->fullTextSearch($_POST['search']);

	if(is_null($results)){ // IF there are no results
		$results = [];
		$results[0]['paperId'] = -1;
		$results[0]['paperName'] = 'No results found.';
	} // END IF there are no results

	$searchResults = '';
     	$timezone = new DateTimeZone($user->getTimezone()); // Set the timezone

	foreach($results as $item){
		if($item['paperId'] >= 0){

			$datetime = new DateTime($item['paperPublishDate']); // Get publish date as datetime object
			$datetime->setTimezone($timezone); // Convert the timezone

			switch($item['isEndorsed']){
 		 		case 1: $endorsed = lang::paperUtil_isEndorsed1; break;
				case 2: $endorsed = lang::paperUtil_isEndorsed2; break;
		       		case 3: $endorsed = lang::paperUtil_isEndorsed3; break;
				default: $endorsed = paperUtil_isEndorsedError; break;
			}

			$searchResults .= '<div class="paperInfo"><h3>' . $item['paperName'] . '</h3><div class="paperButtons">
						<h4 class="pYear">' . lang::paperUtil_published . $datetime->format('Y-m-d') . '</h4>' . $endorsed . '<ul>
						<li><a href="./index.php?p=11&paperId=' . $item['paperId'] . '">' . lang::paperUtil_fullPaper . '</a></li>
						<li><a href="./index.php?p=9&paper=' . $item['paperId'] . '">' . lang::paperUtil_feedback . '</a></li>
						</ul></div><!--End PaperButtons--></div>';
		}else{
			$searchResults .= '<div class="paperInfo">' . $item['paperName'] . '</div>';
		}
	}

} // END IF the search was submitted

// Its important that the token setter isn't in the submit! That way the token isn't valid for attacks.
if(!$user->checkToken('search')){
	$user->setToken('search');
}

include($root . '/pages/html/searchLanding.html');
include($root . '/pages/html/footer.html');

?>
