<?php
/////////////////////////////////////////////////////////////////
//
// mySQLiClass.php
// Sets up the mySQLi link and handles some queries to the database.
//
/////////////////////////////////////////////////////////////////

// In the interest of time, not all of these have been updated in object oriented format!

if(isset($includeCheck)){
// IF included from another source

class sqlOR{

	private $dbLink, $hashOptions;
	protected $root;

	public function __construct($configPath, $root){
		$includeCheck = 1;
		$this->root = $root;

		// Included here so following scripts can't get the $dbPassword
		require_once( $configPath . 'dbConfig.php');
		unset($configPath);
		$this->hashOptions = $hashOptions;
		unset($hashOptions);
		$this->dbLink = new mysqli($dbSource, $dbUsername, $dbPassword, $dbName);
		unset($dbSource);unset($dbUsername);unset($dbPassword);unset($dbName);

		// force mysql charset to prevent charset dodging attack
		if (!$this->dbLink->set_charset("utf8")) {
			die(lang::genericError);
		}

		return $this->dbLink;
	}

	public function db_safe($text){
		$text=strip_tags($text);
		$text=escapeshellcmd($text);
		if(get_magic_quotes_gpc()) {$text = stripslashes($text);}
		$text=trim($text);
		return mysqli_escape_string($this->dbLink, $text);
	}

	public function displayable_db_safe($text){
		if(get_magic_quotes_gpc()) {$text = stripslashes($text);}
		$text=trim($text);
		$size=strlen($text);

		for($i=0;$i<=$size-1;$i++) // Break down the text character by character
		{
		        if((preg_match('/[^0-9A-Za-z ]/',$text{$i})) && (ord($text{$i})>=32) && (ord($text{$i})<=126)){
			        $newtext.="&#" . ord($text{$i}) . ";"; // Symbols of any kind are changed into html special characters to prevent bad code from running on user's side
		        }else{
		                if((ord($text{$i})==10) || (ord($text{$i})==9) || (ord($text{$i})==13) || (ord($text{$i})==11) || (ord($text{$i})==12))
		                {
			                $newtext.="&#" . ord($text{$i}) . ";";
		                }else{
			                $newtext.=$text{$i}; // Only print if it is a normal letter or number
		                }
		        }
		}

		return "'" . mysqli_escape_string($dbLink, $newtext) . "'";
	}

	//xss mitigation functions; its light, but its something
	public function xssafe($data,$encoding='UTF-8')
	{
		return htmlspecialchars($data, ENT_QUOTES | ENT_HTML401, $encoding);
	}

	public function getSetting($name){
		$query = 'SELECT numValue,textValue FROM Settings WHERE name=?';
                $stmt = mysqli_stmt_init($this->dbLink);

                if(mysqli_stmt_prepare($stmt, $query)){
                        mysqli_stmt_bind_param($stmt, "s", $name);
                        mysqli_stmt_execute($stmt);

                        mysqli_stmt_store_result($stmt);
                        mysqli_stmt_bind_result($stmt, $setting['num'], $setting['text']);
                        mysqli_stmt_fetch($stmt);
                        mysqli_stmt_close($stmt);
                        unset($query); unset($stmt);
                }else{ // IF stmt prepare fails
                        die(lang::genericError);
                }//if stmt prepare

                return $setting;
	}

	public function updateSetting($name, $textValue, $numValue){

		if((!is_numeric($numValue)) || ($numValue != (int)$numValue)){
			if(!is_null($numValue)){
	                	return false;
			}
            	}

		$stmt = $this->dbLink->prepare('UPDATE Settings SET textValue=?, numValue=? WHERE name=?');
		$stmt->bind_param('sis', $textValue, $numValue, $name);

              	if(!$stmt->execute()){
			return false;
		}

		if($stmt->affected_rows > 0){
			$stmt->close();
			unset($stmt);
			return true;
		}else{
	                $stmt->close();
	              	unset($stmt);
			return false;
		}
	}

	public function setUserTimezone($userNumber, $timezone){

		if((is_numeric($userNumber)) && ($userNumber != (int)$userNumber)){ die(lang::genericError); }
		$timeList = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
		if(!in_array($timezone, $timeList)){ die(lang::genericError); }

		if($stmt = $this->dbLink->prepare('UPDATE Users SET timezone=? WHERE userId=?')){
                        $stmt->bind_param('si', $timezone, $userNumber);
                        if(!$stmt->execute()){
                                $stmt->close();
                                unset($stmt);
                                return false;
                        }else{
                                if($this->dbLink->affected_rows > 0){
					$stmt->close();
	                                unset($stmt);
                                        return true;
                                }else{
					$stmt->close();
	                                unset($stmt);
                                        return false;
                                }
                        }
                }else{
                        return false;
                }
        }

	public function setUserLanguage($userNumber, $language){

                if((is_numeric($userNumber)) && ($userNumber != (int)$userNumber)){ die(lang::genericError); }

                if($stmt = $this->dbLink->prepare('UPDATE Users SET language=? WHERE userId=?')){
                        $stmt->bind_param('si', $language, $userNumber);
                        if(!$stmt->execute()){
                                $stmt->close();
                                unset($stmt);
                                return false;
                        }else{
                                if($this->dbLink->affected_rows > 0){
					$stmt->close();
	                                unset($stmt);
                                        return true;
                                }else{
					$stmt->close();
	                                unset($stmt);
                                        return false;
                                }
                        }
                }else{
                        return false;
                }
        }

	public function getUserData($userId){

		if($userId != (int)$userId){ die(lang::genericError); }

		if( $stmt = $this->dbLink->prepare('SELECT userId,userName,password,joinDate,degree,title,university,timezone,language,lastLogin,emailAddress,avatar,uniqueID,activationCode,admin,userFullName,skipCount,skipTime FROM Users WHERE userId=? LIMIT 1')){
			$stmt->bind_param('i', $userId);

			if ($stmt->execute()){
				$stmt->store_result();
				$stmt->bind_result($result_userId,$result_username,$result_password,$result_joinDate,$result_degree,$result_title,$result_university,$result_timezone,$result_language,$result_lastLogin,
							$result_emailAddress,$result_avatar,$result_uniqueID,$result_activationCode,$result_admin,$result_userFullName,$result_skipCount,$result_skipTime);

				while ($stmt->fetch()){
					$userData = array('userId' => $result_userId, 'userName' => $result_username, 'password'=> $result_password, 'joinDate' => $result_joinDate, 'degree' => $result_degree,
					'title' => $result_title, 'university' => $result_university, 'timezone' => $result_timezone, 'language' => $result_language, 'lastLogin' => $result_lastLogin, 'emailAddress' => $result_emailAddress,'avatar' => $result_avatar,
					 'uniqueID' => $result_uniqueID, 'activationCode' => $result_activationCode, 'admin' => $result_admin, 'userFullName' => $result_userFullName,
					 'skipCount' => $result_skipCount, 'skipTime' => $result_skipTime);
				}
			}else{
			    die(lang::genericError);
			}
		}else{
			die(lang::genericError);
		}

		$stmt->close();
		unset($stmt);

		if((!isset($userData)) || (is_null($userData))){
			return null;
		}else{
			return $userData;
		}
	}

	public function login($userName, $password, $ipAddress){
		// Returns user's id number if good, -1 if incorrect login, and -2 if blocked for too many login attempts

		/*
		* Yes this is a long section of code, but it is needed for security. First we get our user information and then
		* we check if the user can even attempt to login again. If so, do it. If not, check for how long the user is blocked.
		* If they were blocked longer than the admin's login-lock-time, then unblock them and attempt to login again.
		* If they weren't, block them and return -2.
		*/

		$dbNumber = -1;

		if($stmt = $this->dbLink->prepare('SELECT userId,password FROM Users WHERE userName=?')){ // IF got user information
			$stmt->bind_param('s', $userName);
			if(!$stmt->execute()){
	                    $stmt->close();
	                    unset($stmt);unset($password);
			    die(lang::genericError);
			}
			$stmt->store_result();
			$stmt->bind_result($dbNumber, $dbPassword);
			$stmt->fetch();
			$stmt->close();
			unset($stmt);


			if($stmt = $this->dbLink->prepare('SELECT loginId, lastLoginAttempt, loginAttempts FROM Logins WHERE ipAddress=?')){ // IF got login/ip information
	                        $stmt->bind_param('s', $ipAddress);
	                        if(!$stmt->execute()){
	                            $stmt->close();
	                            unset($stmt);unset($password);
	                            die(lang::genericError);
	                        }
	                        $stmt->store_result();
	                        $stmt->bind_result($loginId, $lastLoginAttempt, $loginAttempts);
	                        $stmt->fetch();
	                        $stmt->close();
	                        unset($stmt);

				if((!isset($loginId)) || (!is_numeric($loginId)) || ($loginId != (int)$loginId)){ // IF the ip isn't already in the Logins database
					$lastLoginAttempt = time();
					$loginAttempts = 0;

					// Set defaults and insert it into the database
					if($stmt = $this->dbLink->prepare('INSERT INTO Logins(ipAddress, lastLoginAttempt, loginAttempts, userId) VALUES (?,?,?,?)')){
	                                        $stmt->bind_param("siii", $ipAddress, $lastLoginAttempt, $loginAttempts, $dbNumber);
	                                        $stmt->execute();
                                        	$stmt->close();
					}
				} // END IF the ip isn't already in the logins database

			$maxAttempts = $this->getSetting('loginAttempts');

			if($loginAttempts < $maxAttempts['num']){ // IF there are still more login attempts
				if(password_verify($password, $dbPassword)){

						// Reset attempts to 0
						$stmt = $this->dbLink->prepare('UPDATE Logins SET lastLoginAttempt=?, loginAttempts=?, userId=? WHERE ipAddress=?');
                                                $lastLoginAttempt = time();
                                                $loginAttempts = 0;
                                                $stmt->bind_param('iiis', $lastLoginAttempt, $loginAttempts, $dbNumber, $ipAddress);
                                                $stmt->execute();
                                                $stmt->close();
                                                unset($stmt);
						// END reset attempts to 0

					if(!password_needs_rehash($dbPassword, PASSWORD_DEFAULT, $this->hashOptions['options'])){
						unset($password); unset($dbPassword); // unset this BEFORE doing anything else
						return $dbNumber;
					}else{
						$this->passwordUpdate($dbNumber, $password);
						unset($password); unset($dbPassword);
						return $dbNumber;
					}
				}else{
					if((!isset($dbNumber)) || (!is_numeric($dbNumber)) || ($dbNumber != (int)$dbNumber)){
						$dbNumber = -1;
					}
						$stmt = $this->dbLink->prepare('UPDATE Logins SET lastLoginAttempt=?, loginAttempts=?, userId=? WHERE ipAddress=?');

						$lastLoginAttempt = time();
						$loginAttempts = (int)$loginAttempts + 1;

						$stmt->bind_param('iiis', $lastLoginAttempt, $loginAttempts, $dbNumber, $ipAddress);
			                        $stmt->execute();
			                        $stmt->close();
			                        unset($stmt);

					unset($password); unset($dbPassword);
					return -1;
				}
 			}else{ // ELSE IF there are still more login attempts

				$loginLockTime = $this->getSetting('loginLockTime');

				if(($lastLoginAttempt + ($loginLockTime['num']*60)) < time()){
					// loginLockTime is in minutes, so multiply by 60 to convert to seconds

						// Reset attempts to 0
                                                $stmt = $this->dbLink->prepare('UPDATE Logins SET lastLoginAttempt=?, loginAttempts=?, userId=? WHERE ipAddress=?');
                                                $lastLoginAttempt = time();
                                                $loginAttempts = 0;
                                                $stmt->bind_param('iiis', $lastLoginAttempt, $loginAttempts, $dbNumber, $ipAddress);
                                                $stmt->execute();
                                                $stmt->close();
                                                unset($stmt);
                                                // END reset attempts to 0

						if(password_verify($password, $dbPassword)){ // Now we can copy/paste our password verify section
		                                        if(!password_needs_rehash($dbPassword, PASSWORD_DEFAULT, $this->hashOptions['options'])){
		                                                unset($password); unset($dbPassword); // unset this BEFORE doing anything else
		                                                return $dbNumber;
		                                        }else{
		                                                $this->passwordUpdate($dbNumber, $password);
		                                                unset($password); unset($dbPassword);
		                                                return $dbNumber;
		                                        }
		                                }else{
		                                        if((isset($dbNumber)) && (is_numeric($dbNumber)) && ($dbNumber == (int)$dbNumber)){
		                                                $stmt = $this->dbLink->prepare('UPDATE Logins SET lastLoginAttempt=? AND loginAttempts=loginAttempts+1 WHERE ipAddress=?');

		                                                $lastLoginAttempt = time();
		                                                $stmt->bind_param('si', $lastLoginAttempt, $ipAddress);

		                                                $stmt->execute();
		                                                $stmt->close();
		                                                unset($stmt);
		                                        }

	                                        unset($password); unset($dbPassword);
	                                        return -1;
		                               	} // END copy/paste password verify section
				}else{
					return -2;
				}


			} // END ELSE IF there are still more login attempts

			}else{ // ELSE IF got login/ip information
	                    unset($stmt);unset($password);
	                    die(lang::genericError);
	                } // END ELSE IF got login/ip information
		}else{ // ELSE IF got user information
	            unset($stmt);unset($password);
		    die(lang::genericError);
		} // END ELSE IF got user information
	}

	public function getLink(){
		return $this->dbLink;
	}

	public function getUserByUsername($userName){
               if($stmt = $this->dbLink->prepare('SELECT userId FROM Users WHERE userName=?')){
                        $stmt->bind_param('s', $userName);
                        if(!$stmt->execute()){
                            $stmt->close();
                            unset($stmt);unset($password);
                            die(lang::genericError);
                        }
                        $stmt->store_result();
                        $stmt->bind_result($userNumber);
                        $stmt->fetch();
                        $stmt->close();

			if((isset($userNumber)) && (is_numeric($userNumber)) && ($userNumber == (int)$userNumber)){
		                return $userNumber;
			}else{
				return -1;
			}
		}else{
			die(lang::genericError);
		}
        }

        public function getUserByEmail($emailAddress){
		// Returns the user's number who's email matches the given
               if($stmt = $this->dbLink->prepare('SELECT userId FROM Users WHERE emailAddress=?')){
                        $stmt->bind_param('s', $emailAddress);
                        if(!$stmt->execute()){
                            $stmt->close();
                            unset($stmt);unset($password);
                            die(lang::genericError);
                        }
                        $stmt->store_result();
                        $stmt->bind_result($userNumber);
                        $stmt->fetch();
                        $stmt->close();

        	        if((isset($userNumber)) && (is_numeric($userNumber)) && ($userNumber == (int)$userNumber)){
	                        return $userNumber;
	                }else{
	                        return -1;
	                }
		}else{
			die(lang::genericError);
		}
        }

	public function checkCategoryExists($catId){
		// Returns true if a category/discipline exists, else false

		if((is_numeric($catId)) && ($catId == (int)$catId)){
                // IF the numbers are good

                        // Check that $idToReplace actually exists:
                        if($stmt = $this->dbLink->prepare('SELECT categoryId FROM Categories WHERE categoryId=?')){ // Silly query, but it works
                                $stmt->bind_param('i', $idToReplace);

                                if(!$stmt->execute()){
                                    $stmt->close();
                                    unset($stmt);
                                    return false;
                                }
                                $stmt->store_result();
                                $stmt->bind_result($dbCatId);
                                $stmt->fetch();
                                $stmt->close();

                                if((isset($dbCatId)) && ($dbCatId == $catId)){
                                        return true;
                                }else{
					return false;
				}
                        }else{
                                return false;
                        }
		}else{
			die(lang::genericError);
		}
	}

	public function getCategoryList($categoryReferenceId=0){
		// Lists all of the categories by default, if categoryReferenceId is set, lists all of the subcategories


		if(is_numeric($categoryReferenceId)){
                        if($categoryReferenceId != (int)$categoryReferenceId){ die(lang::genericError); }

			if($categoryReferenceId != 0){
				$stmt = $this->dbLink->prepare('SELECT  categoryId, categoryName, categoryIdRef FROM Categories WHERE categoryIdRef=?');
				$stmt->bind_param('i', $categoryReferenceId);
			}else{
				$stmt = $this->dbLink->prepare('SELECT  categoryId, categoryName, categoryIdRef FROM Categories');
			}


			if( $stmt ){
	                        if ($stmt->execute()){
	                                $stmt->store_result();
	                                $stmt->bind_result($result_categoryId, $result_categoryName, $result_categoryIdRef);
					$categories = [];
					$i = 0;

	                                while ($stmt->fetch()){
						$categories[$i]['id'] = $result_categoryId;
						$categories[$i]['name'] = $result_categoryName;
						$categories[$i]['reference'] = $result_categoryIdRef;
						$i = $i + 1;
 	                               }
	                        }else{
	                            die(lang::genericError);
	                        }
	                }else{
	                        die(lang::genericError);
	                }

	                $stmt->close();
	                unset($stmt);

			if((isset($categories)) && (count($categories)>0)){
				return $categories;
			}else{
				return null;
			}
		}else{
			return null;
		}
	}


	public function getUserCategoryList($userNumber){
                // Lists all of the categories associated with the given user number, but NOT their subcategories


                if(is_numeric($userNumber)){
                        if($userNumber != (int)$userNumber){ die(lang::genericError); }

                    	//$stmt = $this->dbLink->prepare('SELECT  categoryId, categoryName, categoryIdRef FROM Categories WHERE categoryIdRef=?');
			$stmt = $this->dbLink->prepare('SELECT categoryId, categoryName, categoryIdRef FROM Categories WHERE categoryId IN (SELECT categoryId FROM UserCategoriesAssoc WHERE userId=?)');
                   	$stmt->bind_param('i', $userNumber);

                        if( $stmt ){
                                if ($stmt->execute()){
                                        $stmt->store_result();
                                        $stmt->bind_result($result_categoryId, $result_categoryName, $result_categoryIdRef);
                                        $categories = [];
                                        $i = 0;

                                        while ($stmt->fetch()){
                                                $categories[$i]['id'] = $result_categoryId;
                                                $categories[$i]['name'] = $result_categoryName;
                                                $categories[$i]['reference'] = $result_categoryIdRef;
                                                $i = $i + 1;
                                       }
                                }else{
                                    die(lang::genericError);
                                }
                        }else{
                                die(lang::genericError);
                        }

                        $stmt->close();
			unset($stmt);

                        if((isset($categories)) && (count($categories)>0)){
                                return $categories;
                        }else{
                                return null;
                        }
                }else{
                        return null;
                }
        }

	public function getFullUserCategoryList($userNumber){
		// Lists all of the categories associated with the given user number AND their subcategories

		if((!is_numeric($userNumber)) || ($userNumber != (int)$userNumber)){ die(lang::genericError); }

		$disciplines = $this->getCategoryList();
		$userDisciplines = $this->getUserCategoryList($userNumber);

		if((is_null($disciplines)) || (is_null($userDisciplines))){ return null; }

		$fullUserList = [];
		$subDisciplines = [];

		foreach($userDisciplines as $item){
			$subDisciplines = $this->returnRefTree( $disciplines, $item['id'], $item['id'] );

			foreach($subDisciplines as $subItem){
				if(!in_array($subItem, $fullUserList, true)){

					$found = false; $i = 0;

					while(!$found){
						if($disciplines[$i]['id'] == $subItem){
							$fullUserList[] = $disciplines[$i];
							$found = true;
						}
						$i ++;
					}
				}
			}
		}

		if(count($fullUserList) > 0){
			return $fullUserList;
		}else{
			return null;
		}
	}

	public function returnRefTree($array, $startId, $currentId){
		// Recursive function that returns the id's of every discipline below our starting id number

		$subArray = [];
		$subArray[] = $currentId;

		foreach($array as $item){
			if($item['reference'] == $currentId){
				$subArray[] = $this->returnRefTree($array, $startId, $item['id']);
			}
		}

		if($currentId == $startId){
			return $subArray;
		}else{
			return $currentId;
		}

	} // END function

	public function addDiscipline($name, $parentId){
                // Adds a discipline to the database and returns true if good or false on failure

		if( (is_numeric($parentId)) && ($parentId == (int)$parentId)){
	                if($stmt = $this->dbLink->prepare('INSERT INTO Categories(categoryName, categoryIdRef) VALUES (?,?)')){

	                        $stmt->bind_param("si", $name, $parentId);

	                        if($stmt->execute()){
					$stmt->close();
	                                unset($stmt);
	                                return true;
	                        }else{
					$stmt->close();
	                                unset($stmt);
	                                return false;
	                        }
	                }else{
	                        return false;
	                }
		}else{
			return false;
		}
       }


	public function removeDiscipline($idToRemove, $idToReplace){
		// Removes a discipline/category and replaces subcategories/subdisciplines and papers with the new one.
		// Returns true/false on success/failure

		if((is_numeric($idToRemove)) && (is_numeric($idToReplace)) && ($idToReplace == (int)$idToReplace) && ($idToRemove == (int)$idToRemove)){
		// IF the numbers are good

			// Check that $idToReplace actually exists:
			if($stmt = $this->dbLink->prepare('SELECT categoryId FROM Categories WHERE categoryId=?')){ // Silly query, but it works
                        	$stmt->bind_param('i', $idToReplace);

	                        if(!$stmt->execute()){
	                            $stmt->close();
	                            unset($stmt);
	                            return false;
	                        }
	                        $stmt->store_result();
	                        $stmt->bind_result($replaceId);
	                        $stmt->fetch();
	                        $stmt->close();

	                        if((!isset($replaceId)) || (!is_numeric($replaceId)) || ($replaceId =! (int)$replaceId)){
	                                return false;
	                        }
	                }else{
	                        return false;
	                }
		// END Check that $idToReplace actually exists

		// Change the referenceId of all categories/Disciplines that reference our old category, ie subcategories
             	if($stmt = $this->dbLink->prepare('UPDATE Categories SET categoryIdRef=? WHERE categoryIdRef=?')){

                 	$stmt->bind_param("ii", $idToReplace, $idToRemove);

                     	if(!$stmt->execute()){
                          	$stmt->close();
                              	unset($stmt);
                              	return false;
                     	}
             	}else{
                  	return false;
              	}
		// END Change the referenceId of all categories/Disciplines that reference our old category, ie subcategories

		// Change the categoryId of all papers referencing our old id to the new one in papersCategoriesAssoc
                if($stmt = $this->dbLink->prepare('UPDATE PaperCategoryAssoc SET categoryId=? WHERE categoryId=?')){

                        $stmt->bind_param("ii", $idToReplace, $idToRemove);

                        if(!$stmt->execute()){
                                $stmt->close();
                                unset($stmt);
                                return false;
                        }
                }else{
                        return false;
                }
                // END Change the categoryId of all papers referencing our old id to the new one in papersCategoriesAssoc


		// Delete the old category from the UserCategoriesAssoc Table
		if($stmt = $this->dbLink->prepare('DELETE FROM UserCategoriesAssoc WHERE categoryId=?')){
                                $stmt->bind_param('i', $idToRemove);
                                if($stmt->execute()){
                                        $stmt->close();
                                        unset($stmt);
                                }else{
                                        $stmt->close();
                                        unset($stmt);
                                        return false;
                                }
                        }else{
                                return false;
                        }
		// END the old category from the UserCategoriesAssoc Table

		// Delete the old category from the database
                if($stmt = $this->dbLink->prepare('DELETE FROM Categories WHERE categoryId=?')){

                        $stmt->bind_param("i", $idToRemove);

                        if(!$stmt->execute()){
                                $stmt->close();
                                unset($stmt);
                                return false;
                        }
                }else{
                        return false;
                }
                // END Delete the old category from the database

		return true; // Finally done!

                }else{ // ELSE IF the numbers are good
			return false;
		} // END ELSE IF the numbers are good
	}

	public function getGroupsList($groupLeader=-1){
                // Lists all of the groups by default, or lists all of the groups where the given number is the leader


                if(is_numeric($groupLeader)){
                        if($groupLeader != (int)$groupLeader){ die(lang::genericError); }

                        if($groupLeader != -1){
                                $stmt = $this->dbLink->prepare('SELECT groupId, groupName, groupLeader FROM Groups WHERE groupLeader=?');
				//groupLeader included in the select anyway to avoid conflicts below
                                $stmt->bind_param('i', $groupLeader);
                        }else{
                                $stmt = $this->dbLink->prepare('SELECT groupId, groupName, groupLeader FROM Groups');
                        }

                        if( $stmt ){
                                if ($stmt->execute()){
                                        $stmt->store_result();
                                        $stmt->bind_result($result_groupId, $result_groupName, $result_groupLeader);
                                        $groups = [];
                                        $i = 0;

                                        while ($stmt->fetch()){
                                                $groups[$i]['id'] = $result_groupId;
                                                $groups[$i]['name'] = $result_groupName;
                                                $groups[$i]['leader'] = $result_groupLeader;
                                                $i = $i + 1;
                                       }
                                }else{
                                    die(lang::genericError);
                                }
                        }else{
                                die(lang::genericError);
                        }

                        $stmt->close();
                        unset($stmt);

			if((isset($groups)) && (count($groups)>0)){
	                        return $groups;
			}else{
				return null;
			}
                }else{
                        return null;
                }
        }


	public function getUserGroupsList($userNumber=-1){
                // Lists all of the groups that a user is in. This is a little more tricky because group
		// leaders aren't listed in UsersGroupsAssoc, so we have to query both.


                if(is_numeric($userNumber)){
                        if($userNumber != (int)$userNumber){ die(lang::genericError); }


		     // Query UsersGroupsAssoc first, to get group numbers of member groups:

                        if($userNumber != -1){
                                $stmt = $this->dbLink->prepare('SELECT groupId FROM UsersGroupsAssoc WHERE userId=?');
				$stmt->bind_param('i', $userNumber);
                        }else{
                                return null;
                        }

			if( $stmt ){
                                if ($stmt->execute()){
                                        $stmt->store_result();
                                        $stmt->bind_result($result_groupId);
                                        $memberGroups = [];

                                        while ($stmt->fetch()){
                                                $memberGroups[] = $result_groupId;
                                       }
                                }else{
                                    die(lang::genericError);
                                }
                        }else{
                                die(lang::genericError);
                        }

                        $stmt->close();
                        unset($stmt);

		    // Now query all of the Groups table

			$stmt = $this->dbLink->prepare('SELECT groupId, groupName, groupLeader FROM Groups');

                        if( $stmt ){
                                if ($stmt->execute()){
                                        $stmt->store_result();
                                        $stmt->bind_result($result_groupId, $result_groupName, $result_groupLeader);
                                        $groups = [];
                                        $j = 0;

                                        while ($stmt->fetch()){
						if($result_groupLeader == $userNumber){
							$groups[$j]['id'] = $result_groupId;
	                                                $groups[$j]['name'] = $result_groupName;
                                                    	$groups[$j]['leader'] = $result_groupLeader;
                                                    	$j = $j + 1;
						}else{
							if(count($memberGroups) > 0){
							    	foreach($memberGroups as $mGroup){
									if($mGroup == $result_groupId){
									    $groups[$j]['id'] = $result_groupId;
									    $groups[$j]['name'] = $result_groupName;
									    $groups[$j]['leader'] = $result_groupLeader;
									    $j = $j + 1;
									}
								}
							}
						}
	                              }
                                }else{
                                    die(lang::genericError);
                                }
                        }else{
                                die(lang::genericError);
                        }

                        $stmt->close();
                        unset($stmt);

			if((isset($groups)) && (count($groups) > 0)){
	                        return $groups;
			}else{
				return null; //probably no results
			}
                }else{
                        return null;
                }
        }


	public function passwordUpdate($userNumber, $newPassword){
		if(is_numeric($userNumber)){
			if($userNumber != (int)$userNumber){ die(lang::genericError); }
			$passwordHash = password_hash($newPassword, PASSWORD_DEFAULT, $this->hashOptions['options']);

			if($stmt = $this->dbLink->prepare('UPDATE Users SET password=? WHERE userId=?')){
	                        $stmt->bind_param('si', $passwordHash, $userNumber);
	                        if($stmt->execute()){
					$stmt->close();
        	                        unset($stmt); unset($passwordHash);
					return true;
				}else{
					$stmt->close();
	                                unset($stmt); unset($passwordHash);
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function updateLastLogin($userId){
		if(is_numeric($userId)){
			if($userId != (int)$userId){ die(lang::genericError); }
			$stmt = $this->dbLink->prepare('UPDATE Users  SET lastLogin=? WHERE userId=?');
			date_default_timezone_set('UTC'); // Insert to database as UTC and change timezones on print/echo later
			$lastLogin = date("Y-m-d H:i:s");
			$stmt->bind_param('si', $lastLogin, $userId);
			$stmt->execute();
			$stmt->close();
			unset($stmt);
		}
	}


	public function addUser($userName, $password, $joinDate, $email, $uid, $activationCode=null, $admin=0, $userFullName){
		// Adds a user to the database and returns the user's number on success, -1 on failure

		$password = password_hash($password, PASSWORD_DEFAULT, $this->hashOptions['options']);

		if($stmt = $this->dbLink->prepare('INSERT INTO Users(userName,password,joinDate,lastLogin,emailAddress,uniqueID,activationCode,admin,userFullName) VALUES (?,?,?,?,?,?,?,?,?)')){

			$stmt->bind_param("sssssssis", $userName, $password, $joinDate, $joinDate, $email, $uid, $activationCode, $admin, $userFullName);

			if($stmt->execute()){
				$userNumber = $this->dbLink->insert_id;
			}else{
				$userNumber = -1;
			}

			$stmt->close();
			unset($password); unset($stmt);
			return $userNumber;
		}else{
			unset($password);
			return -1;
		}

	}

	public function updateUserInfo($userID, $userFullName, $email, $degree, $title, $university){

		if($stmt = $this->dbLink->prepare('UPDATE Users SET userFullName = ?, emailAddress = ?, degree = ?, title = ?, university = ? WHERE userId = ?')){

			if($stmt->bind_param('sssssi', $userFullName, $email, $degree, $title, $university, $userID)){
				if($stmt->execute()){
					$stmt->close();
					unset($stmt);
				} else {
					echo 'statement execute error';
					die(lang::genericError);
				}
			} else {
				echo 'statement bind error';
				die(lang::genericError);
			}
		} else {
			echo 'statement prepare error';
			die(lang::genericError);
		}
	}


	public function addUserDiscipline($disciplineId, $userNumber){
			// Adds a discipline to a user's list. Returns true/false on success/failure.

		if((!is_numeric($disciplineId)) || ($disciplineId != (int)$disciplineId)){ die(lang::genericError); }
		if((!is_numeric($userNumber)) || ($userNumber != (int)$userNumber)){ die(lang::genericError); }

		if($stmt = $this->dbLink->prepare('INSERT INTO UserCategoriesAssoc(userId,categoryId) VALUES (?,?)')){

                        $stmt->bind_param("ii", $userNumber, $disciplineId);

                        if(!$stmt->execute()){
                                $stmt->close();
	                        unset($stmt);
				return false;
                        }else{
				$stmt->close();
	                        unset($stmt);
				return true;
			}
                }else{
                        $stmt->close();
                        unset($stmt);
                        return false;
		}
	}

	public function removeUserDiscipline($disciplineId, $userNumber){
		// Removes a discipline from a user's list. Returns true/false on success/failure.

		if((!is_numeric($disciplineId)) || ($disciplineId != (int)$disciplineId)){ die(lang::genericError); }
                if((!is_numeric($userNumber)) || ($userNumber != (int)$userNumber)){ die(lang::genericError); }

		if($stmt = $this->dbLink->prepare('DELETE FROM UserCategoriesAssoc WHERE userId=? AND categoryId=?')){
                      	$stmt->bind_param('ii', $userNumber, $disciplineId);
                     	if($stmt->execute()){
                           	$stmt->close();
                              	unset($stmt);
                              	return true;
                     	}else{
                            	$stmt->close();
                               	unset($stmt);
                               	return false;
                 	}
            	}else{
                   	return false;
               	}
	}

	public function setReviewSkip($userNumber, $skipCount=0, $skipTime=null){
		// Updates the number of skips and last skip attempt in the user's table
		// Returns true/false on success/failure

		// IF something is wrong with the user number:
                if((!is_numeric($userNumber)) || ($userNumber != (int)$userNumber)){ die(lang::genericError); }

		if($stmt = $this->dbLink->prepare('UPDATE Users SET skipCount=?,skipTime=? WHERE userId=?')){
                        $stmt->bind_param('iii', $skipCount, $skipTime, $userNumber);
                        if(!$stmt->execute()){
                                $stmt->close();
                                unset($stmt);
                                return false;
                        }else{
                                if($this->dbLink->affected_rows > 0){
					$stmt->close();
	                                unset($stmt);
                                        return true;
                                }else{
					$stmt->close();
	                                unset($stmt);
                                        return false;
                                }
                        }
                }else{
                        return false;
                }
	}

	public function removeUserReview($peerReviewId){
		if((!is_numeric($peerReviewId)) || ($peerReviewId != (int)$peerReviewId)){ die(lang::genericError); }

                if($stmt = $this->dbLink->prepare('DELETE FROM PeerReviews WHERE peerReviewId=?')){
                        $stmt->bind_param('i', $peerReviewId);
                        if($stmt->execute()){
                                $stmt->close();
                                unset($stmt);
                                return true;
                        }else{
                                $stmt->close();
                                unset($stmt);
                                return false;
                        }
                }else{
                        return false;
                }
	}

	public function getUserReview($userNumber){
		// Gets all of the data for the peer review

		// IF something is wrong with the user number:
                if((!is_numeric($userNumber)) || ($userNumber != (int)$userNumber)){ die(lang::genericError); }

		// IF the user doesn't currently has a review ongoing:
                if(!$this->checkUserReview($userNumber)){ return false; }

		if($stmt = $this->dbLink->prepare('SELECT peerReviewId, paperId,comment,startTime,isComplete,isEndorsed FROM PeerReviews WHERE userId=? AND isComplete=0')){
                        $stmt->bind_param('i', $userNumber);
                        if(!$stmt->execute()){
                            $stmt->close();
                            die(lang::genericError);
                        }
                        $stmt->store_result();
                        $stmt->bind_result($peerReviewId, $paperId, $comment, $startTime, $isComplete, $isEndorsed);
                        $stmt->fetch();
                        $stmt->close();

			$reviewData = array( 'peerReviewId' => $peerReviewId, 'paperId' => $paperId, 'comment' => $comment, 'startTime' => $startTime, 'isComplete' => $isComplete, 'isEndorsed' => $isEndorsed );

			return $reviewData;
		}else{
			return false;
		}
	}

	public function setUserReview($userNumber){
		// Returns the id of the review on success or (-1 on generic failure, -2 if user already has a review out, -3 when out of papers)
		// Assigns the id of one random paper that is up for review and does NOT belong to the given user number to that user for peer review.

		// IF something is wrong with the user number:
		if((!is_numeric($userNumber)) || ($userNumber != (int)$userNumber)){ die(lang::genericError); }

		// IF the user currently has a review ongoing:
		if($this->checkUserReview($userNumber)){ return -2; }


		// Get array of user's disciplines and any sub-disciplines/categories:
		$userFullDisciplineArray = $this->getFullUserCategoryList($userNumber);
		$userFullDisciplineList = '';

		// Change array into a string of comma seperated values:
		foreach($userFullDisciplineArray as $item){
			$userFullDisciplineList .= (int)$item['id'] . ',';
		}

		// Remove last comma:
		$userFullDisciplineList = substr($userFullDisciplineList, 0, strlen($userFullDisciplineList)-1);

		// Get list of possible papers to review:
		if($stmt = $this->dbLink->prepare('SELECT paperId FROM Papers WHERE isEndorsed=1
							 AND paperId IN
								(SELECT paperId FROM PaperCategoryAssoc WHERE categoryId IN(' . $userFullDisciplineList . '))
							 AND paperId NOT IN
								(SELECT paperId FROM UsersPapersAssoc WHERE userId=?)
							 AND paperId NOT IN
								(SELECT paperId FROM PeerReviews WHERE userId=?)')){
				/*
			WARNING: This query is bad! It works, but it drops $userFullDisciplineList right in. Although they are forced to be integers earlier,
					it can still lead to problems. It should be binded below, however binding it as a string forces it to only accept the first
					number in the comma-seperated values. This was the only fast solution I could find. This should be fixed in a future version!
				*/

                        $stmt->bind_param('ii', $userNumber, $userNumber);

                        if(!$stmt->execute()){
                                $stmt->close();
                                unset($stmt);
                                return -1;
                        }
                        $stmt->store_result();
                        $stmt->bind_result($paperNumber);

			$reviewablePapers = []; // Our array of papers that this user can review

                        while ($stmt->fetch()){
				$almostReviewablePapers[] = $paperNumber;
			}

                        $stmt->close();
                }else{
                        return -1;
                }

		if((!isset($almostReviewablePapers)) || (count($almostReviewablePapers) < 1)){ return -3; } // out of papers to review

		$reviewablePapers = [];
		$maxReviews = $this->getSetting('paperReviews'); // get the maximum number of reviews per paper

		// Check that none of them have more than the max number of reviews:
		foreach($almostReviewablePapers as $item){
			if($stmt = $this->dbLink->prepare('SELECT count(paperId) FROM PeerReviews WHERE paperId=?')){
				$stmt->bind_param('i', $item);
	                        if(!$stmt->execute()){
	                                $stmt->close();
	                                unset($stmt);
	                                return -1;
	                        }
	                        $stmt->store_result();
	                        $stmt->bind_result($numberOfReviews);
				$stmt->fetch();
	                	$stmt->close();
			}else{
				return -1;
			}

			if($numberOfReviews < $maxReviews['num']){
				$reviewablePapers[] = $item;
			}
		}

		if(count($reviewablePapers) < 1){ return -3; } // out of papers to review


		$enableCSPRNG = $this->getSetting('enableCSPRNG'); // get the cryptographically secure random number generator settings

		if($enableCSPRNG == 1){
			// Use cryptographically secure random number generator random_int:

			$max = count($reviewablePapers) - 1; // get maximum random number
			$paper = $reviewablePapers[ random_int(0, $max) ];
		}else{
			// Use Mersenne Twister random number generator:

			$max = count($reviewablePapers) - 1; // get maximum random number
			if($max > mt_getrandmax()){ $max = mt_getrandmax(); } // if it larger than the safe number, replace it.
			$paper = $reviewablePapers[ mt_rand(0, $max) ];
		}

		//Assign user to peer review this paper:
		if($stmt = $this->dbLink->prepare('INSERT INTO PeerReviews(paperId,userId,startTime,isComplete) VALUES (?,?,?,0)')){

					$startTime = time();
                                        $stmt->bind_param("iii", $paper, $userNumber, $startTime);
                                        if(!$stmt->execute()){
                                                $stmt->close();
                                                return -1;
                                        }
					$reviewId = mysqli_insert_id($this->dbLink);
                                }else{
                                        return -1;
                                }

		return $reviewId; // Finally done, return true

	}

	public function updateUserReview($reviewId, $comment, $isComplete, $isEndorsed){
			// Updates a peer review, returns true/false on success/failure

			if((!is_numeric($reviewId)) || ($reviewId != (int)$reviewId)){ die(lang::genericError); }
			if((!is_numeric($isComplete)) || ($isComplete != (int)$isComplete)){ die(lang::genericError); }
			if((!is_numeric($isEndorsed)) || ($isEndorsed != (int)$isEndorsed)){ die(lang::genericError); }

			if($stmt = $this->dbLink->prepare('UPDATE PeerReviews SET comment=?,isComplete=?,isEndorsed=? WHERE peerReviewId=?')){

                                $stmt->bind_param('siii', $comment, $isComplete, $isEndorsed, $reviewId );
                                if($stmt->execute()){
					if($this->dbLink->affected_rows < 1){
						return false;
					}
					$stmt->close();
					unset($stmt);
                                }else{
                                        $stmt->close();
                                        unset($stmt);
                                        return false;
                                }
                        }else{
                                return false;
                        }

			if($isComplete == 1){ // IF this update is complete

				// Get all of the endorsements from the reviews of this paper
				if($stmt = $this->dbLink->prepare('SELECT isEndorsed FROM PeerReviews WHERE isComplete=1 AND paperId=(SELECT paperId FROM PeerReviews WHERE peerReviewId=?)')){
                                        $stmt->bind_param('i', $reviewId);

                                        if(!$stmt->execute()){
                                                $stmt->close();
                                                unset($stmt);
                                                die(lang::genericError);
                                        }
                                        $stmt->store_result();
                                        $stmt->bind_result($paperEndorsement);
					$forPaper = 0; // votes for the paper
					$againstPaper = 0; // votes against the paper

                                        while($stmt->fetch()){
						if((isset($paperEndorsement)) && (!is_null($paperEndorsement))){
							if($paperEndorsement == 1){
								$forPaper = $forPaper + 1;
							}else{
								$againstPaper = $againstPaper + 1;
							}
						}
					}
                                        $stmt->close();
                                }else{
                                        die(lang::genericError);
                                }

				$maxReviews = $this->getSetting('paperReviews');

				if(($forPaper + $againstPaper) >= $maxReviews['num']){ // IF there are enough reviews on this paper to make a decision

					$approvals = $this->getSetting('paperApprovals');

					if($forPaper >= $approvals['num']){ // IF the paper passed peer review
						if($stmt = $this->dbLink->prepare('UPDATE Papers SET isEndorsed=2 WHERE paperId=(SELECT paperId FROM PeerReviews WHERE peerReviewId=?)')){
			                                $stmt->bind_param('i', $reviewId );
			                                if($stmt->execute()){
			                                        $stmt->close();
			                                        unset($stmt);
			                                }
						}
					}else{ // ELSE IF the paper passed peer review
						if($stmt = $this->dbLink->prepare('UPDATE Papers SET isEndorsed=3 WHERE paperId=(SELECT paperId FROM PeerReviews WHERE peerReviewId=?)')){
                                                	$stmt->bind_param('i', $reviewId );
	                                                if($stmt->execute()){
	                                                        $stmt->close();
	                                                        unset($stmt);
	                                                }
						}
					} // END ELSE IF the paper passed peer review

				} // END IF there are enough reviews on this paper to make a decision

				return true;

			}else{ // ELSE IF this update is complete
				return true; // it is only an update, so we are done
			} // END ELSE IF this update is complete
	}


	public function checkUserReview($userNumber){
		// Returns if the user is currently reviewing a paper

		if((!is_numeric($userNumber)) || ($userNumber != (int)$userNumber)){ die(lang::genericError); }
                // IF the given user number is good

               	if($stmt = $this->dbLink->prepare('SELECT count(peerReviewId) FROM PeerReviews WHERE userId=? AND isComplete=0')){
               		$stmt->bind_param('i', $userNumber);

                       	if(!$stmt->execute()){
                        	$stmt->close();
                              	unset($stmt);
                              	die(lang::genericError);
                     	}
                       	$stmt->store_result();
                     	$stmt->bind_result($number);
                      	$stmt->fetch();
                       	$stmt->close();

			if($number > 0){
				return true;
			}else{
				return false;
			}
		}else{
			die(lang::genericError);
		}
	}

	public function removeExpiredReviews(){
		// Removes any reviews that are passed their expiration date

		$reviewTimeSetting = $this->getSetting('reviewTime');
		$reviewTime = $reviewTimeSetting['num']*24*60*60; // Get the time you have (in days) * 24 hours/day * 60 minutes/hour * 60 seconds/minute
		$currentTime = time();
		$expirationTime = (int)$currentTime - (int)$reviewTime;

		if($stmt = $this->dbLink->prepare('DELETE FROM PeerReviews WHERE startTime<?')){
                        $stmt->bind_param('i', $expirationTime);
                        if($stmt->execute()){
                                $stmt->close();
                                unset($stmt);
                                return true;
                        }else{
                                $stmt->close();
                                unset($stmt);
                                return false;
                        }
                }else{
                        return false;
                }

	}

	public function checkActivationCode($aac){
		// Check to see if the Account Activation Code matches in the database and confirms the user's email address as valid

		if($stmt = $this->dbLink->prepare('UPDATE Users SET activationCode=null WHERE activationCode=?')){
                 	$stmt->bind_param('s', $aac);
                      	if(!$stmt->execute()){
                              	$stmt->close();
                      		unset($stmt); unset($passwordHash);
                          	return false;
                        }else{
				if($this->dbLink->affected_rows > 0){
					return true;
				}else{
					return false;
				}
			}
               	}else{
                  	return false;
              	}

	}

	public function addIP($ip, $userId){
                // Adds the user's IP to the IPs table; returns true/false on success/failure

		if((is_numeric($userId)) && ($userId == (int)$userId) && ($userId > -1) && (strlen($ip)>1)){ // IF the ip and userId are good

			if($stmt = $this->dbLink->prepare('SELECT ipId FROM IPs WHERE ipAddress=? AND userId=? LIMIT 1')){

				$stmt->bind_param('si', $ip, $userId);

	                        if(!$stmt->execute()){
	                            $stmt->close();
	                            unset($stmt);
	                            die(lang::genericError);
	                        }

	                        $stmt->store_result();
	                        $stmt->bind_result($ipId);
	                        $stmt->fetch();
	                        $stmt->close();
 	               }else{
	                        die(lang::genericError);
	                }


			if((!isset($ipId)) || ($ipId != (int)$ipId)){ // IF the ip or userId isn't in the database together
			      	if($stmt = $this->dbLink->prepare('INSERT INTO IPs(ipAddress,userId) VALUES (?,?)')){

			        	$stmt->bind_param("si", $ip, $userId);
			             	if($stmt->execute()){
						$stmt->close();
		                                return true;
			              	}else{
						$stmt->close();
		                                return false;
			               	}
			       	}else{
		                        return false;
			      	}
			}else{  // ELSE IF the ip or userId isn't in the database together
				return false;
			} // END ELSE IF the ip or userId isn't in the database together
		}else{ // ELSE IF the ip and userId are good
			return false;
		} // END ELSE IF the ip and userId are good
        }

	public function addPaper($paperName, $paperURL=NULL, $paperPublishDate, $publisherId=NULL, $abstract, $sponsor=NULL, $digObjIdUrl, $digObjId, $copyRightOwner=NULL, $copyRightDate=NULL, $paperText)
	{
		//get user name and store in variable
		//$user->getNumber();
		//userData = $sql->
		//$user_name = $this->getName()
		//add to associative table
		//check to see that both inserts worked
		//kick out error on failure that prevents both inserts from happening

		if ($stmt = $this->dbLink->prepare('INSERT INTO Papers(paperName, paperURL, paperPublishDate, publisherId, abstract, sponsor, digObjIdUrl, digObjId, copyRightOwner, copyRightDate, paperText) VALUES (?,?,?,?,?,?,?,?,?,?,?)'))
		{
			//format the input date to fit the database
			date_default_timezone_set('UTC'); // Insert as UTC and change timezones on print/echo later
			$paperPublishDate = date("Y-m-d", strtotime($paperPublishDate));
			$copyRightDate = date("Y-m-d", strtotime($copyRightDate));
			//prepare the statement
			$stmt->bind_param('sssisssssss', $paperName, $paperURL, $paperPublishDate, $publisherId, $abstract, $sponsor, $digObjIdUrl, $digObjId, $copyRightOwner, $copyRightDate, $paperText);
				if($stmt->execute())
				{
					$insertId = $this->dbLink->insert_id; // Get the paperId
					$stmt->close();
					unset ($stmt);
					return $insertId; // Return the paperId
				}else{
					$stmt->close();
			        	unset($stmt);
					return -1; // Return -1 on failure
				}
		}else {die(lang::genericError);}
	}


	public function addAuthor($userId, $paperId)
	{
		if ($stmt = $this->dbLink->prepare('INSERT INTO UsersPapersAssoc(userId, paperId) VALUES (?, ?)'))
		{
			$stmt->bind_param('ii', $userId, $paperId);
			if($stmt->execute())
			{
				$insertId = $this->dbLink->insert_id;
				$stmt->close();
				unset ($stmt);
				return $insertId;
			}else{
					$stmt->close();
			        	unset($stmt);
					return -1; // Return -1 on failure
				}
		}else {die(lang::genericError);}
	}

	public function addPaperToCategory($paperId, $categoryId){

		if($this->checkCategoryExists($categoryId)){ die(lang::genericError . 'i'); }

                if ($stmt = $this->dbLink->prepare('INSERT INTO PaperCategoryAssoc(paperId,categoryId) VALUES (?, ?)')){
                        $stmt->bind_param('ii', $paperId, $categoryId);
                        if($stmt->execute()){
                                $insertId = $this->dbLink->insert_id;
                                $stmt->close();
                                unset ($stmt);
                                return $insertId;
                        }else{
                              	$stmt->close();
                              	unset($stmt);
                              	return -1; // Return -1 on failure
                    	}
                }else{
			die(lang::genericError);
		}
        }

	public function getPaperInfo($paperID){

		if(!is_numeric($paperID) || ($paperID != (int) $paperID)){ //if something is wrong...
			//error code here
			die(lang::genericError);
		} else {
			$paperInfo = array();

			//prep sql statement
			if($stmt = $this->dbLink->prepare('SELECT p.paperName, p.paperURL, p.paperPublishDate, p.abstract, p.paperText, p.isEndorsed, u.userFullName, u.userId FROM Papers AS p JOIN UsersPapersAssoc AS upa ON p.paperId = upa.paperId JOIN Users AS u ON upa.userId = u.userId WHERE p.paperId = ?')){
				$stmt->bind_param('i', $paperID);

				//run sql statement
				if ($stmt->execute()){
					$stmt->store_result();
					$stmt->bind_result($paperName, $paperURL, $paperDate, $paperAbstract, $paperText, $isEndorsed, $authorName, $authorID);

				//put values into keyed array
					while ($stmt->fetch()){
						$paperInfo[] = array('paperName' => $paperName, 'paperURL' => $paperURL, 'paperDate' => $paperDate, 'paperAbstract' => $paperAbstract, 'paperText' => $paperText, 'isEndorsed' => $isEndorsed, 'author' => $authorName, 'paperID' => $paperID, 'userId' => $authorID);
					}
				//return values
				return $paperInfo; //this array may contain multiples of the same paper with different author listings, so factor that in when parsing

				}else{ //if there's an error...
			    	die(lang::genericError);
				}

			}else{
				die(lang::genericError);
			}

		}
	} //end getPaperInfo

	public function getPaperComments($paperID){
		if(!is_numeric($paperID) || ($paperID != (int) $paperID)){ //if something is wrong...
			//error code here
			die(lang::genericError);
		} else {

			//prep sql
			if($stmt = $this->dbLink->prepare('SELECT c.paperCommentId, c.parentId, c.content, u.userName, u.userId, u.avatar FROM PaperComments AS c JOIN Users AS u ON c.userId = u.userId WHERE c.paperId = ?')){
				//run sql
				$stmt->bind_param('i', $paperID);
				if($stmt->execute()){
					$stmt->store_result();
					$stmt->bind_result($paperCommentID, $parentID, $content, $userName, $userID, $avatar);

					$comments = array();

					//put values into keyed array
					while ($stmt->fetch()){
						$comments[] = array('paperID' => $paperID, 'parent_id' => $parentID, 'paperCommentId' => $paperCommentID, 'content' => $content, 'username' => $userName, 'userID' => $userID, 'avatar' => $avatar);
					}

					//spit them back out
					return $comments;
				} else {
					die(lang::genericError);
				}
			} else {
				die(lang::genericError);
			}
		}
	}//end getPaperComments

	public function addComment($paperID, $userID, $content, $parentID){
		if((!is_numeric($paperID) && !is_numeric($userID)) || ($paperID != (int) $paperID) || ($userID != (int) $userID)){ //if something is wrong...
			//error code here
			die(lang::genericError);
		} else {
			//prep sql
			if($stmt = $this->dbLink->prepare('INSERT INTO PaperComments (paperId, userId, content, parentId) VALUES (?, ?, ?, ?)')){
				//run sql
				$stmt->bind_param('iisi', $paperID, $userID, $content, $parentID);
				if($stmt->execute()){
					echo $this->dbLink->error;
					$stmt->close();
					unset ($stmt);
					return true;
				} else {
					return false;
				}
			} else {
					die(lang::genericError);
			}
		}
	} // end Add Comment

	public function getPaperInfoByUserID($userID){

		if(!is_numeric($userID) || ($userID != (int) $userID)){ //if something is wrong...
			//error code here
			die(lang::genericError);
		} else {
			$paperInfo = array();

			//prep sql statement
			if($stmt = $this->dbLink->prepare('SELECT p.paperName, p.paperURL, p.paperPublishDate, p.abstract, p.isEndorsed, u.userFullName, u.userId, p.isEndorsed, p.paperId FROM Papers AS p JOIN UsersPapersAssoc AS up ON up.paperId = p.paperId JOIN Users AS u ON u.userId = up.userId WHERE p.paperId IN (SELECT p2.paperID FROM Papers as p2 JOIN UsersPapersAssoc AS up2 ON up2.paperId = p2.paperId WHERE up2.userId = ?) ORDER BY p.paperId DESC')){
				$stmt->bind_param('i', $userID);

				//run sql statement
				if ($stmt->execute()){
					$stmt->store_result();
					$stmt->bind_result($paperName, $paperURL, $paperDate, $paperAbstract, $isEndorsed, $authorName, $authorID, $endorsed, $paperID);

				//put values into keyed array
					while ($stmt->fetch()){
						$paperInfo[] = array('paperName' => $paperName, 'paperURL' => $paperURL, 'paperDate' => $paperDate, 'paperAbstract' => $paperAbstract, 'isEndorsed' => $isEndorsed, 'author' => $authorName, 'userId' => $authorID, 'endorsed' => $endorsed, 'paperID' => $paperID);
					}
				//return values
				return $paperInfo; //this array may contain multiples of the same paper with different author listings, so factor that in when parsing

				}else{ //if there's an error...
			    	die(lang::genericError);
				}

			}else{
				die(lang::genericError);
			}

		}
	} //end getPaperInfoByUserID

	public function fullTextSearch($searchTerm){
		// Returns the number and title of papers that match search

		if((!isset($searchTerm)) || (strlen($searchTerm) < 1)){ return null; }

		if($stmt = $this->dbLink->prepare('SELECT paperId, paperName, paperPublishDate, isEndorsed FROM Papers WHERE match (paperName, abstract) against (? in natural language mode)')){
			$stmt->bind_param('s', $searchTerm);

		       	if(!$stmt->execute()){
		              	$stmt->close();
		        	unset($stmt);
		         	die(lang::genericError);
		        }
		        $stmt->store_result();
		       	$stmt->bind_result($paperId, $paperName, $paperPublishDate, $isEndorsed);

			$results = [];

			while($stmt->fetch()){
				$results[] = array('paperId' => $paperId, 'paperName' => $paperName, 'paperPublishDate' => $paperPublishDate, 'isEndorsed' => $isEndorsed);
			}

			$stmt->close();

			if(count($results) > 0){
				return $results;
			}else{
		       		return null;
		       	}
	     	}else{
	          	die(lang::genericError);
	       	}
	}


} // END mySQLi Class

}else{ // ELSE IF included from another php source
if(headers_sent()){ die(include("404.html")); } // If headers were somehow already sent, just die to a custom 404 page
header("HTTP/1.1 404 Not Found"); // Send headers if not sent already
die(include("404.html")); // Die to custom 404 page
} // END ELSE IF included from another source

?>
